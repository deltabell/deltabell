// Packages
var React = require('react');
var browserHistory = require('react-router').browserHistory;

// Styles
var Styles = require('../css/footer.css');

// Class
var Footer = React.createClass({
    browseContactUs: function(){
        browserHistory.push('contactUs');
        window.scroll(0,0);
    },
    render: function(){
        return(
            <footer className="footer-container">
                <div className="container footer-links-container">
                    <div className="col-xs-3 col-xs-offset-6 footer-links-container-col">
                        <div>שירותים</div>
                        <div>מחירים</div>
                    </div>
                    <div className="col-xs-3 footer-links-container-col">
                        <div>אודות</div>
                        <div>תנאי שימוש</div>
                        <div onClick={this.browseContactUs}>צור קשר</div>
                    </div>
                </div>
                <div className="container">
                    <div className="footer-copyright-container">
                        כל הזכויות שמורות לדלתא בל 770 בע״מ
                        <img className="footer-facebook-sharing-icon" src="/misc/share-in-facebook.png" title="פייסבוק"
                             onClick={(e)=>{window.open('https://www.facebook.com/deltabell770')}}/>
                    </div>
                </div>
            </footer>
        )
    }
});

module.exports = Footer;