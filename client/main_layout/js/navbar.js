//
// the navbar contains the header navbar of the application
//

// packages
var React = require('react');
var Axios = require('axios');

// Helper
var Utils = require('../../common/js/utils.js')

// Css
var Styles = require('../css/navbar.css');

// Used Classes
var browserHistory = require('react-router').browserHistory;

ITEMS_WITH_HTTP_ACCESS = ['/map']; 
var DELTA_NAV_ITEM_STYLE;

// creating the navbar class
var NavBar = React.createClass({
    getInitialState: function(){
        return(
            {isMobile: Utils.isMobile()}
        )
    },
    // Have to render component if screen width was changed significantly
    checkScreenChange: function(){
        if (Utils.isMobile() != this.state.isMobile){
            this.setState({isMobile: Utils.isMobile});
        }
    },
    componentDidMount: function(){
        window.addEventListener("resize", this.checkScreenChange);
    },
    componentWillUnmount: function(){
        // removing event listener
        window.removeEventListener("resize", this.checkScreenChange);
    },
    browseMain: function(){
        this.goToNavbarItem('/');
    },
    goToNavbarItem: function(itemPath){
        targetItemShouldRunInHttps = !ITEMS_WITH_HTTP_ACCESS.includes(itemPath);
        isCurrentConectionUsesHttps = window.location.href.includes('https')
        
        if (targetItemShouldRunInHttps != isCurrentConectionUsesHttps){
            // create new request to server - for refresh the access to be with new method http / https
            window.open(itemPath,'_self');
        } else {
            // navigate into react component without new request to server
            browserHistory.push(itemPath);
            window.scrollTo(0,0);
        }
    },

    browseContactUs: function(){
        this.goToNavbarItem('/contactUs');
    },
    browseAccount: function(){
        if (this.props.account){
            this.goToNavbarItem('/account');
        } else {
            this.props.setLoginModalState(true, '/account');
        }
    },
    browseBoard: function(){
        this.goToNavbarItem('/board');
    },
    browseMap: function(){
        if (this.props.account){
            this.goToNavbarItem('/map');
        } else {
            this.props.setLoginModalState(true, '/map');
        }
    },
    login: function(){
        this.props.setLoginModalState(true);
    },
    logout: function(){
        Axios.get('/api/accounts/logout', this, browserHistory).then(function (response) {
            this.props.refreshAccountDetails();
            this.goToNavbarItem('/');
        }.bind(this, browserHistory));
    },
    register: function(){
        this.props.setLoginModalState(true,'/',true);
    },
    getNavItemClass: function(urlParam){
       if (document.URL.split('/').pop() === urlParam){
           return(DELTA_NAV_ITEM_STYLE + " selected-nav-item");
       } else {
           return(DELTA_NAV_ITEM_STYLE);
       }
    },
    getRefToLogInOut: function(){
        userHelloMessage = this.props.account ? "שלום" + " " + this.props.account.name : '';

        if (this.props.account) {
            rightSide = "שלום" + " " + this.props.account.name;
            leftSide = <span className="delta-log-in-out"
                             onClick={this.logout}>
                            |&nbsp;&nbsp;<u>התנתק</u>
                       </span>
        } else {
            rightSide = <span className="delta-log-in-out"
                              onClick={this.register}><u>הרשמה</u>
                        </span>
            leftSide =  <span className="delta-log-in-out"
                             onClick={this.login}>|&nbsp;&nbsp;<u>התחבר</u>
                        </span>
        }

        return <div className="row">
                    <div className={Utils.getStyleSuffixForMobile("col-sm-4 col-xs-12  delta-hello-user-area")}>
                        {leftSide}
                    </div>
                    <div className={Utils.getStyleSuffixForMobile("col-sm-8 hidden-xs delta-hello-user-area")} style={{textAlign:"left", paddingLeft:"10px"}}>
                        {rightSide}
                    </div>
                </div>;

    },
    render: function(){
        DELTA_NAV_ITEM_STYLE = Utils.getStyleSuffixForMobile('delta-nav-item');
        return(
            <div className="navbar-fixed-top delta-bell-navbar">
                <div className="container-fluid delta-navbar-container">
                    <div className="col-xs-2">
                        {this.getRefToLogInOut()}
                    </div>
                    <div className="col-xs-10">
                        <img className={Utils.getStyleSuffixForMobile("delta-bell-navbar-icon")} 
                                src="/misc/delta_bell_logo_no_text.png" 
                                onClick={this.browseMain}
                                title="לדף הבית"/>
                        <div onClick={this.browseAccount} className={"hidden-xs hidden-sm " + this.getNavItemClass("account")}>החשבון שלי</div>
                        <div onClick={this.browseContactUs} className={this.getNavItemClass("contactUs")}>צור קשר</div>
                        <div onClick={this.browseBoard} className={this.getNavItemClass("board")}>לוח דירות</div>
                        <div onClick={this.browseMap} className={"hidden-xs hidden-sm " + this.getNavItemClass("map")}>מפת נדל״ן בתלת מימד - גרסת beta</div>
                    </div>
                </div>
            </div>
        )
    }
})

module.exports = NavBar;
