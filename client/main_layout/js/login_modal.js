// Requiring the packages
var React = require('react');
var Axios = require('axios');
var Validator = require('bootstrap-validator');
var BrowserHistory = require('react-router').browserHistory;
var Utils = require('../../common/js/utils.js');

Styles = require('../css/login_modal.css');

// LoginPageLayout2 Class
var LoginModal = React.createClass({
    componentDidMount: function(){
        // Whne th modal is closed then reset modal values
        $('#loginModal').modal('show');
        $('#loginModal').on('hidden.bs.modal', (e)=>{
            this.closeModal()
        });
        if (this.props.chooseRegisterTab){
            $('.nav-tabs a[href="#create"]').tab('show');
        } else {
            $('.nav-tabs a[href="#login"]').tab('show');
        }
        $('#login').validator();
        $('#create').validator();
    },
    closeModal: function(){
        $('#loginModal').modal('hide');
        this.props.setLoginModalState(false)
    },
    clearFieldsContent: function(){
        this.refs.loginUserName.value="";
        this.refs.loginPassword.value="";
        this.refs.loginResponseMessage.innerHTML="";
        this.refs.registerName.value="";
        this.refs.registerUserName.value="";
        this.refs.registerPassword.value="";
        this.refs.registerResponseMessage.innerHTML="";
    },
    handleLogin: function() {
        if (this.refs.loginUserName.value !== "" & this.refs.loginValid1.innerText === "" & this.refs.loginValid2.innerText === ""){
            this.setLoginControlsDisable(true);
            Axios.post('/api/accounts/login', {
                username: this.refs.loginUserName.value,
                password: this.refs.loginPassword.value
            }).then(function (response) {
                if (!response.data.isAccountExists)
                {
                    this.refs.loginResponseMessage.innerHTML = Utils.getFailureHtmlMessage("שם משתמש או סיסמה אינם נכונים");
                    this.setLoginControlsDisable(false);
                }
                else if (response.data.account)
                {
                    this.props.refreshAccountDetails(response.data.account);
                    if (this.props.originalPath) {
                        if (this.props.originalPath === '/map') {
                            window.open('/map','_self');
                        }
                        BrowserHistory.push(this.props.originalPath);
                    }  
                    this.closeModal();                                      
                }
                // account isn't activated
                else
                {
                    this.refs.loginResponseMessage.innerHTML = Utils.getFailureHtmlMessage("נא הפעל את חשבונך מהקישור שנשלח לדואר האלקטרוני");
                    this.setLoginControlsDisable(false);
                }
            }.bind(this))
        }
    },
    setRegisterControlsDisable: function(disable) {
        this.refs.registerName.disabled =
            this.refs.registerUserName.disabled =
                this.refs.registerPassword.disabled =
                    this.refs.btnExecuteRegistering.disabled = disable;
    },
    setLoginControlsDisable: function(disable) {
        this.refs.btnExecuteLogin.disabled =
            this.refs.loginUserName.disabled =
                this.refs.loginPassword.disabled = disable;
    },
    handleRegister: function() {
        if (this.refs.registerUserName.value !== ""
          & this.refs.registerValid1.innerText === "" 
          & this.refs.registerValid2.innerText === ""
          & this.refs.registerValid3.innerText === "")
        {
            this.setRegisterControlsDisable(true);

            Axios.post('/api/accounts/register', {
                username: this.refs.registerUserName.value,
                nameOfUser: this.refs.registerName.value,
                password: this.refs.registerPassword.value
            }).then(function (response) {
                if (response.data.isRegistrationSuccess)
                {
                    this.refs.registerResponseMessage.innerHTML = "<font color=\"green\">" + (response.data.message) + "</font>";
                }
                else
                {
                    this.refs.registerResponseMessage.innerHTML = "<font color=\"red\">ההרשמה נכשלה מסיבה כלשהי, נא צור איתנו קשר באמצעות ׳יצירת קשר׳ באתר</font>";
                    this.set_registering_control_disabling(true);
                }

            }.bind(this))
        }
    },
    forgotPassword: function() {
        this.closeModal();
        BrowserHistory.push('/forgotPassword');
    },
    render: function() {
        return (<div className="container"><div id="loginModal" className="modal custom fade" role="dialog">
                <div className="modal-dialog" id="loginModal">
                    <div className="modal-body">
                        <div className="well">
                            <div>
                                <button type="button" className="close" onClick={(e)=>{this.closeModal()}}
                                        data-dismiss="modal" aria-hidden="true">X</button>
                            </div>
                            <div>
                                <ul className="nav nav-tabs delta-login-nav-tabs">
                                    <li><a href="#login" data-toggle="tab" id="tabLogin">כניסה</a></li>
                                    <li><a href="#create" data-toggle="tab" id="tabRegister">הרשמה</a></li>
                                </ul>
                            </div>
                            <div id="myTabContent" className="tab-content">
                                <form data-toggle="validator" role="form" className="tab-pane" id="login">
                                    <div className="container container-login-form">
                                        <div className="row form-group">
                                            <div className="col-xs-12 col-sm-8">
                                                <input type="email" ref="loginUserName" className="form-control" 
                                                       id="loginUserName" placeholder="דואר אלקטרוני" required
                                                       data-error="נא הזן כתובת דואר אלקטרוני תקינה"/>
                                                <div ref="loginValid1" className="help-block with-errors"/>
                                            </div>
                                            <label className="hidden-xs col-sm-4 control-label">דואר אלקטרוני</label>
                                        </div>
                                        <div className="row login-top-buffer form-group">
                                            <div className="col-xs-12 col-sm-8">
                                                <input type="password" ref="loginPassword" className="form-control" 
                                                       id="loginPassword" placeholder="סיסמה" required
                                                       data-error="סיסמה הינה שדה חובה"/>
                                                <div ref="loginValid2" className="help-block with-errors"/>
                                            </div>
                                            <label className="hidden-xs col-sm-4 control-label">סיסמה</label>
                                        </div>
                                        <div className="row forgot-password-link">
                                            <u className="col-xs-12 col-sm-8" onClick={this.forgotPassword}>שכחת סיסמה?</u>
                                        </div>
                                        <div className="col-login-button">
                                            <input type="button" ref="btnExecuteLogin" onClick={this.handleLogin} className="btn btn-primary delta-btn" defaultValue="התחבר"/>
                                        </div>
                                        <div className="delta-forgot-password">
                                            <div ref="loginResponseMessage" className="help-block"/>
                                        </div>
                                    </div>
                                </form>
                                <form data-toggle="validator" role="form" className="tab-pane" id="create">
                                    <div className="container container-login-form">
                                        <div className="row form-group">
                                            <div className="col-xs-12 col-sm-8">
                                                <input type="email" ref="registerUserName" className="form-control" 
                                                       id="registerUserName" placeholder="דואר אלקטרוני" name="username" required
                                                       defaultValue="" data-error="נא הזן כתובת דואר אלקטרוני תקינה" data-remote="/api/accounts/isUsernameNotExists"
                                                       defaultValue="" data-error="נא הזן כתובת דואר אלקטרוני תקינה"
                                                       data-remote-error="שם משתמש כבר קיים"/>
                                                <div ref="registerValid1" className="help-block with-errors"/>
                                            </div>
                                            <label className="hidden-xs col-sm-4 control-label">דואר אלקטרוני</label>
                                        </div>
                                        <div className="row login-top-buffer form-group">
                                            <div className="col-xs-12 col-sm-8">
                                                <input type="password" ref="registerPassword" className="form-control" 
                                                       id="registerPassword" placeholder="סיסמה" required
                                                       data-error="סיסמה הינה שדה חובה"/>
                                                <div ref="registerValid2" className="help-block with-errors"/>
                                            </div>
                                            <label className="hidden-xs col-sm-4 control-label">סיסמה</label>
                                        </div>
                                        <div className="row login-top-buffer form-group">
                                            <div className="col-xs-12 col-sm-8">
                                                <input type="text" ref="registerName" className="form-control" 
                                                       id="registerName" placeholder="שם" required
                                                       data-error="שם הינו שדה חובה"/>
                                                <div ref="registerValid3" className="help-block with-errors"/>
                                            </div>
                                            <label className="hidden-xs col-sm-4 control-label">שם</label>
                                        </div>
                                        <div className="row login-top-buffer">
                                            <div className="col-login-button">
                                                <input type="button" ref="btnExecuteRegistering" onClick={this.handleRegister} className="btn btn-primary delta-btn" defaultValue="בצע רישום"/>
                                            </div>
                                            <div>
                                                <div ref="registerResponseMessage" className="help-block"/>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>)
    }
});

module.exports = LoginModal;
