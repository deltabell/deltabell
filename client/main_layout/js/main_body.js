// Packages
var React = require('react');

// Styles
var Styles = require('../css/main_body.css');

// Class
var MainBody = React.createClass({
    render: function(){
        return(
            <div className="delta-main-body">
                {React.cloneElement(this.props.childrenList, {
                    account: this.props.account,
                    refreshAccountDetails: this.props.refreshAccountDetails,
                    setLoginModalState: this.props.setLoginModalState
                })}
            </div>
        );
    }
});

module.exports = MainBody;