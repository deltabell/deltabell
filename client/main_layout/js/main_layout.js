// This is the main layout class
// It contains the header and footer of the application (the upper navbar and the lower one as well)
// should be static and allways used in all parts of the application

// Requiring the packages
var React = require('react');
var browserHistory = require('react-router').browserHistory;
var Axios = require('axios');

// Getting the Used Classes
NavBar = require('./navbar.js');
Footer = require('./footer.js');
MainBody = require('./main_body');
LoginModal = require('./login_modal');

// Styles
Styles = require('../css/main_layout.css');

// Main Layout Class
var MainLayout = React.createClass({
    // Settings default
    getInitialState: function(){
        return({
            account:null,
            isLoginModalOpen:false,
            originalPath: "",
            loginChooseRegisterTab: "",
            isAuthorizationChecked:false
        });
    },
    
    // Update layouts if visitor is connected or not
    componentDidMount: function(){
        this.loadAccountDetailsFromServer();
    },
    loadAccountDetailsFromServer: function(){
        Axios.get('/api/accounts/getMyAccount', this).then(function (response) {
            var account = response.data.isAccountExists ? response.data.account : null;
            this.setState({account: account, isAuthorizationChecked: true});
        }.bind(this))
    },
    setLoginModalState: function(isLoginModalOpen, originalPath, loginChooseRegisterTab){
        this.setState({isLoginModalOpen: isLoginModalOpen,
                       originalPath: originalPath,
                       loginChooseRegisterTab: loginChooseRegisterTab});
    },
    refreshAccountDetails: function(){
        this.loadAccountDetailsFromServer();
    },
    getMainDiv: function(){
        if (this.state.isAuthorizationChecked){
            return <MainBody childrenList={this.props.children}
                             account={this.state.account}
                             refreshAccountDetails={this.refreshAccountDetails}
                             setLoginModalState={this.setLoginModalState}/>
        } else {
            return '';
        }
    },
    getLoginModalIfRequired: function(){
        if (this.state.isLoginModalOpen) {
            return <LoginModal setLoginModalState={this.setLoginModalState}
                               refreshAccountDetails={this.refreshAccountDetails}
                               account={this.state.account}
                               originalPath={this.state.originalPath}
                               chooseRegisterTab={this.state.loginChooseRegisterTab}/>
        } else {
            return <div/>
        }
    },
    render: function() {
        return (
            <div className="container-fluid delta-main-container">
                <NavBar account={this.state.account}
                        refreshAccountDetails={this.refreshAccountDetails}
                        setLoginModalState={this.setLoginModalState}/>
                {this.getMainDiv()}
                {this.getLoginModalIfRequired()}
            </div>
        )
    }
});

module.exports = MainLayout;
