// Required pckages
var React = require('react');
var ReactDOM = require('react-dom');
var ReactRouter = require('react-router');

// Define Objects
var Route = ReactRouter.Route;
var Router = ReactRouter.Router;
var IndexRoute = ReactRouter.IndexRoute;
var IndexRedirect = ReactRouter.IndexRedirect;
var BrowserHistory = ReactRouter.browserHistory;

// Adding Common Styles to app
var CommonCss = require('./common/css/common_styles.css');

// Get layouts
var MainLayout = require('./main_layout/js/main_layout.js');
var MapLayoutContainer = require('./map_layout/js/map_layout_sec_container');
var SingleApartmentLayout = require('./single_apartment_layout/js/single_apartment_layout.js')
var NotFoundLayout = require('./simple_layouts/js/not_found_layout.js')
var HomeLayout = require('./home_layout/js/home_layout.js');
var ContactUsLayout = require('./contact_us_layout/js/contact_us_layout.js');
var BoardLayout = require('./board_layout/js/board_layout.js');
var AccountLayout = require('./account_layout/js/account_layout.js');
var ConfirmationAccountLayout = require('./simple_layouts/js/confirmation_account_layout.js');
var ForgotPasswordLayout = require('./simple_layouts/js/forgot_password_layout.js');
var ShowTourLayout = require('./simple_layouts/js/show_tour_layout.js');
var ShowExternalLayout = require('./simple_layouts/js/show_external_layout.js');

// Routing
ReactDOM.render((
  <Router history={BrowserHistory}>
    <Route path="/showTour/:id" component={ShowTourLayout}/> 
    <Route path="/showExternal/:id" component={ShowExternalLayout}/>   
    <Route path="/" component={MainLayout}>
      <IndexRoute component={HomeLayout}/>
      <Route path="contactUs" component={ContactUsLayout}/>
      <Route path="board" component={BoardLayout}/>
      <Route path="map" component={MapLayoutContainer}/>
      <Route path="account" component={AccountLayout}/>
      <Route path="singleApartment/:id" component={SingleApartmentLayout}/>
      <Route path="confirmationAccount/:result/:message" component={ConfirmationAccountLayout}/>
      <Route path="forgotPassword" component={ForgotPasswordLayout}/>
      <Route path='*' component={NotFoundLayout} />
    </Route>
  </Router>
), document.getElementById('root'))
