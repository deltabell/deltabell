// Packages
var React = require('react');

// Styles
var Styles = require('../css/cesium_map.css');

var CesiumHelper = require('../../common/js/cesium_helper.js');

var Utils = require('../../common/js/utils.js');

var CesiumMap = React.createClass({
    getInitialState: function(){
        return {estimatedHeight: null}
    },
    componentDidMount: function(){
        viewerOptions = {containerDiv: this.refs.aptCesiumMap,
                         minimumZoomDistance: 20.0,
                         maximumZoomDistance: 5000.0}
        
        this.viewer = CesiumHelper.getViewer(viewerOptions);
        
        this.tiles = [];
        for (var i = 0; i<this.props.tiles.length; i++) {
            this.tiles.push(this.viewer.scene.primitives.add(new Cesium.Cesium3DTileset({
                url: this.props.tiles[i],
                maximumScreenSpaceError : Utils.isMobile() ? 8 : 1, // Temporary workaround for low memory mobile devices - Increase maximum error to 8.
	            maximumNumberOfLoadedTiles : Utils.isMobile() ? 10 : 1000 // Temporary workaround for low memory mobile devices - Decrease (disable) tile cache.
            })));
        }

        this.getHeight(function(height){
            // define locaion center cartesian3 for another functions
            this.center = new Cesium.Cartesian3.fromDegrees(this.props.lng, this.props.lat, height)
            
            // fly to apartment location
            this.flyToPrimaryLocation();
            
            // define function called after mooving camera by user
            this.addMovingCameraListener();

            // drow icon in center
            if (this.props.height){
                var orangeOutlined = this.viewer.entities.add({
                    polyline : {
                        positions : Cesium.Cartesian3.fromDegreesArrayHeights([this.props.lng, this.props.lat, height,
                                                                            this.props.lng, this.props.lat, height + 40]),
                        width : 0.8,
                        material : new Cesium.PolylineOutlineMaterialProperty({
                            color : Cesium.Color.WHITE,
                            outlineWidth : 0
                        })
                    }
                });
                // Adding point should be icon
                this.viewer.entities.add({
                    position : Cesium.Cartesian3.fromDegrees(this.props.lng, this.props.lat, height+40),
                    billboard: {
                        image: "/misc/house_marker.png",
                        height: 25,
                        width: 25,
                        color: Cesium.Color.fromCssColorString('#35089d')
                    }
                });
            }
        }.bind(this))
    },
    addMovingCameraListener: function(){
        self = this;
        this.viewer.camera.moveEnd.addEventListener(function(e) {
            cameraPosition = self.viewer.camera.position;
            originalPosition = self.center;
            maxDistance = 1000
            if (Math.abs(cameraPosition.x - originalPosition.x) > maxDistance ||
                Math.abs(cameraPosition.y - originalPosition.y) > maxDistance){
                self.flyToPrimaryLocation();
            }
        })       
    },
    flyToPrimaryLocation: function(){
        var boundingSphere = new Cesium.BoundingSphere(this.center);
        var requiredHeading = this.props.cameraHeading ? this.props.cameraHeading : this.viewer.camera.heading; 
        this.viewer.camera.flyToBoundingSphere(boundingSphere, {
            offset: new Cesium.HeadingPitchRange(requiredHeading, Cesium.Math.toRadians(-45.0), 175)
        })
    },
    getHeight: function(callback){
        if (this.props.height) {
            callback(this.props.height);
        } else if (this.state.estimatedHeight) {
            callback(this.state.estimatedHeight)
        }
        else { //create astumated height
            CesiumHelper.getZByXandY(this.props.lng, this.props.lat, function(height){
                this.setState({estimatedHeight: height});
                callback(height);
            }.bind(this))
        }
    },
    setClickEvent: function(viewer){
        handler = new Cesium.ScreenSpaceEventHandler(viewer.scene.canvas);
        var self = this;
        handler.setInputAction(function(clickedPosition) {
            clearInterval(self.interval);
        }, Cesium.ScreenSpaceEventType.LEFT_CLICK);
    },
    rotateCamera: function(){
        if (this.degrees > 720)
        {
            clearInterval(this.interval);
        }
        this.degrees = this.degrees + 1;
        this.viewer.camera.rotate(this.cameraCenter, (1 * Math.PI) / 360);
    },
    render: function(){
        return(
            <div className="cesium-inner-container" ref="aptCesiumMap"/>
        )
    }
})

module.exports = CesiumMap;