// packages
var React = require('react');
var MatterportContainer = require('./matterport_container.js');
var ApartementDetailsContainer = require('./apartment_details_container.js');
var ApartementSummaryContainer = require('./apartment_summary_container.js');
var ApartmentGMapContainer = require('./apartment_gmap_container.js');
var ApartmentSketchContainer = require('./apartment_sketch_container.js');
var GalleryContainer = require('./gallery_container.js');
var NotFoundLayout = require('../../simple_layouts/js/not_found_layout.js');
var VisitingCodeModal = require('../../common/js/visiting_code_modal.js');
var CesiumContainer = require('./cesium_container.js');
var ApartmentHeader = require('./apartment_header.js');
var Axios = require('axios');
var Footer = require('../../main_layout/js/footer.js')
var Utils = require('../../common/js/utils.js');


// Styles
Styles = require('../css/single_apartment_layout.css');

// Creating the single apartment Layout
var SingleApartmentLayout = React.createClass({
    getInitialState: function(){
        return {apartment: 'loading',
                isApartmentAccessible: false};
    },

    // Setting the state from DB
    componentDidMount: function(){
        Axios.get('/api/apartments/' + this.props.params.id, this.state)
            .then(function (response) {
                const returned_json = response.data;
                if (returned_json.images){
                    this.setState({apartment: returned_json})
                } else {
                    this.setState({apartment: null});
                }
            }.bind(this))  
        Axios.put('/api/visits/addApartmentVisit/' + this.props.params.id, {
            navigatorAgent: navigator.userAgent
        });       
    },
    renderCesiumContainer: function(){
        if (this.props.account){
            return(<CesiumContainer apartment={this.state.apartment}/>);
            //return(["admin","cesium"].includes(this.props.account.userType) ? 
            //    <CesiumContainer apartment={this.state.apartment}/> : <div/>);
        } else {
            return (<div/>);
        }
    },
    setIsApartmentAccessible: function(isAvaliable){
        this.setState({isApartmentAccessible: isAvaliable});
    },
    // Rendering Component
    render: function(){
        // Initial state of page before apartment data receives from server
        if (this.state.apartment == 'loading')
        {
            return <div/>;
        }
        // If apartment id not found
        else if (this.state.apartment == null)
        {
            return <NotFoundLayout/>;
        }
        // If apartment data receives from database
        else if (!this.state.isApartmentAccessible &&
                 !Utils.doesApartmentAccessible(this.state.apartment, this.props.account))
        {
            return(<VisitingCodeModal setIsApartmentAccessible={this.setIsApartmentAccessible}
                                      apartment={this.state.apartment}/>);
        }
        else
        {
            return(<div>
                        <ApartementSummaryContainer
                            apartment={this.state.apartment}/>            
                        <GalleryContainer images={this.state.apartment.images}/>
                        <ApartementDetailsContainer details={this.state.apartment.details}/>
                        <div className="container">
                            <ApartmentHeader title="סיור תלת מימד" 
                                            additionalInfo={Utils.getPhotographyDateMessage(this.state.apartment)} 
                                            glyphicon="glyphicon glyphicon-facetime-video"/>
                        </div>
                        <div className="container sa-mat-container">
                            <MatterportContainer matterportKey={this.state.apartment.matterportKey}/>
                        </div>
                        <ApartmentSketchContainer sketchFile={this.state.apartment.sketchFile}/>
                        {this.renderCesiumContainer()}
                        <ApartmentGMapContainer
                            lat={this.state.apartment.lat}
                            lng={this.state.apartment.lng}
                            gush={this.state.apartment.gush}
                            section={this.state.apartment.section}/>
                        <Footer/>
                    </div>);
        }
    }
});

module.exports = SingleApartmentLayout;

