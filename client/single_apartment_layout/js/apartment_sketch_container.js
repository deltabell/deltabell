// Packages
var React = require('react');
var ApartmentHeader = require('./apartment_header.js');

// Styles
var Styles = require('../css/apartment_sketch_container.css');

// Class
var ApartmentSketchContainer = React.createClass({
    render: function(){
        if (this.props.sketchFile === ""){
            return(<div/>);
        }
        return(
            <div className="container">
                <ApartmentHeader title="תשריט" glyphicon="glyphicon glyphicon-book"/>
                <div className="apt-sketch-container">
                    <img className="img-responsive apt-sketch-container" src={this.props.sketchFile}/>
                </div>
            </div>
        )
    }
})

module.exports = ApartmentSketchContainer;