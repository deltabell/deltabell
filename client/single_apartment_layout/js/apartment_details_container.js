// Packages
var React = require('react');
var ApartmentHeader = require('./apartment_header.js');

// Styles
var Styles = require('../css/apartment_details_container.css');

// Get All Possible features array
var possibleFeatures = require('../../../common/static_data/apartment_feature_list.js').filter(function(feature){
    return (feature.visible === true)
});

// Class
var ApartmentDetailsContainer = React.createClass({
    // checks if the feature exists in the apartment features
    getGlyphicon: function(featureName){
        return (this.props.details.features.includes(featureName) ? "glyphicon glyphicon-ok" : "glyphicon glyphicon-remove");
    },

    // Gets 3 details array and return row
    renderFeatureItem: function(feature){
        return(
            <div className="col-xs-12 col-md-4 feature_item" key={feature.name}>
                <div className="row">
                   <div className="col-xs-2 col-xs-offset-2 col-md-offset-5 feature-check">
                        <span className={this.getGlyphicon(feature.name)} aria-hidden="true"></span>
                    </div>
                    <div className="col-xs-8 col-md-5 feature-title">
                        {feature.name}
                    </div>
                </div>
            </div>
        )
    },

    // Creating icon for each feature
    renderDetails: function(){
        return(
            possibleFeatures.map(this.renderFeatureItem)
        )
    },

    render: function(){
        return(
            <div className="container">
                <ApartmentHeader title="פרטים נוספים" glyphicon="glyphicon glyphicon-book"/>
                <div className="apt-details-container">
                    {this.props.details.description}
                    <div className="feature-container container-fluid">
                        {this.renderDetails()}
                    </div>    
                </div>
            </div>
        )
    }
});

module.exports = ApartmentDetailsContainer;

