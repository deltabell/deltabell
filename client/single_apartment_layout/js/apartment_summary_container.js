// Packages
var React = require('react');
var ApartmentSummaryItem = require('./apartment_summary_item.js');
var SharingComponent = require('../../common/js/sharing_component.js');

// Styles
var Styles = require('../css/apartment_summary_container.css');

// Class
var ApartmentSummaryContainer = React.createClass({
    getFormattedPrice : function(){
        return !this.props.apartment.summary.price ? '-' : Number(this.props.apartment.summary.price).toLocaleString();
    },
    render: function(){
        return(
            <div className="container">
                <div className="row">
                    <div className="col-xs-3 delta-align-content-left">
                        <SharingComponent apartmentId={this.props.apartment._id}/>
                    </div>
                    <div className="col-xs-9">
                        <h1 className="single-apartment-title">{this.props.apartment.apartmentTitle}</h1>
                    </div>
                </div>
                <div className="delta-bell-apt-summary-container">
                    <div className="row">
                        <div className="col-sm-12 col-md-3 col-md-offset-1">
                            <ApartmentSummaryItem title="איש קשר" value={this.props.apartment.summary.contact}/>
                        </div>
                        <div className="col-sm-12 col-md-2">
                            <ApartmentSummaryItem title="קומה" value={this.props.apartment.summary.floor}/>
                        </div>
                        <div className="col-sm-12 col-md-2">
                            <ApartmentSummaryItem title="גודל במ״ר" value={this.props.apartment.summary.size}/>
                        </div>
                        <div className="col-sm-12 col-md-2">
                            <ApartmentSummaryItem title="חדרים" value={this.props.apartment.summary.rooms}/>
                        </div>
                        <div className="col-sm-12 col-md-2">
                            <ApartmentSummaryItem title="מחיר"
                                                  value={this.getFormattedPrice()}/>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
});

module.exports = ApartmentSummaryContainer;