// Packages
var React = require('react');
var ApartmentHeader = require('./apartment_header.js');

// Styles
var Styles = require('../css/apartment_gmap_container.css');

// Class
var ApartmentGMapContainer = React.createClass({
    componentDidMount: function(){
        // setting location
        var location = {lat: this.props.lat, lng: this.props.lng};

        // Creating the Map 
        this.map = new google.maps.Map(this.refs.gmap, {
                center:  location,
                zoom: 17
        });
        // Centring the map and creating marker
        this.marker = new google.maps.Marker({
            position: location,
            map: this.map
        });
        this.map.setCenter(location);
    },

    render: function(){
        // calculate title
        if (this.props.gush != "" & this.props.section != "")
        {
            var apartmentTitle = "מיקום,        גוש: " + this.props.gush + " חלקה: " + this.props.section;
        }
        else
        {
            var apartmentTitle = "מיקום";
        }
        
        return(
            <div className="container">
                <ApartmentHeader title={apartmentTitle} glyphicon="glyphicon glyphicon-map-marker"/>
                <div className="apt-gmp-container">
                    <div ref="gmap" className="g-map"/>
                </div>
            </div>
        )
    }
});

module.exports = ApartmentGMapContainer;