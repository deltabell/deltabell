// Pakcages
var React = require('react');

// Styles
var Styles = require('../css/apartment_summary_item.css');

// Class
var ApartmentSummaryItem = React.createClass({
    render: function(){
        return (
            <div>
                <div className="apr-sum-item-value">{this.props.value}</div>
                <div className="apr-sum-item-title">{this.props.title}</div>
            </div>
        )
    }
});

module.exports = ApartmentSummaryItem;