// Packages
var React = require('react');
var LightBox = require('react-images');

// Styles
var Styles = require('../css/gallery_container.css');

// Class
var GalleryContainer = React.createClass({

    getInitialState: function () {
        return {
            lightBoxIsOpen: false,
            currentImage: 0
        }
    },
    renderBigImage: function(key) {
        var boundClick = this.clickImage.bind(this, key);
        return (
                <div className="row gallery-img-bg">
                    <img src={this.props.images[key].src} onClick={boundClick} className="gallery-disbled-padding-left img-responsive"/>
                </div>
           );  
    },
    renderSmallImage: function(key, isMostRight) {
        var boundClick = this.clickImage.bind(this, key);
        return (
                <div className="row gallery-img-sm">
                    <img src={this.props.images[key].src} 
                    onClick={boundClick} className={isMostRight ? "gallery-disbled-padding-right img-responsive" : "img-responsive"}/>
                </div>
           );  
    },
    closeLightbox: function() {
        this.setState({lightBoxIsOpen: false});
    },
    gotoPrevLightboxImage: function() {
        var currentImage = this.state.currentImage;
        if (currentImage > 0) {
            this.setState({
                currentImage: currentImage - 1
            });
        }
    },
    gotoNextLightboxImage: function() {
        var currentImage = this.state.currentImage;
        if (currentImage < this.props.images.length) {
            this.setState({
                currentImage: currentImage + 1
            });
        }
    },
    clickImage: function (index) {
        this.setState({
            currentImage: index,
            lightBoxIsOpen: true}
        );
    },
    render: function() {
        if (this.props.images.length === 0){return <div/>}
        return (
            <div className="container gallery-container">
                <LightBox 
                        images={this.props.images}
                        currentImage={this.state.currentImage}
                        isOpen={this.state.lightBoxIsOpen}   
                        onClickPrev={this.gotoPrevLightboxImage}
                        onClickNext={this.gotoNextLightboxImage}
                        onClose={this.closeLightbox} 
                        showImageCount={false}
                        backdropClosesModal={true}
                        showThumbnails={true}
                        onClickThumbnail={this.clickImage}/>
                <div className="col-sm-12 col-md-6 gallery-col">
                    {this.renderBigImage(0)}
                </div>
                <div className="col-md-3 gallery-col hidden-sm hidden-xs">
                     {this.renderSmallImage(1)}
                     {this.renderSmallImage(2)}
                </div>
                <div className="col-md-3 gallery-col hidden-sm hidden-xs">
                     {this.renderSmallImage(3, true)}
                     {this.renderSmallImage(4, true)}
                </div>
          </div>
        )
    }
});

module.exports = GalleryContainer;