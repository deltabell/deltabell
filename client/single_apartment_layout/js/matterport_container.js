// Matterport container should contain the matterport viewer
// and handle all events regarding it

// Packages
var React = require('react');
var ApartmentHeader = require('./apartment_header.js');

// Styles
Styles = require('../css/matterport_container.css')

// Matterport Container Class 
var MatterportContainer = React.createClass({

    getMatterportUrl: function(){
        return("https://my.matterport.com/show/?m=" + this.props.matterportKey + "&qs=1&play=1&hl=0");
    },
    render: function(){
        return(
            <div className="delta-matterport-container">
                <iframe height="100%"
                        width="100%"
                        src={this.getMatterportUrl()}
                        frameBorder="0"
                        allowFullScreen/>
            </div>
        )
    }
});

module.exports = MatterportContainer;