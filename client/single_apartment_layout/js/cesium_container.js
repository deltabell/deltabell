// Packages
var React = require('react');
var Axios = require('axios');
var CesiumMap = require('./cesium_map.js');
var ApartmentHeader = require('./apartment_header.js');
var CesiumHelper = require('../../common/js/cesium_helper.js');
var Utils = require('../../common/js/utils.js');
        
// Styles
var Styles = require('../css/cesium_container.css');

// Class
var CesiumContainer = React.createClass({
    getInitialState: function(){
        return({tiles:[]})
    },
    componentDidMount: function(){
        CesiumHelper.getTiles(this.props.apartment.lng, this.props.apartment.lat, this.props.apartment.hgt, function(tiles){
            this.setState({tiles: tiles});
        }.bind(this))
    },
    openExternalTourWindow: function(){
        window.open("/showExternal/" + this.props.apartment._id);
    },
    render: function(){
        if (this.state.tiles.length === 0){   
            return(<div/>);
        }
        return(
            <div className="container">
                <ApartmentHeader title="תלת מימד חיצוני של האיזור" glyphicon="glyphicon glyphicon-picture"/>
                {Utils.isMobile() ? <div>תלת מימד חיצוני אפשרי לצפיה באמצעות מחשב בלבד</div> :
                    <div className="apt-cesium-container">
                        <CesiumMap lat={this.props.apartment.lat} 
                                lng={this.props.apartment.lng} 
                                height={this.props.apartment.hgt} 
                                cameraHeading={this.props.apartment.cameraHeading}
                                tiles={this.state.tiles} />
                        <div className="delta-full-screen-button-container">
                            <span title="הצג במסך מלא" className="glyphicon glyphicon-fullscreen delta-full-screen-button" onClick={this.openExternalTourWindow}/>
                        </div>
                    </div>}
            </div>
        );
    }
})

module.exports = CesiumContainer;
