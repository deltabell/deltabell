// Packages
var React = require('react');

// Styles
var Styles = require('../css/apartment_header.css');

// The apartment Classs
// props:
// 1. header text
// 2. icon glyphicon
var ApartmentHeader = React.createClass({
    render: function(){
        return(
            <h1 className="page-header">
                <div className="row">
                    <div className="col-md-4 hidden-sm hidden-xs" style={{textAlign:"left", fontSize:"18px"}}>
                        <span>
                            {this.props.additionalInfo}
                        </span>
                    </div>
                    <div className="col-md-8">
                        <span className={this.props.glyphicon}>
                        </span>
                        {this.props.title}
                    </div>
                </div>
            </h1>

        )
    }
});

module.exports = ApartmentHeader;

