// packages
var React = require('react');
var BrowserHistory = require('react-router').browserHistory;
var Validator = require('bootstrap-validator');
var Axios = require('axios');

// Styles
var Styles = require('../css/forgot_password_layout.css');

var ForgotPasswordLayout = React.createClass({
    sendPasswordToEmail: function (){
        if (this.refs.emailValid.value === "")
        {

            Axios.get('/api/accounts/generateNewPassword', {
                params: {
                    email: this.refs.email.value
                }
            }).then(function (response) {
                if (response.data.isPasswordSent)
                {
                    this.refs.responseMessage.innerHTML = "<font color=\"green\">" + (response.data.message) + "</font>";
                }
                else
                {
                    this.refs.responseMessage.innerHTML = "<font color=\"red\">" + (response.data.message) + "</font>";
                }
            }.bind(this))
        }
    },
    componentDidMount: function() {
        this.refs.email.focus();
        this.refs.emailValid.value = "";
    },
    render: function(){
        return(
            <div className="container delta-forgot-password-container">
                <div className="row vertical-center-row form-group">
                    <p>נא הזן/הזיני את כתובת הדואר אלקטרוני באמצעותה נרשמת לאתר</p>
                    <form data-toggle="validator" role="form">
                            <div className="col-xs-6 col-xs-offset-6">
                                <input type="email" ref="email" className="form-control"
                                       id="email" required
                                       data-error="נא הזן כתובת דואר אלקטרוני תקינה"/>
                                <div ref="emailValid" className="help-block with-errors"/>
                            </div>
                    </form>

                    <botton type="button" className="btn btn-primary delta-btn" onClick={this.sendPasswordToEmail}>שלח סיסמה חדשה לדואר אלקטרוני</botton>
                    <div ref="responseMessage" className="help-block"/>
                </div>
            </div>
        )
    }
})

module.exports = ForgotPasswordLayout;