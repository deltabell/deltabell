// packages
var React = require('react');
var BrowserHistory = require('react-router').browserHistory;

// STyles
var Styles = require('../css/confirmation_account_layout.css');

var ConfirmationAccountLayout = React.createClass({
    get_div_link_to_continue(){
        if (this.props.params.result === 'success') {
            return <botton type="button" className="btn btn-primary delta-btn" onClick={(e) => {BrowserHistory.push("/")}}>עבור לדף הבית</botton>
        }
        else {
            return <div/>;
        }

    },
    render: function(){
        return(
            <div className="container confirmation-account-container direction-rtl">
                <div className="jumbotron direction-rtl delta-confirmation-message">
                    <p>{this.props.params.message}</p>
                    <p>{this.get_div_link_to_continue()}</p>
                </div>
            </div>
        )
    }
})

module.exports = ConfirmationAccountLayout;