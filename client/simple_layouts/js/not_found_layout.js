// packages
var React = require('react');

// STyles
var Styles = require('../css/not_found_layout.css');

var NotFoundLayout = React.createClass({
    render: function(){
        return(
            <div className="container not-found-container direction-ltr">
                <div className="jumbotron">
                    <h1>404!</h1>
                    <p>Page Not Found</p>
                </div>
            </div>
        )
    }
})

module.exports = NotFoundLayout;