var React = require('react');
var Axios = require('axios');
var CesiumMap = require('../../single_apartment_layout/js/cesium_map.js');
var CesiumHelper = require('../../common/js/cesium_helper.js');
var Styles = require('../css/show_external_layout.css');

var ShowExternalLayout = React.createClass({
    getInitialState: function(){
        return {apartment: null, tiles: null};
    },
    componentDidMount: function(){
        Axios.get('/api/apartments/' + this.props.params.id)
            .then(function (response) {
                // Getting apartment data from server
                var apartment = response.data;
                // Getting relevant tiles
                CesiumHelper.getTiles(apartment.lng, apartment.lat, apartment.hgt, function(tiles){
                    this.setState({tiles: tiles, apartment: apartment});
                }.bind(this))
            }.bind(this));
    },
    render: function(){
        if (!this.state.tiles) { 
            return(<div/>) // need to show waiting animation 
        } else {
            return(
                <div className="show-external-layout">
                    <CesiumMap lat={this.state.apartment.lat} 
                               lng={this.state.apartment.lng} 
                               height={this.state.apartment.hgt} 
                               cameraHeading={this.state.apartment.cameraHeading}
                               tiles={this.state.tiles}/>
                </div>
            )
        }
    }
})

module.exports = ShowExternalLayout;