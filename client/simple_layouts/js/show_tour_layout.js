var React = require('react');
var Axios = require('axios');
var MatterportContainer = require('../../single_apartment_layout/js/matterport_container.js');
var Styles = require('../css/show_tour_layout.css');

var ShowTourLayout = React.createClass({
    getInitialState: function(){
        return {matterportKey: ""};
    },

    // Setting the state from DB
    componentDidMount: function(){
        Axios.get('/api/apartments/' + this.props.params.id)
            .then(function (response) {
                if (response.data.matterportKey)    {
                    this.setState({matterportKey: response.data.matterportKey});
                } 
            }.bind(this));
    },
    getMatterportUrl: function(){
        return("https://my.matterport.com/show/?m=" + this.state.matterportKey + "&qs=1&play=1&brand=0&hl=0");
    },

    render: function(){
        if (this.state.matterportKey===""){return(<div/>)} else {
            return(
                <div className="show-tour-layout">
                    <MatterportContainer matterportKey={this.state.matterportKey}/>
                </div>
            )
        }
    }
})

module.exports = ShowTourLayout;