var React = require('react');
var Styles = require('../css/map_info_street_view.css');

var MapInfoStreetViewComponent = React.createClass({
    componentDidMount: function(){
        var defaultLocation = new google.maps.LatLng(this.props.primaryLocation.lat, this.props.primaryLocation.lng);
        // Creating the panorama
        this.panorama = new google.maps.StreetViewPanorama(this.refs.mapInfoPano, {
            position: defaultLocation,
            pov: {
                heading: 34,
                pitch: 10
            },
            addressControl: false
        });
    },
    componentDidUpdate: function(prevProps, prevState){
        if (prevProps.primaryLocation !== this.props.primaryLocation)
        {
            // changing the panorama
            var centerLatLng = new google.maps.LatLng(this.props.primaryLocation.lat, this.props.primaryLocation.lng)
            this.panorama.setPosition(centerLatLng);
        }
    },
    render: function(){
        return(
            <div className="map-info-pano" ref="mapInfoPano"/>
        )
    }
})

module.exports = MapInfoStreetViewComponent;