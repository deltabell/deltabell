var React = require('react');
var Styles = require('../css/map_info_street_view.css');
var MapInfoStreetViewComponent = require('./map_info_street_view_component.js')
var MapInfoHeader = require('./map_info_header.js');

var MapInfoStreetView = React.createClass({
    render: function(){
        return(
            <div>
                <MapInfoHeader title={"תמונת רחוב"} setOpenTab={this.props.setOpenTab} tab={"streetView"} isOpen={this.props.isOpen}/>
                {this.props.isOpen ? <MapInfoStreetViewComponent primaryLocation={this.props.primaryLocation}/> : <div/>}
            </div>
        )
    }
})

module.exports = MapInfoStreetView;