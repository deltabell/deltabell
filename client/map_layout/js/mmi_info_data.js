var React = require('react');
var Axios = require('axios');
var Styles = require('../css/mmi_info_data.css');
var LoadIcon = require('../../common/js/loading_icon_layout.js');

var MMIInfoData = React.createClass({
    getInitialState: function(){
        return({
            tabaArr: [],
            dataReturned: false
        })
    },
    componentDidMount: function(){
        if (this.props.gush !== -2){
            this.getMMIData();
        }
    },
    componentDidUpdate: function(prevProps, prevState){
        if (prevProps.gush !== this.props.gush | prevProps.parcel !== this.props.parcel)
        {
            if (this.props.gush !== -2){
                this.getMMIData();
            }
        }
    },
    getMMIData: function(){
        // resetting state
        this.setState({
            tabaArr: [],
            dataReturned: false
        })
        // getting the mmi data from api
        let params = {gush: this.props.gush, parcel: this.props.parcel};
        // debug - console.log("getMMIData-sent: gush:" + params.gush + " parcel:" + params.parcel);
        Axios.post('/api/crawlers/getMMIData',params)
        .then(function (response) {
            // debug - console.log("getMMIData-got: gush:" + response.data.gush + " parcel:" + response.data.parcel + " result:" + response.data.result);
            // changing the state only if the gush that returned is the same as the component have
            // this will fix calling multipple calls before answer
            this.setState({
                dataReturned: true,
                tabaArr: response.data.result
            })
        }.bind(this));
    },
    renderIconLinks: function(array){
        var self=this;
        return(
            array.map(function(item){
                if (item.indexOf('.pdf') > -1){
                    return(
                        <span key={item} className="glyphicon glyphicon-file mmi-info-data-glypicon"
                              onClick={(e)=>{self.props.openIFrameModal(item)}} />
                    );
                } else {
                    if (item.indexOf('.zip') > -1){
                        return(
                            <a href={item} target="_blank" key={item}>
                                <span className="glyphicon glyphicon-download-alt mmi-info-data-glypicon" />
                            </a>
                        );
                    } else {
                        return (<div key={item}/>)
                    }
                }
            })
        )
    },
    renderTabaGrid: function(tabaArr){
        var self=this;
        return(
            tabaArr.map(function(taba){return(
                <div className="mmi-info-data-row-container" key={taba.taba}>
                    <div className="row" >
                        <div className="col-xs-4">
                            <div className="mmi-info-header">
                                תיאור
                            </div>
                        </div>
                        <div className="col-xs-4">
                            <div className="mmi-info-header">
                                סטטוס
                            </div>
                        </div>
                        <div className="col-xs-2">
                            <div className="mmi-info-header">
                                מס׳ תב״ע
                            </div>
                        </div>
                        <div className="col-xs-2">
                            <div className="mmi-info-header">
                                ישוב
                            </div>
                        </div>
                    </div>
                    <div className="row" >
                        <div className="col-xs-4">
                            <div className="mmi-info-data">
                                {taba.description}
                            </div>
                        </div>
                        <div className="col-xs-4">
                            <div className="mmi-info-data">
                                {taba.status}
                            </div>
                        </div>
                        <div className="col-xs-2">
                            <div className="mmi-info-data">
                                {taba.taba}
                            </div>
                        </div>
                        <div className="col-xs-2">
                            <div className="mmi-info-data">
                                {taba.city}
                            </div>
                        </div>
                    </div>
                    <div className="row" >
                        <div className="col-xs-2 col-xs-offset-4">
                            <div className="mmi-info-header">
                                ממ״ג
                            </div>
                        </div>
                        <div className="col-xs-2">
                            <div className="mmi-info-header">
                                נספחים
                            </div>
                        </div>
                        <div className="col-xs-2">
                            <div className="mmi-info-header">
                                תשריט
                            </div>
                        </div>
                        <div className="col-xs-2">
                            <div className="mmi-info-header">
                                תקנון
                            </div>
                        </div>
                    </div>
                    <div className="row" >
                        <div className="col-xs-2 col-xs-offset-4">
                            <div className="mmi-info-data">
                                {self.renderIconLinks(taba.mmgList)}
                            </div>
                        </div>
                        <div className="col-xs-2">
                            <div className="mmi-info-data">
                                {self.renderIconLinks(taba.nispachimList)}
                            </div>
                        </div>
                        <div className="col-xs-2">
                            <div className="mmi-info-data">
                                {self.renderIconLinks(taba.tasritList)}
                            </div>
                        </div>
                        <div className="col-xs-2">
                            <div className="mmi-info-data">
                                {self.renderIconLinks(taba.takanonlist)}
                            </div>
                        </div>
                    </div>
                </div>
            )})
        )
    },
    render: function(){
        if (!this.props.isOpen){return <div/>}
        if (!this.state.dataReturned | this.props.gush === -2){
            return (
                <LoadIcon center={true}/>
            )
        }
        if (this.state.dataReturned & this.state.tabaArr === null){
            return (
                <div className="mmi-info">
                    <div className="mmi-info-data">התרחשה תקלת תקשורת, אנא בדקו את חיבור האינטרנט שלכם.</div>
                </div>
            )
        }
        if (this.state.dataReturned & this.state.tabaArr.length == 0){
            return (
                <div className="mmi-info">
                    <div className="mmi-info-data">לא נמצאו נתונים.</div>
                </div>
            )
        }
        return(
            <div>
                {this.renderTabaGrid(this.state.tabaArr)}
            </div>
        );
    }
});

module.exports = MMIInfoData;
