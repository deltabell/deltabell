var React = require('react');
var MavatInfoData = require('./mavat_info_data.js');
var MapInfoHeader = require('./map_info_header.js');

var MavatInfo = React.createClass({
    render: function(){
        var header = <MapInfoHeader title={"תוכניות שאושרו בשנה האחרונה"} setOpenTab={this.props.setOpenTab} tab={"mavat"} isOpen={this.props.isOpen}/>;
        if (this.props.gush==-1){
            return (
                <div>
                    {header}
                    {this.props.isOpen ? <div className="mmi-info"><div className="mmi-info-data">לא נמצאו נתונים.</div></div> : <div/>}
                </div>
            )
        }
        return(
            <div>
                {header}
                <MavatInfoData gush={this.props.gush} parcel={this.props.parcel} isOpen={this.props.isOpen}
                               openIFrameModal={this.props.openIFrameModal}/>
            </div>
        );
    }
});

module.exports = MavatInfo;
