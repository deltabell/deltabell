var React = require('react');
var ReactModal = require('react-modal');

// Styles
Styles = require('../css/iframe_modal.css');

const customStyles = {
    overlay : {
    position : 'fixed',
    top  : -40,
    left : 0,
    right : 0,
    backgroundColor : "rgba(0, 0, 0, 0.8)",
    zIndex: "1031"
  },

  content : {
    position : 'absolute',
    bottom  : 0,
    left : '10%',
    right: '10%',
    overflow  : 'auto',
    WebkitOverflowScrolling : 'touch',
    borderRadius : '4px',
    outline : 'none',
    padding : '0px',
    border: "0px",
    backgroundColor: "rgba(0, 0, 0, 0.8)",
    width: "80%",
    height: "100%"
  }
};

IFrameModal = React.createClass({
    render: function(){
        return(
            <ReactModal
                contentLabel=""
                isOpen={this.props.isModalOpen}
                style={customStyles}
                onRequestClose={this.props.onRequestClose}>
                 <span title="סגור" className="glyphicon glyphicon-remove delta-close-modal-button"
                            onClick={this.props.onRequestClose}/>
                <iframe src={this.props.src} height="100%" width="100%"/>
            </ReactModal>
        )
    }
})

module.exports = IFrameModal;