var React = require('react');
var browserHistory = require('react-router').browserHistory;
var MapLayout = require('./map_layout.js');

var Style = require('../css/map_layout_sec_container.css');

// This componenet uses to check if user is allowed to access the map.
var MapLayoutSecurityContainer = React.createClass({
    componentDidMount: function(){
        if (!this.props.account){
            this.props.setLoginModalState(true, '/map');
        }
    },
    render: function(){
        //notAuthDiv = <div className="mlc-no-access">You are not authorized to view the content of this page</div>;
        notAuthDiv = <div/>;
        if (!this.props.account)
        {
            return(notAuthDiv);
        } 
        else 
        {
            return (<MapLayout primaryLocation={this.props.primaryLocation}
                               account={this.props.account}
                               setPrimaryLocation={this.props.setPrimaryLocation}/>)
        }
    }
})

module.exports = MapLayoutSecurityContainer;