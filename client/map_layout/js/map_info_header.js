// Packages
var React = require('react');

// Styles
var Styles = require('../css/map_info_header.css');

// Class
var MapInfoHeader = React.createClass({
    render: function(){
        return(
            <div className="map-info-header-container" onClick={(e)=>{this.props.setOpenTab(this.props.tab)}}>
                <div className="map-info-header-title">
                    {this.props.title}
                </div>
                <span className={"glyphicon " + (this.props.isOpen ? "glyphicon-menu-up": "glyphicon-menu-down") + " map-info-header-glypicon"}/>
            </div>
        );
    }
});

module.exports = MapInfoHeader;

