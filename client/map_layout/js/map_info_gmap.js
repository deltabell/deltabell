var React = require('react');
var Styles = require('../css/map_info_gmap.css');
var MapInfoGMapComponent = require('./map_info_gmap_component.js')
var MapInfoHeader = require('./map_info_header.js');

var MapInfoGMap = React.createClass({
    render: function(){
        return(
            <div>
                <MapInfoHeader title={"מפה"} setOpenTab={this.props.setOpenTab} tab={"gmap"} isOpen={this.props.isOpen}/>
                <div className={"map-info-gmap-container " + (this.props.isOpen ? "": "hidden")}>
                    <div className="map-info-gmap-container-in">
                        {this.props.isOpen ? <MapInfoGMapComponent primaryLocation={this.props.primaryLocation} 
                                                                   polygon={this.props.polygon}/> : <div/>}
                    </div>
                </div>
            </div>
        )
    }
})

module.exports = MapInfoGMap;