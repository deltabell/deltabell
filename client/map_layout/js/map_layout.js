// Packages
var React = require('react');
var MapInfoLayout = require('./map_info_layout.js');
var Axios = require('axios');
var CesiumHelper = require('../../common/js/cesium_helper.js');
var GeoServices = require('../../common/js/geo_services.js');
var MapToolBar = require('./map_toolbar.js');
var IFrameModal = require('./iframe_modal.js');

Cesium.BingMapsApi.defaultKey = 'Aro7P5Pdawzes8_XJZzLo6opiVjrWdxqv6fpTqJT9F6_Rlapr8ZQf8x8IYSkW1el';
// Styles
var Styles = require('../css/map_layout.css');

var MapLayout = React.createClass({
    getInitialState: function(){
        return({
            isMapInfoExpand: false,
            infoDataOpenTabs: {
                streetView: true,
                mun: false,
                mmi: false,
                gmap: false,
                mapiInfo: true,
                mavat: false
            },
            checkedButtons:{
                polygon: true,
                marker: true,
                rotate: false,
            },
            primaryLocation:{lng: 35.213651,lat: 31.775902, height: 800, N:0, E:0},
            gush: -1,
            parcel: -1,
            polygon: null,
            legalArea: null,
            mapiToken: "",
            areTilesInLocation: true,
            isIFrameModalOpen: false,
            iframeModalSrc: ""
        })
    },
    componentDidMount: function(){
        if (this.props.account){
            Axios.put('/api/visits/addMapPageVisit');
        }

        viewerOptions = {containerDiv: this.refs.deltabellCesiumContainer,
                         maximumZoomDistance: 30000.0,
                         scene3DOnly: false};
        
        this.viewer = CesiumHelper.getViewer(viewerOptions);
        
        // Performance
        this.viewer.scene.globe.maximumScreenSpaceError = 2;
        this.viewer.scene.globe.tileCacheSize = 1000;
        
        this.tilesets = [];
        this.polygonsOnMap = [];
        
        // set camera min\max zoop options
        //this.viewer.scene.screenSpaceCameraController.minimumZoomDistance = 20.0;
        this.viewer.scene.screenSpaceCameraController.maximumZoomDistance = 30000.0;

        this.setDoubleClickEvent();
        this.setClickEvent();
        CesiumHelper.loadTileSets(this.viewer, this.tilesets);
        this.flyToPrimaryLocation();

        // getting mapi token
        Axios.get('/api/configs/mapiToken')
        .then(function (response) {
            this.setState({mapiToken: response.data})
        }.bind(this));

        // Openning help button
        this.openIFrameModal('/map_layout_help.html');
    },
    setDoubleClickEvent: function(){
        // Mouse over the globe to see the cartographic position
        var handler = new Cesium.ScreenSpaceEventHandler(this.viewer.scene.canvas);
        var self = this;
        handler.setInputAction(function(clickedPosition) {
            var scene = self.viewer.scene;
            // pickedOvject for getting the tile
            if (scene.pickPositionSupported) {
                var cartesian = self.viewer.scene.pickPosition(clickedPosition.position);
                if (Cesium.defined(cartesian)) {
                    var cartographic = Cesium.Cartographic.fromCartesian(cartesian);
                    var point = GeoServices.translatePointCoord(Number(Cesium.Math.toDegrees(cartographic.longitude)),
                                                                      Number(Cesium.Math.toDegrees(cartographic.latitude)),
                                                                      'WGS');
                    point.height = Number(cartographic.height);
                    // reset to new position
                    self.setGeoData({
                        gush: -2,
                        parcel: -2,
                        point: point,
                        polygon: null,
                        legalArea: null
                    });
                    // getting new position data
                    GeoServices.getGeoDataByPoint(point,function(data){
                        self.setGeoData(data);
                    });
                } 
            }    
        }, Cesium.ScreenSpaceEventType.LEFT_DOUBLE_CLICK);
        
    },
    setClickEvent: function(){
        var handler = new Cesium.ScreenSpaceEventHandler(this.viewer.scene.canvas);
        var self = this;
        handler.setInputAction(function(clickedPosition) {
            self.stopRotate();
        }, Cesium.ScreenSpaceEventType.LEFT_CLICK);
    },
    componentDidUpdate: function(prevProps, prevState){
        // indicates what have been changed
        var locationChanged = prevState.primaryLocation.lat !== this.state.primaryLocation.lat | 
                              prevState.primaryLocation.lng !== this.state.primaryLocation.lng;
        var parcelChanged = prevState.gush !== this.state.gush | prevState.parcel !== this.state.parcel;
        // removing polygon and marker and drawing if needed
        if ((!this.state.checkedButtons.marker) & prevState.checkedButtons.marker){this.removeMarker()}
        if ((!this.state.checkedButtons.polygon) & prevState.checkedButtons.polygon){this.removePolygons()}
        if ((!this.state.checkedButtons.rotate) & prevState.checkedButtons.rotate){this.stopRotate()}
        if (this.state.areTilesInLocation !== prevState.areTilesInLocation){
            this.viewer.scene.globe.depthTestAgainstTerrain = !this.state.areTilesInLocation;
            this.flyToPrimaryLocation();
        }
        if (locationChanged){
            // checking if there are tiles in the new location
            CesiumHelper.getTiles(this.state.primaryLocation.lng, 
                                  this.state.primaryLocation.lat, 
                                  this.viewer.scene.globe.getHeight(Cesium.Cartographic.fromDegrees(this.state.primaryLocation.lng, this.state.primaryLocation.lat)), function(tiles){
                if ((tiles.length > 0) !== (this.state.areTilesInLocation)){
                    this.setState({areTilesInLocation: tiles.length > 0});
                } else {
                    this.flyToPrimaryLocation();
                }
            }.bind(this));
        }
        // Checking if there is need to draw polygon
        if ((this.state.checkedButtons.polygon & (!prevState.checkedButtons.polygon)) |
             !locationChanged & parcelChanged){
            this.drawPolygon();
        }

        // Checkign if there is need to draw marker
        if ((this.state.checkedButtons.marker & (!prevState.checkedButtons.marker)) |
             !locationChanged & parcelChanged){
            this.drawMarker();
        }

        // Checking if there is need to start rotate
        if (this.state.checkedButtons.rotate & (!prevState.checkedButtons.rotate)){
            this.startRotate();
        }
    },
    flyToPrimaryLocation: function(){
        var location = this.state.primaryLocation;
        var pitch = this.state.areTilesInLocation ? Cesium.Math.toRadians(-45.0) : Cesium.Math.toRadians(-90.0);
        var range = this.state.areTilesInLocation ? 175 : 600;
        // moving the camera to the location recieved
        var camera = this.viewer.camera;
        // getting the height of the position and setting it
        var height = location.height ? location.height : this.viewer.scene.globe.getHeight(Cesium.Cartographic.fromDegrees(location.lng, location.lat));
        var center = new Cesium.Cartesian3.fromDegrees(location.lng,location.lat,height);
        this.cameraCenter = center;
        var sphere = new Cesium.BoundingSphere(center);
        var self = this;
        camera.flyToBoundingSphere(sphere, {
            offset: new Cesium.HeadingPitchRange(camera.heading, pitch ,range),
            complete: function(){
                self.setState({isMapInfoExpand: true})
                self.drawMarker();
                self.drawPolygon();
            }
        });
    },
    removeMarker: function(){
        // removing old line
        if (this.line){
            this.viewer.entities.remove(this.line);
            this.line=null;
            
        }
        if (this.flag){
            this.viewer.entities.remove(this.flag);
            this.flag=null;
        }
    },
    removePolygons: function(){
        // removing old polygons
        for (var i =0; i< this.polygonsOnMap.length; i++){this.viewer.entities.remove(this.polygonsOnMap[i])}
        this.polygonsOnMap = [];
    },
    drawMarker: function(){
        var location = this.state.primaryLocation;
        if (this.state.checkedButtons.marker){
            this.removeMarker();
            var height = location.height ? location.height : this.viewer.scene.globe.getHeight(Cesium.Cartographic.fromDegrees(location.lng, location.lat));
            height = height + 45;
            // adding new line and flag
            if (this.state.areTilesInLocation){
                this.line = this.viewer.entities.add({
                    polyline : {
                        positions : Cesium.Cartesian3.fromDegreesArrayHeights([location.lng, location.lat, height - 45,
                                                                            location.lng, location.lat, height]),
                        width : 0.8,
                        material : new Cesium.PolylineOutlineMaterialProperty({
                            color : Cesium.Color.WHITE,
                            outlineWidth : 0
                        })
                    }
                });
            }
            this.flag = this.viewer.entities.add({
                position : Cesium.Cartesian3.fromDegrees(location.lng, location.lat, height),
                billboard: {
                    image:"/misc/spotlight-poi_hdpi.png",
                    scale: 0.4
                }
            });
        }
    },
    startRotate: function(){
        this.degrees = 0;
        clearInterval(this.interval);
        this.interval = setInterval(() => this.rotateCamera(), 1000 / 24);
    },
    stopRotate: function(){
        clearInterval(this.interval);
        var checkedButtons = JSON.parse(JSON.stringify(this.state.checkedButtons));
        checkedButtons.rotate = false;
        this.setState({checkedButtons: checkedButtons});
    },
    rotateCamera: function(){
        if (this.degrees > 5760)
        {
            this.stopRotate();
        }
        this.degrees = this.degrees + 1;
        this.viewer.camera.rotate(this.cameraCenter, Math.PI / 2880);
    },
    // Showing the polygon on the map
    drawPolygon: function(){
        if (this.state.checkedButtons.polygon){
            this.removePolygons();
            var location = this.state.primaryLocation;
            var height = location.height ? location.height + 10 : this.viewer.scene.globe.getHeight(Cesium.Cartographic.fromDegrees(location.lng, location.lat)) + 20;
            var heightReference = this.state.areTilesInLocation ? Cesium.HeightReference.RELATIVE_TO_GROUND : Cesium.HeightReference.CLAMP_TO_GROUND;
            // loading new polygons to map
            if (this.state.polygon){
                var newPolygon = JSON.parse(JSON.stringify(this.state.polygon));
                // Fixing polygon posityion if needed
                newPolygon = this.state.areTilesInLocation ? newPolygon : CesiumHelper.fixPolygonPosition(newPolygon);
                // for each ring
                for (var j = 0; j < newPolygon.length; j++)
                { 
                    var ring = newPolygon[j];
                    var flatArray = [];
                    for (var k =0; k < ring.length; k ++){
                        flatArray = flatArray.concat([ring[k][1],ring[k][0]]);
                    }
                    var polygonPositionArray = Cesium.Cartesian3.fromDegreesArray(flatArray);

                    // Adding polygon: 
                    this.polygonsOnMap.push(this.viewer.entities.add({
                        polygon : {
                            hierarchy : {
                                positions: polygonPositionArray},
                            heightReference : heightReference,
                            extrudedHeight : height,
                            material : Cesium.Color.BLUE.withAlpha(0.3),
                            closeTop: true
                        }
                    }));    
                }
            }
        }
    },
    getButtonSizeClass: function(){
        var defaultClass="col-xs-1 map-info-button-col ";
        return(this.state.isMapInfoExpand ? "col-xs-offset-7 " + defaultClass : "col-xs-offset-11 " + defaultClass);
    },
    changeMapInfoSize: function(){
        this.setState({
            isMapInfoExpand: !this.state.isMapInfoExpand
        })
    },
    setOpenTab: function(tab){
        var infoDataOpenTabs = JSON.parse(JSON.stringify(this.state.infoDataOpenTabs));
        // changing the state
        infoDataOpenTabs[tab] = !infoDataOpenTabs[tab];
        // Setting new State
        this.setState({infoDataOpenTabs: infoDataOpenTabs});
    },
    renderMapInfoLayout: function(){
        if (this.state.isMapInfoExpand){
            return(
                <div className="map-info-right-container">
                    <div className="col-xs-4 col-xs-offset-8 map-info-background"/>
                    <div className="col-xs-4 col-xs-offset-8 map-info-col">
                        <MapInfoLayout primaryLocation={this.state.primaryLocation}
                                       setPrimaryLocation={this.setPrimaryLocation}
                                       polygon = {this.state.polygon}
                                       gush = {this.state.gush}
                                       parcel = {this.state.parcel}
                                       setOpenTab = {this.setOpenTab}
                                       infoDataOpenTabs = {this.state.infoDataOpenTabs}
                                       mapiToken = {this.state.mapiToken}
                                       legalArea = {this.state.legalArea}
                                       openIFrameModal={this.openIFrameModal}/>
                    </div>
                </div>
            )
        } else {return(<div/>)}
    },
    getGlyphicon: function(){
        return(this.state.isMapInfoExpand ? "glyphicon-menu-right" : "glyphicon-menu-left")
    },
    getGlyphiconDivTitle: function(){
        return this.state.isMapInfoExpand ? "הסתר" : "הצג"
    },
    componentWillUnmount: function(){
        clearInterval(this.interval);
    },
    setGeoData: function(data){
        if (data.parcel){
            this.setState({
                gush: data.gush,
                parcel: data.parcel,
                primaryLocation: data.point,
                polygon: data.polygon,
                legalArea: data.legalArea
            })
        }
    },
    setPrimaryLocation: function(location){
        this.setState({primaryLocation: location})
    },
    setCheckedButtons: function(checkedButtons){
        this.setState({checkedButtons: checkedButtons});
    },
    closeIFrameModal: function(){
        this.setState({isIFrameModalOpen: false})
    },
    openIFrameModal: function(src){
        this.setState({isIFrameModalOpen: true, iframeModalSrc: src})
    },
    render: function(){
        return(
            <div className="delta-bell-cesium-container">
                <IFrameModal src={this.state.iframeModalSrc} isModalOpen={this.state.isIFrameModalOpen} onRequestClose={this.closeIFrameModal}/>
                <MapToolBar primaryLocation={this.state.primaryLocation}
                            setGeoData={this.setGeoData}
                            mapiToken={this.state.mapiToken}
                            setCheckedButtons={this.setCheckedButtons}
                            checkedButtons={this.state.checkedButtons}
                            openIFrameModal={this.openIFrameModal}/>
                <div className="expand-button-container">
                    <div className={this.getButtonSizeClass()}>
                        <div className="map-info-div"  onClick={this.changeMapInfoSize}>
                            <div className="glyphicon-div" title={this.getGlyphiconDivTitle()}>
                                <span className={"glyphicon map-info-div-glyphicon " + this.getGlyphicon()}/>
                            </div>
                        </div>
                    </div>
                </div>
                {this.renderMapInfoLayout()}
                <div id="deltabellCesiumContainer" ref="deltabellCesiumContainer" className="cesium-container"/>
            </div>
        )
    }
})

module.exports = MapLayout;
