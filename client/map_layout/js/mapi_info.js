var React = require('react');
var MapiInfoData = require('./mapi_info_data.js');
var MapInfoHeader = require('./map_info_header.js');
var Styles = require('../css/mapi_info.css');
var LoadIcon = require('../../common/js/loading_icon_layout.js');

var MapiInfo = React.createClass({
    render: function(){
        var header = <MapInfoHeader title={"מידע כללי"} setOpenTab={this.props.setOpenTab} tab={"mapiInfo"} isOpen={this.props.isOpen}/>;
        var gushParcel = <div className="mmi-info-data">
                            <div className="row">
                                <div className="col-xs-3 col-xs-offset-6">
                                    <div className="parcel-info-header">
                                        חלקה:
                                    </div>
                                </div>
                                <div className="col-xs-3">
                                    <div className="parcel-info-header">
                                        גוש:
                                    </div>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col-xs-3 col-xs-offset-6">
                                    <div className="parcel-info-data">
                                        {this.props.parcel}
                                    </div>
                                </div>
                                <div className="col-xs-3 ">
                                    <div className="parcel-info-data">
                                        {this.props.gush}
                                    </div>
                                </div>
                            </div>
                         </div>;
        if (this.props.parcel == -2){
            return(
                <div>
                    <LoadIcon center={true}/>
                    {header}
                </div>       
            )
        }
        if (this.props.parcel == -1){
            return (
                <div>
                    {header}
                    <div className={"parcel-info " + (this.props.isOpen ? "": "hidden")}>
                        <div className="parcel-info-data">לא נמצאו נתונים.</div>    
                    </div>
                </div>
            )
        }
        return(
            <div>
                {gushParcel}
                {header}
                <MapiInfoData gush={this.props.gush} parcel={this.props.parcel} 
                              isOpen={this.props.isOpen} setPrimaryLocation={this.props.setPrimaryLocation}
                              mapiToken={this.props.mapiToken}
                              legalArea={this.props.legalArea}
                              primaryLocation={this.props.primaryLocation}/>
            </div>
        );
    }
})

module.exports = MapiInfo;