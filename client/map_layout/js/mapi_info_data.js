var React = require('react');
var GeoConvert = require('../../common/js/geo_convert.js');
var Axios = require('axios');

var Styles = require('../css/mapi_info_data.css');

var MapiInfoData = React.createClass({
    getInitialState: function(){
        return({
            address: [],
            dataReturned: false,
            locality: "",
            region: "",
            county: ""

        })
    },
    componentDidMount: function(){
        // setting events to for tha mapi api that outer functions could change the component
        $(document).on("ReactComponent:MapiInfoData:handleParcelToAddressResult", this.handleParcelToAddressResult);
        $(document).on("ReactComponent:MapiInfoData:handleSearchLocationByAddress",this.handleSearchLocationByAddress);
        this.getGushData();
        this.getMapiData();
    },
    componentDidUpdate: function(prevProps, prevState){
        if (prevProps.gush !== this.props.gush){
            this.getGushData();
            this.getMapiData();

        } else {
            if (prevProps.parcel !== this.props.parcel){
                this.getMapiData();
            }
        }
    },
    getGushData: function(){
        // Getting gush data
        Axios.get('/api/geo/getGushData/' + this.props.gush)
        .then(function (response, err) {
            if (err | (!response.data)){
                this.setState({
                    locality: "",
                    region: "",
                    county: ""
                })
            } else {
                const data = response.data;
                this.setState({
                    locality: data.LOCALITY_N,
                    region: data.REGION_NAM,
                    county: data.COUNTY_NAM
                })
            }
        }.bind(this));
    },
    getMapiData: function(){
        // resetting state
        this.setState({
            address: [],
            dataReturned: false
        })
        var string = ' גוש ' + this.props.gush + ' חלקה ' + this.props.parcel;
        GovMapGisApi_LotParcelToAddress(this.props.mapiToken, string, "ParcelToAddressResult");
    },
    renderAddress: function(){
        var self = this;
        return(this.state.address.map(function(address, index){
                                        return(<div key={"address" + index} 
                                                    className="parcel-info-data mapi-info-data-address"
                                                    onClick={(e)=>{self.addressGeoCode(address)}}>
                                                    {address}
                                               </div>)
                                      }))
    },
    handleParcelToAddressResult: function(event, object){
        if (object.statusCode == 0){
            this.setState({address: object.result.map(function(address){
                                        return(address[1] + " " + address[2] + ", " + address[0])
                                    }),
                           dataReturned: true})
        } else {
            this.setState({address: [],
                           dataReturned: true})
        }
    },
    addressGeoCode: function(address){
        GovMapGisApi_GeocodeString(this.props.mapiToken, address, "MapiInfoDataAddressGeoCode");
    },
    handleSearchLocationByAddress: function(event,x,y,resultCode,addrList){
        switch (resultCode) {
            case 1:    
                this.navigateToLocation(GeoConvert.convertITMToWGS(Number(x),Number(y)));
                break;
            default:
                break;
        }
    },
    navigateToLocation: function(point){
        this.props.setPrimaryLocation({lng: point.lon, lat: point.lat, height: this.props.primaryLocation.height});
    },
    componentWillUnmount: function(){
        // removing event listeners
        $(document).off("ReactComponent:MapiInfoData:handleParcelToAddressResult");
        $(document).off("ReactComponent:MapiInfoData:handleSearchLocationByAddress");
    },
    renderLegalArea: function(){
        if (this.props.legalArea){
            return(
                <div>
                    <div className="row">
                        <div className="col-xs-3 col-xs-offset-9">
                            <div className="parcel-info-header">
                                שטח רשום (מ״ר)
                            </div>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-xs-3 col-xs-offset-9">
                            <div className="parcel-info-data">
                                {this.props.legalArea}
                            </div>
                        </div>
                    </div>
                </div>
            )
        } else {
            return(<div/>);
        }
    },
    render: function(){
        if (!this.props.isOpen){return <div/>}
        if (this.state.dataReturned & this.state.address === null){
            return (
                <div className="mmi-info">
                    <div className="mmi-info-data">התרחשה תקלת תקשורת, אנא בדקו את חיבור האינטרנט שלכם.</div>
                </div>
            )
        }
        var addressNodes;
        if (this.state.dataReturned){
            if (this.state.address.length > 0){
                addressNodes =  <div>
                                    <div className="parcel-info-header">'כתובות:'</div>
                                    <div>
                                        {this.renderAddress()}
                                    </div>
                                </div>
            } else {
                addressNodes =  <div/>
            }
        } else {
            addressNodes =  <div/>
        }
        return(
            <div>
                <div className={"parcel-info-container " + (this.props.isOpen ? "": "hidden")}>
                    <div className="row">
                        <div className="col-xs-3 col-xs-offset-3">
                            <div className="parcel-info-header">
                                איזור:
                            </div>
                        </div>
                        <div className="col-xs-3">
                            <div className="parcel-info-header">
                                מחוז:
                            </div>
                        </div>
                        <div className="col-xs-3">
                            <div className="parcel-info-header">
                                ישוב:
                            </div>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-xs-3 col-xs-offset-3">
                            <div className="parcel-info-data">
                                {this.state.region}
                            </div>
                        </div>
                        <div className="col-xs-3 ">
                            <div className="parcel-info-data">
                                {this.state.county}
                            </div>
                        </div>
                        <div className="col-xs-3">
                            <div className="parcel-info-data">
                                {this.state.locality}
                            </div>
                        </div>
                    </div>
                    {addressNodes}
                    {this.renderLegalArea()}
                </div>
            </div>
        );
    }
});

module.exports = MapiInfoData;