var React = require('react');
var MunInfoData = require('./mun_info_data.js');
var Styles = require('../css/mun_info.css');
var MapInfoHeader = require('./map_info_header.js');

var MunInfo = React.createClass({
    render: function(){
        var header = <MapInfoHeader title={"מידע עירוני"} setOpenTab={this.props.setOpenTab} tab={"mun"} isOpen={this.props.isOpen}/>;
        if (this.props.gush==-1){
            return (
                <div>
                    {header}
                    {this.props.isOpen ? <div className="mmi-info"><div className="mmi-info-data">לא נמצאו נתונים.</div></div> : <div/>}
                </div>
            )
        }
        return(
            <div>
                {header}
                <MunInfoData gush={this.props.gush} parcel={this.props.parcel} polygon={this.props.polygon}
                             isOpen={this.props.isOpen} primaryLocation={this.props.primaryLocation}
                             openIFrameModal={this.props.openIFrameModal}/>
            </div>
        );
    }
});

module.exports = MunInfo;
