var React = require('react');
var Styles = require('../css/mun_info_data_jerusalem.css');

var MunInfoDataJerusalem = React.createClass({
    renderStreet: function(street, index){
        var address = (street.streetName1 + ' ' + (street.buildingNumber === 0 ? '' : street.buildingNumber.toString()) + ' ' + street.buildingLetter).trim();
        return(
            <div className="mmi-info-data-row-container" key={index}>
                <div className="row">
                    <div className="col-xs-9">
                        <div className="parcel-info-header">
                            כתובת:
                        </div>
                    </div>
                    <div className="col-xs-3">
                        <div className="parcel-info-header">
                            עירייה:
                        </div>
                    </div>
                </div>
                <div className="row">
                    <div className="col-xs-9">
                        <div className="parcel-info-data">
                            {address}
                        </div>
                    </div>
                    <div className="col-xs-3">
                        <div className="parcel-info-data">
                            ירושלים
                        </div>
                    </div>
                </div>
                <div className="row">
                    <div className="col-xs-3">
                        <div className="parcel-info-header">
                            מספר עסקים:
                        </div>
                    </div>
                    <div className="col-xs-3">
                        <div className="parcel-info-header">
                             מספר דירות:
                        </div>
                    </div>
                    <div className="col-xs-3">
                        <div className="parcel-info-header">
                             מספר כניסות:     
                        </div>
                    </div>
                    <div className="col-xs-3">
                        <div className="parcel-info-header">
                            מספר קומות:
                        </div>
                    </div>
                </div>
                <div className="row">
                    <div className="col-xs-3">
                        <div className="parcel-info-data">
                            {street.businessNumber}
                        </div>
                    </div>
                    <div className="col-xs-3">
                        <div className="parcel-info-data">
                            {street.apartmentsNumber}
                        </div>
                    </div>
                    <div className="col-xs-3 ">
                        <div className="parcel-info-data">
                            {street.entranceNumber}
                        </div>
                    </div>
                    <div className="col-xs-3">
                        <div className="parcel-info-data">
                            {street.floorNumber}
                        </div>
                    </div>
                </div>
            </div>
        )
    },
    render: function(){
        var self = this;
        var infoLink = 'https://jergisinfohub.jerusalem.muni.il/UI/GisMeidaT/index.html?gush=' + this.props.gush + '&helka=' + this.props.parcel;
        return(
            <div>
                <div className="jer-btn">
                    <button className="btn btn-primary jer-btn-color" onClick={(e)=>{self.props.openIFrameModal(infoLink)}}>
                        מידע תכנוני כולל על יחידת הקרקע
                    </button>
                </div>
                {this.props.streets.map(function(street, index){return self.renderStreet(street, index)})}
            </div>
        )
    }
})

module.exports = MunInfoDataJerusalem;