var React = require('react');
var MMIInfoData = require('./mmi_info_data.js');
var Styles = require('../css/mmi_info.css');
var MapInfoHeader = require('./map_info_header.js');

var MMIInfo = React.createClass({
    render: function(){
        var header = <MapInfoHeader title={"תוכניות בניין עיר"} setOpenTab={this.props.setOpenTab} tab={"mmi"} isOpen={this.props.isOpen}/>;
        if (this.props.gush==-1){
            return (
                <div>
                    {header}
                    {this.props.isOpen ? <div className="mmi-info"><div className="mmi-info-data">לא נמצאו נתונים.</div></div> : <div/>}
                </div>
            )
        }
        return(
            <div>
                {header}
                <MMIInfoData gush={this.props.gush} parcel={this.props.parcel} isOpen={this.props.isOpen}
                             openIFrameModal={this.props.openIFrameModal}/>
            </div>
        );
    }
});

module.exports = MMIInfo;
