var React = require('react');
var Axios = require('axios');
var LoadIcon = require('../../common/js/loading_icon_layout.js');

var MavatInfoData = React.createClass({
    getInitialState: function(){
        return({
            parcelTabaArr: [],
            gushTabaArr: [],
            dataReturned: false
        })
    },
    componentDidMount: function(){
        if (this.props.gush !== -2){
            this.getMavatData();
        }
    },
    componentDidUpdate: function(prevProps, prevState){
        if (prevProps.gush !== this.props.gush | prevProps.parcel !== this.props.parcel)
        {
            if (this.props.gush !== -2){
                this.getMavatData();
            }
        }
    },
    getMavatData: function(){
        // resetting state
        this.setState({
            parcelTabaArr: [],
            gushTabaArr: [],
            dataReturned: false
        })
        // getting the mavat data from api
        let params = {gush: this.props.gush, parcel: this.props.parcel};
        // debug - console.log("getMMIData-sent: gush:" + params.gush + " parcel:" + params.parcel);
        Axios.post('/api/crawlers/getMavatData',params).then(function (response) {
            this.setState({
                parcelTabaArr: [],
                gushTabaArr: [],
                dataReturned: true
        })}.bind(this));
    },
    render: function(){
        if (!this.props.isOpen){return <div/>}
        if (!this.state.dataReturned | this.props.gush == -2){
            return (
                <LoadIcon center={true}/>
            )
        }
        if (this.state.dataReturned & this.state.parcelTabaArr === null){
            return (
                <div className="mmi-info">
                    <div className="mmi-info-data">התרחשה תקלת תקשורת, אנא בדקו את חיבור האינטרנט שלכם.</div>
                </div>
            )
        }
        if (this.state.dataReturned & this.state.gushTabaArr.length == 0){
            return (
                <div className="mmi-info">
                    <div className="mmi-info-data">לא נמצאו נתונים.</div>
                </div>
            )
        }
        return(
            <div>
                נתונים
            </div>
        );
    }
});

module.exports = MavatInfoData;
