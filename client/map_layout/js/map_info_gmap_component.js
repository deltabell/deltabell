var React = require('react');

var MapInfoGMapComponent = React.createClass({
    componentDidMount: function(){
        var defaultLocation = new google.maps.LatLng(this.props.primaryLocation.lat, this.props.primaryLocation.lng);
        // Creating the Map 
        this.googleMap = new google.maps.Map(this.refs.mapinfogmap, {
            center: defaultLocation,
            zoom: 17,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        });

        // drawing polygon and marker
        this.drawPolygon();
        this.setMarker();     
    },
    componentDidUpdate: function(prevProps, prevState){
        if (prevProps.primaryLocation !== this.props.primaryLocation)
        {
            // Cetering the map
            var centerLatLng = new google.maps.LatLng(this.props.primaryLocation.lat, this.props.primaryLocation.lng)
            this.googleMap.setCenter(centerLatLng);
            this.googleMap.setZoom(17);
        }
        this.drawPolygon();
        this.setMarker();     
    },
    drawPolygon: function(){
        // removing poylgon 
        if (this.polygon){
            this.polygon.setMap(null);
        }

        if (this.props.polygon){
            // Creating the parcel polygon
            if (this.props.polygon.length > 0){
                var polygonCoordinates = [];
                this.props.polygon[0].forEach(function(point){
                    polygonCoordinates.push({lat: point[0], lng: point[1]});
                });
                this.polygon = new google.maps.Polygon({
                    paths: polygonCoordinates,
                    strokeColor: '#0000FF',
                    strokeOpacity: 0.8,
                    strokeWeight: 2,
                    fillColor: '#0000FF',
                    fillOpacity: 0.35
                });
                this.polygon.setMap(this.googleMap);
            }
        }
    },
    setMarker: function(){
        var location = new google.maps.LatLng(this.props.primaryLocation.lat, this.props.primaryLocation.lng);
        // setting marker
        if (!this.marker){
            this.marker = new google.maps.Marker({
                position: location,
                map: this.googleMap
            });
        } else {
            this.marker.setPosition(location);
        }
    },
    render: function(){
        return(
            <div ref="mapinfogmap" className="map-info-gmap"/>
        )
    }
})

module.exports = MapInfoGMapComponent;