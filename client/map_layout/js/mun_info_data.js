var React = require('react');
var Axios = require('axios');
var MunInfoDataJerusalem = require('./mun_info_data_jerusalem.js');
var LoadIcon = require('../../common/js/loading_icon_layout.js');

var Styles = require('../css/mun_info.css');

var MunInfo = React.createClass({
    getInitialState: function(){
        return({
            dataReturned: false,
            streets: []
        })
    },
    componentDidMount: function(){
        if (this.props.gush !== -2){
            this.getStreetsDataByPolygon();
        }
    },
    componentDidUpdate: function(prevProps, prevState){
        if (prevProps.gush !== this.props.gush | prevProps.parcel !== this.props.parcel){
            if (this.props.gush !== -2){
                this.getStreetsDataByPolygon();
            }
        }
    },
    getStreetsDataByPolygon: function(){
        // resetting state
        this.setState({dataReturned: false, streets: []});
        if (this.props.polygon){
            // getting the streets data of the parcel polygon
            Axios.post('/api/geo/getStreetsByPolygon',{polygon: this.props.polygon})
            .then(function (response) {
                var streets = response.data.streets;
                if (streets){
                    this.setState({dataReturned: true, streets: streets})
                } else {
                    this.setState({dataReturned: true, streets: []});
                }
            }.bind(this));
        } else {
            this.setState({dataReturned: true});
        }
    },
    render: function(){
        if (!this.props.isOpen){return <div/>}
        if (!this.state.dataReturned | this.props.gush == -2){
            return (
                <LoadIcon center={true}/>
            )
        }
        if (this.state.dataReturned & (this.state.streets.length === 0)){
            return (
                <div className="mmi-info">
                    <div className="mmi-info-data">לא נמצאו נתונים.</div>
                </div>
            )
        }
        var munComponent =  <div className="mmi-info"><div className="mmi-info-data">לא נמצאו נתונים.</div></div>;
        switch (this.state.streets[0].city){
            case "jerusalem":
                munComponent = <MunInfoDataJerusalem streets={this.state.streets} 
                                                     openIFrameModal={this.props.openIFrameModal}
                                                     gush={this.props.gush}
                                                     parcel={this.props.parcel}/>
                break;
            default:
                break;
        }
        return(     
            <div className={"parcel-info-container " + (this.props.isOpen ? "": "hidden")}>
                {munComponent}
            </div>
        );
    }
});

module.exports = MunInfo;