// Packages
var React = require('react');
var MunInfo = require('./mun_info.js');
var MMIInfo = require('./mmi_info.js');
var MavatInfo = require('./mavat_info.js');
var MapInfoStreetView = require('./map_info_street_view.js');
var MapInfoGmap = require('./map_info_gmap.js');
var MapiInfo = require('./mapi_info.js');
var CesiumHelper = require('../../common/js/cesium_helper.js');

// Styles
var Style = require('../css/map_info_layout.css');

var MapInfoLayout = React.createClass({
    render: function(){
        // Fixing position for show purpose
        var polygon = JSON.parse(JSON.stringify(this.props.polygon));
        if (polygon){
            polygon = CesiumHelper.fixPolygonPosition(polygon);
        }
        var point = JSON.parse(JSON.stringify(this.props.primaryLocation));
        point = CesiumHelper.fixPointPosition(point);
        return (
            <div className="map-info-layout">
                <MapiInfo gush={this.props.gush} parcel={this.props.parcel}
                          isOpen={this.props.infoDataOpenTabs.mapiInfo}
                          setOpenTab={this.props.setOpenTab}
                          mapiToken={this.props.mapiToken}
                          setPrimaryLocation={this.props.setPrimaryLocation}
                          legalArea={this.props.legalArea}
                          primaryLocation={this.props.primaryLocation}/>
                <MapInfoStreetView primaryLocation={this.props.primaryLocation} 
                                   isOpen={this.props.infoDataOpenTabs.streetView}
                                   setOpenTab={this.props.setOpenTab}/>
                <MunInfo parcel={this.props.parcel} gush={this.props.gush}
                         primaryLocation={this.props.primaryLocation}
                         isOpen={this.props.infoDataOpenTabs.mun}
                         setOpenTab={this.props.setOpenTab}
                         polygon={this.props.polygon}
                         openIFrameModal={this.props.openIFrameModal}/>
                <MMIInfo parcel={this.props.parcel} gush={this.props.gush}
                         isOpen={this.props.infoDataOpenTabs.mmi}
                         setOpenTab={this.props.setOpenTab}
                         openIFrameModal={this.props.openIFrameModal}/>
                <MavatInfo parcel={this.props.parcel} gush={this.props.gush}
                           isOpen={this.props.infoDataOpenTabs.mavat}
                           setOpenTab={this.props.setOpenTab}
                           openIFrameModal={this.props.openIFrameModal}/>
                <MapInfoGmap primaryLocation={point} 
                             polygon={polygon}
                             isOpen={this.props.infoDataOpenTabs.gmap}
                             setOpenTab={this.props.setOpenTab}/>
                <div className="map-info-header-container"/>
            </div>
        );
    }
});

module.exports = MapInfoLayout;
