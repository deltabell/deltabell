var React = require('react');
var GeoConvert = require('../../common/js/geo_convert.js')
var GeoServices = require('../../common/js/geo_services.js')

var Styles = require('../css/map_toolbar.css');

var MapToolBar = React.createClass({
    getInitialState: function(){
        return({
            isMouseHover: false,
            isFocues: false
        })
    },
    componentDidMount: function(){
        var self = this;
         // setting the enter key to fire the search
        $("#mapToolBarSearchInput").keyup(function(event){
            if(event.keyCode == 13){
                self.searchLocationByAddress();
            }
        });
        $("#mapToolBarParcelInput").keyup(function(event){
            if(event.keyCode == 13){
                self.searchLocationByGushParcel();
            }
        });
        $("#mapToolBarGushInput").keyup(function(event){
            if(event.keyCode == 13){
                self.searchLocationByGushParcel();
            }
        });

        // setting events to for tha mapi api that outer functions could change the component
        $(document).on("ReactComponent:MapToolBar:handleToolBarSearchLocationByAddress", this.handleToolBarSearchLocationByAddress);

        // setting default tab to be shown
        $('.nav-tabs a[href="#addressSearch"]').tab('show');

        // init the google geocoder
        this.geocoder = new google.maps.Geocoder();
    },
    searchLocationByAddress: function(){
        this.resetSearch();
        GovMapGisApi_GeocodeString(this.props.mapiToken, this.refs.map_toolbar_search_input.value, "ToolBarSearchLocationByAddress");
    },
    searchLocationByGushParcel: function(){
        this.resetSearch();
        var self = this;
        GeoServices.getGeoDataByGushParcel(this.refs.mapToolBarGushInput.value, this.refs.mapToolBarParcelInput.value, 
                                           this.searchNotFound, function(data){
            self.props.setGeoData(data);    
        })
    },
    resetSearch: function(){
        // Reset to new position when new search
        this.props.setGeoData({
            gush: -2,
            parcel: -2,
            point: {lng: this.props.primaryLocation.lng, lat: this.props.primaryLocation.lat},
            polygon: null,
            legalArea: null
        });
    },
    searchNotFound: function(){
        this.props.setGeoData({
            gush: -1,
            parcel: -1,
            point: {lng: this.props.primaryLocation.lng, lat: this.props.primaryLocation.lat},
            polygon: null,
            legalArea: null
        });
    },
    handleToolBarSearchLocationByAddress: function(event, x, y, resultCode, addrList){
        // checking result and according to result handling it
        // 1 - exact match, 2 - not exact match, 3 - not found in mapi trying with google, 4 - multiple results, taking the first one
        switch (resultCode) {
            case 1:
                this.handleAddressPoint(x,y,'ITM');
                break;
            case 2:
                this.handleAddressPoint(x,y,'ITM');
                break;
            case 3:
                this.searchAddressWithGoogle();
                break;
            case 4:
                // seraching again with the first address found
                var newString = addrList[0].replace('כתובת: ','').replace('מוסד: ','');
                if (newString !== this.refs.map_toolbar_search_input.value){
                    this.refs.map_toolbar_search_input.value = newString;
                    this.searchLocationByAddress();
                }
                // not found
                this.searchNotFound();
                break;
            default:
                // not found
                this.searchNotFound();
                break;
        }
    },
    searchAddressWithGoogle: function(){
        var self = this;
        this.geocoder.geocode( { 'address': this.refs.map_toolbar_search_input.value}, function(results, status) {
            if (status == 'OK') {
                self.refs.map_toolbar_search_input.value = results[0].formatted_address;
                self.handleAddressPoint(results[0].geometry.location.lng(), results[0].geometry.location.lat(),'WGS');
            } else {
                // check the address in other place
                self.searchNotFound();
            }
        })
    },
    handleAddressPoint: function(x,y,coordType){
        // Getting other data
        var self = this;
        var point = GeoServices.translatePointCoord(x,y,coordType);
        // Getting new position data
        GeoServices.getGeoDataByPoint(point,function(data){
            self.props.setGeoData(data);
        });
    },
    componentWillUnmount: function(){
        // removing event listeners
        $(document).off("ReactComponent:MapToolBar:handleToolBarSearchLocationByAddress");
    },
    buttonPressed: function(button){
        var checkedButtons = JSON.parse(JSON.stringify(this.props.checkedButtons));
        switch (button){
            case "marker": checkedButtons.marker = !checkedButtons.marker; break;
            case "polygon": checkedButtons.polygon = !checkedButtons.polygon; break;
            case "rotate": checkedButtons.rotate = !checkedButtons.rotate; break;
            case "help": this.props.openIFrameModal('/map_layout_help.html'); break;
        }
        this.props.setCheckedButtons(checkedButtons);
    },
    getTitleForButton: function(buttonName){
        switch(buttonName){
            case "marker": 
                prefix = this.props.checkedButtons.marker ? "הסר " : "הצג ";
                return prefix + "סימון מיקום";
            case "polygon":
                prefix = this.props.checkedButtons.polygon ? "הסתר " : "הצג ";
                return prefix + "תיחום חלקה";
            case "rotate": 
                prefix = this.props.checkedButtons.rotate ? "עצור " : "התחל ";
                return prefix + "סיבוב מפה";
            case "help": 
                return "הצג הוראות שימוש";
        }
    },
    render: function(){
        return(
            <div className="map-toolbar">
                <iframe className="hidden" id="ifrMap" width='0' height='0' src='http://www.govmap.gov.il/Map.aspx?lay=PARCEL_ALL|&laym=retzefMigrashim|PARCEL_ALL|SUB_GUSH_ALL&showBackBtn=1&showNavBtn=1&AllowDrag=1&width=640&height=480&b=0&mapMode=1&in=1'></iframe>
                <div onMouseEnter={(e)=>{this.setState({isMouseHover: true})}} onMouseLeave={(e)=>{this.setState({isMouseHover: false})}}
                     onFocus={(e)=>{this.setState({isFocus: true})}} onBlur={(e)=>{this.setState({isFocus: false})}}>
                    <div className={"map-toolbar-button map-toolbar-search-button " + (this.state.isMouseHover | this.state.isFocus ? "hidden" : "")}>
                        <span className="glyphicon glyphicon-search map-toolbar-search-glyphicon"/>
                    </div>
                    <div className={this.state.isMouseHover | this.state.isFocus ? "" : "hidden"}>
                        <ul className="nav nav-tabs delta-nav-tabs map-toolbar-ul">
                            <li><a className="map-search-tab" href="#addressSearch" data-toggle="tab" id="tabAdressSearch">כתובת</a></li>
                            <li><a className="map-search-tab" href="#parcelSearch" data-toggle="tab" id="tabParcelSearch">גוש חלקה</a></li>
                        </ul>
                        <div id="myTabContent" className="tab-content">
                            <div className="tab-pane" id="addressSearch">
                                <div className="map-toolbar-search-container">
                                    <span onClick={this.searchLocationByAddress} className="glyphicon glyphicon-search map-toolbar-search-glyphicon map-toolbar-search-glyphicon-search-bar"/>
                                    <input ref="map_toolbar_search_input" id="mapToolBarSearchInput" 
                                        className="map-toolbar-search-input" placeholder='רחוב, מספר, עיר'/>
                                </div>
                            </div>
                            <div className="tab-pane" id="parcelSearch">
                                <div className="map-toolbar-search-container">
                                    <span onClick={this.searchLocationByGushParcel} className="glyphicon glyphicon-search map-toolbar-search-glyphicon map-toolbar-search-glyphicon-search-bar"/>
                                    <span className="map-toolbar-parcel-label">גוש:</span>
                                    <span><input ref="mapToolBarGushInput" id="mapToolBarGushInput" type="number" className="map-toolbar-search-parcel-input"/></span>
                                    <span className="map-toolbar-parcel-label">חלקה:</span>
                                    <span><input ref="mapToolBarParcelInput" id="mapToolBarParcelInput" type="number" className="map-toolbar-search-parcel-input"/></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="map-toolbar-button-container">
                    <div className={"map-toolbar-button " + (this.props.checkedButtons.polygon ? "" : "map-toolbar-button-uncheckd")}
                         onClick={(e)=>{this.buttonPressed("polygon")}}>
                        <img title={this.getTitleForButton("polygon")} className="img-responsive" src="/misc/polygon.png"/>
                    </div>
                    <div className={"map-toolbar-button " + (this.props.checkedButtons.marker ? "" : "map-toolbar-button-uncheckd")}
                         onClick={(e)=>{this.buttonPressed("marker")}}>
                        <img title={this.getTitleForButton("marker")} className="map-toolbar-marker-img" src="/misc/spotlight-poi_hdpi.png"/>
                    </div>
                    <div className={"map-toolbar-button rotate-button " + (this.props.checkedButtons.rotate ? "" : "map-toolbar-button-uncheckd")}
                         onClick={(e)=>{this.buttonPressed("rotate")}}>
                         <img title={this.getTitleForButton("rotate")} className="img-responsive" src="/misc/rotate.png"/>
                    </div>
                    <div className="map-toolbar-button" onClick={(e)=>{this.buttonPressed("help")}}>
                         <span className="glyphicon glyphicon-info-sign glyphicon-info-sign-pos-fix" title={this.getTitleForButton("help")}/>
                    </div>
                </div>
            </div>
        )
    }
})

module.exports = MapToolBar;