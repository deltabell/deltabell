// Packages
var React = require('react');
var HomeHeadLine = require('./home_headline.js');
var InformationPanel = require('./information_panel.js');
var Footer = require('../../main_layout/js/footer.js');
var CesiumHelper = require('../../common/js/cesium_helper.js');
var Utils = require('../../common/js/utils.js');
var CesiumHelper = require('../../common/js/cesium_helper.js');
var browserHistory = require('react-router').browserHistory;

// Styles
var Styles = require('../css/home_layout.css');
// data
var exampleApartmentId = '59244065462463e16746ac2e';

// Class
var HomeLayout = React.createClass({
    
    componentDidMount: function(){
        var lng=34.845349383337876;
        var lat=32.311409067926334;
        var hgt=150.04188430152777;

        viewerOptions = {containerDiv: this.refs.homeLayoutCesium}
        this.viewer = CesiumHelper.getViewer(viewerOptions);

        // Getting relevant tiles
        this.tilesArr=[];
        CesiumHelper.getTiles(lng, lat, hgt , function(tiles){
            for (var i = 0; i < tiles.length; i++) {
                this.tilesArr.push(this.viewer.scene.primitives.add(new Cesium.Cesium3DTileset({
                    url: tiles[i],
                    maximumScreenSpaceError : Utils.isMobile() ? 8 : 1, // Temporary workaround for low memory mobile devices - Increase maximum error to 8.
                    maximumNumberOfLoadedTiles : Utils.isMobile() ? 10 : 1000 // Temporary workaround for low memory mobile devices - Decrease (disable) tile cache.
                })));
            }
        }.bind(this));

        // fly and rotate
        this.center = new Cesium.Cartesian3.fromDegrees(lng, lat, hgt);
        var boundingSphere = new Cesium.BoundingSphere(this.center);
        this.viewer.camera.flyToBoundingSphere(boundingSphere, {
            offset: new Cesium.HeadingPitchRange(this.viewer.camera.heading, Cesium.Math.toRadians(-45.0), 140)
        })
        this.interval = setInterval(() => this.rotateCamera(), 1000 / 96);
    },
    rotateCamera: function(){
        this.viewer.camera.rotate(this.center,Math.PI / 2880);
    },
    componentWillUnMount: function(){
        clearInterval(this.interval);
    },
    browseContactUs: function(){
        browserHistory.push('/contactUs');
        window.scrollTo(0,0);
    },
    browseMap: function(){
        if (this.props.account){
            window.open('/map','_self');
        } else {
            this.props.setLoginModalState(true, '/map');
        }
    },
    browseBoard: function(){
        browserHistory.push('/board');
        window.scrollTo(0,0);
    },
    browseDemoApartment: function(){
        browserHistory.push('/singleApartment/' + exampleApartmentId);
        window.scrollTo(0,0);
    },
    browseDemoApartmentTour: function(){
        browserHistory.push('/showTour/' + exampleApartmentId);
        window.scrollTo(0,0);
    },
    render: function(){
        return(
            <div>
                <div className="home-layout-container">
                    <HomeHeadLine/>
                    <InformationPanel/>
                    <div className="home-layout-container-tab home-layout-container-tab-white">
                        <div className="col-md-6 col-md-offset-6">
                            <div className="home-layout-container-tab-title">מערכת מידע נדל״ן ע״ג מפת תלת מימד</div>
                            <div className="home-layout-container-tab-description">
                                <p>מערכת מידע נדל״ן ייחודית הכוללת:</p>
                                <p>מידע ממשלתי, מידע עירוני, מידע משפטי, נסחי טאבו ועוד.</p>
                                <p>הצגת הנכסים בתלת מימד פנורמי.</p>
                                <p>קבלת החלטות מהירה.</p>
                                <button onClick={this.browseMap} className="hidden-sm hidden-xs btn home-layout-btn home-layout-btn-gray">מפה</button>
                                <button onClick={this.browseContactUs} className="btn home-layout-btn home-layout-btn-gray">צור קשר</button>
                            </div>
                        </div>
                        <img className="hidden-xs hidden-sm home-layout-img-left" src="/misc/home_layout_1.png"/>
                    </div>
                    <div className="home-layout-container-tab home-layout-container-tab-gray">
                        <div className="col-xs-12 col-md-6">
                            <div className="home-layout-container-tab-title">לוח דירות בתלת מימד</div>
                            <div className="home-layout-container-tab-description">
                                <p>מידע כללי חיוני.</p>
                                <p>הצגת הנכסים ע״ג מפה אזורית.</p>
                                <p>ממשק ניהול מתקדם.</p>
                                <p>כל הדירות שבלוח עם סיור 360 תלת מימד.</p>
                                <button onClick={this.browseBoard} className="btn home-layout-btn home-layout-btn-white">לוח דירות</button>
                                <button onClick={this.browseDemoApartment} className="btn home-layout-btn home-layout-btn-white">דף דירה לדוגמא</button>
                            </div>
                        </div>
                        <img className="hidden-xs hidden-sm home-layout-img-right" src="/misc/home_layout_2.png"/>
                    </div>
                    <div className="home-layout-container-tab home-layout-container-tab-white">
                        <div className="col-xs-12 col-md-6 col-md-offset-6">
                            <div className="home-layout-container-tab-title">סיורי 360 תלת מימד לכל סוגי הנכסים</div>
                            <div className="home-layout-container-tab-description">
                                <p>צילום נכסים ויצירת סיורי 360 תלת מימד.</p>
                                <p>דירות למכירה/השכרה, משרדים, בתי מלון וצימרים.</p>
                                <p>הפקת תשריט הנכס באופן אוטומטי.</p>
                                <p>נוחות לבעל העסק וללקוח.</p>
                                <button onClick={this.browseContactUs} className="btn home-layout-btn home-layout-btn-gray">הזמן צילום</button>
                                <button onClick={this.browseDemoApartmentTour} className="hidden-md hidden-lg btn home-layout-btn home-layout-btn-gray">סיור לדוגמא</button>
                            </div>
                        </div>
                        <iframe className="hidden-xs hidden-sm home-layout-img-left" src="https://my.matterport.com/show/?m=U8qBN1Stndn&qs=1&play=1&hl=0" frameBorder="0" allowFullScreen/>
                    </div>
                    <div className="home-layout-container-tab home-layout-container-tab-gray">
                        <div className="col-xs-12 col-md-6">
                            <div className="home-layout-container-tab-title">פרוייקטים בבניה בתלת מימד</div>
                            <div className="home-layout-container-tab-description">
                                <p>מודלים של בניינים ע״ג מפת תלת מימד אמיתית.</p>
                                <p>הדמיות של מבנים ודירות בתלת מימד.</p>
                                <button onClick={this.browseContactUs} className="btn home-layout-btn home-layout-btn-white">צור קשר</button>
                            </div>
                        </div>
                        <div className="hidden-xs hidden-sm">
                            <div className="three-dimensions-directives">להזזת המפה - השתמש בלחצן הימני בעכבר, להטייתה - בלחצן הימני<br/> לשינוי מרחק - גלול באמצעות העכבר</div>
                            <div className="hidden-xs hidden-sm home-layout-img-right delta-main-cesium" id="homeLayoutCesium" ref="homeLayoutCesium"/>
                        </div>
                    </div>
                </div>
                <Footer/>
            </div>
        );
    }
});

module.exports = HomeLayout;
