// Packages
var React = require('react');
var browserHistory = require('react-router').browserHistory;

// Styles
var Styles = require('../css/home_headline.css');

// Class
var HomeHeadLine = React.createClass({
    getInitialState: function(){
        return({
            showList: false
        })
    },
    renderList: function(){
        if (this.state.showList){
            return(
                <div className="home-headline-list">
                    <div onClick={this.mapClicked}>מידע נדל״ן</div>
                    <div onClick={this.boardClicked}>דירות למכירה</div>
                </div>
            )
        } else {
            return(<div/>);
        }
    },
    boardClicked: function(){
        browserHistory.push('/board');
        window.scrollTo(0,0);
    },
    mapClicked: function(){
        window.open('/map','_self');
    },
    scrollDown: function(){
        $('html, body').animate({scrollTop: window.screen.availHeight}, 1500);
    },
    render: function(){
        return(
            <div>
                <div>
                    <div className="home-headline-container hidden-xs hidden-sm">
                        <h1 className="home-header">דלתא בל. פשוט. נדל״ן.</h1>
                    </div>
                    <div className="home-headline-container hidden-xs hidden-sm">
                        <input className="home-searchbar" onClick={(e)=>{this.setState({showList: true})}} 
                               placeholder="מה אתה מחפש?" readOnly/>
                    </div>
                    <div className="home-headline-container hidden-xs hidden-sm">
                        {this.renderList()}
                    </div>
                </div>
                <img className="img-responsive home-headline-image" src="/misc/home_page.jpg" onMouseEnter={(e)=>{this.setState({showList: false})}}/>
                <img className="img-responsive home-headline-indicator hidden-xs hidden-sm" src="/misc/indicator_white.png" onClick={this.scrollDown}/>
            </div>
        );
    }
});

module.exports = HomeHeadLine;
