// Packages
var React = require('react');

//Styles
Styles = require('../css/information_panel.css');

// Data
var Sections = [
    {title: "טכנלוגיות חדשניות",        icon: "/misc/3d-small.png"},
    {title: "מידע מעודכן כל הזמן",      icon: "/misc/update-small.png"},
    {title: "מאגרי מידע יחודיים",  icon: "/misc/copy-small.png"}, 
    {title: "נכסי נדל״ן אקסלוסיביים",   icon: "/misc/mansion-small.png"}
]

// Class
var InformationPanel = React.createClass({
    renderSection: function(section){
        return  (
            <div className="col-xs-3" key={section.title}>
                <div className="row panel-section-icon-container">
                    <img src={section.icon} className="img-rounded" />
                </div>
                <div className="row text-center">{section.title}</div>
            </div>
        );
    },

    renderInformationSections: function(sections){
        return (
            <div className="row home-information-panel-sections">
                {sections.map(this.renderSection)}
            </div>
        );
    },

    render: function() {
        return (
            <div className="home-information-panel container-rtl text-center">
                <h2>מידע, טכנולוגיה ונדל״ן - במקום אחד</h2>
                <div className="row">
                    <div className="col-xs-12 col-md-8 col-md-offset-2">
                        {this.renderInformationSections(Sections)}
                    </div>
                </div>
            </div>
        );
    }
});


module.exports = InformationPanel;