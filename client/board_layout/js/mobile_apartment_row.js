// Packages
var React = require('react');

var Utils = require('../../common/js/utils.js');

var Styles = require('../css/mobile_apartment_row.css');

// Class
var BoardGrid = React.createClass({
    getInitialState: function(){
        return({showExtandedInfo: false})
    },
    showHideExtandedInfo: function(apartmentId){
        extandedInfoApartments = this.state.extandedInfoApartments
        if (extandedInfoApartments.includes(apartmentId)){
            extandedInfoApartments.pop(apartmentId)
        } else {
            extandedInfoApartments.push(apartmentId)
        }
    },
    openSingleApartmentPage: function(){
        window.open("/singleapartment/" + this.props.apartment._id);
    },
    openMatterportModal: function(){
        this.props.openModal(this.props.apartment)
    },
    renderExtandedInfo: function(){  
        apartment = this.props.apartment  
        generalInfoList = []
        if (apartment.summary.floor) {generalInfoList.push("קומה: " + " " + apartment.summary.floor)};
        if (apartment.summary.contact) {generalInfoList.push("איש קשר: " + apartment.summary.contact)}
        generalInfoList = generalInfoList.concat(apartment.details.features)
        
        return <div className="row mobile-info-window">
                    <div className="col-xs-2 mobile-info-icons-section">
                        <div className="glyphicon glyphicon-eye-open mobile-grid-eye-icon"
                              title="סיור תלת מימד"
                              onClick={this.openMatterportModal}/> 
                        <div className="glyphicon glyphicon-new-window mobile-grid-show-page-icon"
                              title="הצג דף דירה"
                              onClick={this.openSingleApartmentPage}/>
                    </div>
                    
                    <div className="col-xs-5 mobile-info-data-section"> 
                        {!apartment.summary.price ? <div/> :
                            <div>{"גודל: " + apartment.summary.size + " מטר"}</div>
                        }
                        {!apartment.summary.floor ? <div/> :
                            <div>{"קומה: " + apartment.summary.floor}</div>
                        }
                        {!apartment.createdAt ? <div/> :
                            <div className="apartment-date">{Utils.getFormattedDate(apartment.createdAt, "DD/MM/YYYY")}</div>
                        }
                    </div>
                    <div className="col-xs-5 mobile-info-img-section">
                        <img className="mobile-info-img" src={apartment.images[0].src}
                             onClick={this.openSingleApartmentPage}/>
                    </div>                
                </div>;
    },
    getRowGlyphiconName: function(){
        suffix = this.state.showExtandedInfo ? "upload" : "download"
        return "glyphicon glyphicon-" + suffix      
    },
    render: function(){
        apartment = this.props.apartment
        return(<div>
                      <div className="row board-grid-col-vertical-mobile">
                            <div className="col-xs-2 text-align-left">
                                {apartment.summary.rooms}
                            </div>
                            <div className="col-xs-3">
                                {apartment.summary.price ? Number(apartment.summary.price).toLocaleString() : ""}
                            </div>
                            <div className="col-xs-7">
                                {apartment.apartmentTitle}
                            </div>
                        </div>
                    
                    <div className="row apartment-extanded-info">
                        {this.state.showExtandedInfo ?
                            this.renderExtandedInfo() : <div></div>}
                    </div>
                    <div className={"mobile-down-up-icon " + this.getRowGlyphiconName()}
                         onClick={(e)=>{this.setState({showExtandedInfo: !this.state.showExtandedInfo})}}/>
                </div>)
    }
});

module.exports = BoardGrid;