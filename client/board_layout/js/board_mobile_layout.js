// Packages
var React = require('react');

var BoardHeader = require('./board_header.js');
var BoardMap = require('./board_map.js');
var BoardGrid = require('./board_grid.js');

// Styles
var Styles = require('../css/board_mobile_layout.css');

// const
const APARTMENTS_IN_PAGE=18;

// Class
var BoardMobileLayout = React.createClass({
    getInitialState: function(){
        return {componentToShow: "map"}
    },
    setViewType: function(viewType){
        this.setState({componentToShow: viewType});
    },
    onInputAddressFocus: function(){
        if (this.state.componentToShow == "list"){
            this.setState({componentToShow: "map"});
        }
    },
    getMainComponent: function(){
        switch(this.state.componentToShow){
            case "map":
                return <BoardMap apartments={this.props.getCurrentPageApartment()}
                               setMapFilters={this.props.setMapFilters}
                               primaryLocation={this.props.primaryLocation}
                               setOpenFilter={this.props.setOpenFilter}
                               setPrimaryLocation={this.props.setPrimaryLocation}/>
            case "list":
                return <BoardGrid apartments={this.props.getCurrentPageApartment()} 
                               openModal={this.props.openModal}
                               currentPageNumber={this.props.currentPageNumber}
                               totalPages={this.props.totalPages}
                               setPage={this.props.setPage}
                               apartmentCount={this.props.apartmentCount}
                               setSortMethod={this.props.setSortMethod}
                               sortMethod={this.props.sortMethod}
                               setOpenFilter={this.props.setOpenFilter}
                               gridDisplay="list"/>            
        }                   
    },
    render: function(){
        return <div>            
                    {this.props.getMatterportModal()}   
                    <BoardHeader setFilters={this.props.setFilters} setPrimaryLocation={this.props.setPrimaryLocation}
                                 setViewType={this.setViewType} setOpenFilter={this.props.setOpenFilter} 
                                 openFilter={this.props.openFilter} componentToShow={this.state.componentToShow}
                                 apartmentCount={this.props.apartmentCount}
                                 onInputAddressFocus={this.onInputAddressFocus}/>
                    <div className="row board-body-row">
                        {this.getMainComponent()}
                    </div>
              </div>
    }
});

module.exports = BoardMobileLayout;