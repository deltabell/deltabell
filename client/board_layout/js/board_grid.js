// Packages
var React = require('react');

// Components
var browserHistory = require('react-router').browserHistory;
var BoardGridHeader = require('./board_grid_header.js')
var BoardGridPager = require('./board_grid_pager.js')
var MobileApartmentRow = require('./mobile_apartment_row.js');
var MobileGridTitles = require('./mobile_grid_titles');
var PcGridTitles = require('./pc_grid_titles');

// Helper
var Utils = require('../../common/js/utils.js');

// Styles
var Styles = require('../css/board_grid.css');

// Class
var BoardGrid = React.createClass({
    getInitialState: function(){
        return({
            gridDisplay: "thumb"
        })
    },
    componentDidMount: function(){
        if (this.props.gridDisplay != this.state.gridDisplay){
            this.setGridDisplay(this.props.gridDisplay);
        }
    },
    setGridDisplay: function(display){
        this.setState({
            gridDisplay: display
        })
    },
    openApartmentWindow: function(apartmentId){
        window.open("/singleapartment/" + apartmentId);
    },
    colorMarker: function(id){
        $("#db-info-window-price-" + id).css('background-color','#35089d');
        $("#marker-gly-" + id).css('color','#35089d');
        $("#info-window-" + id).parent().parent().parent().parent().addClass("front");
    },
    unColorMarker: function(id){
        $("#db-info-window-price-" + id).css('background-color','#9d9e9e');
        $("#marker-gly-" + id).css('color','#9d9e9e');
        $("#info-window-" + id).parent().parent().parent().parent().removeClass("front");
    },
    renderThumbApartment: function(apartment){
        var imageSrc = apartment.images[0] ? apartment.images[0].src : "/misc/delta.png";
        return(
            <div className="col-xs-12 col-md-6 " key={apartment._id} 
                 onMouseOver={(e)=>{this.colorMarker(apartment._id)}} onMouseOut={(e)=>{this.unColorMarker(apartment._id)}}>
                <div className="board-grid-thumbnail" onClick={(e)=>{this.openApartmentWindow(apartment._id)}}>  
                    <div className="board-grid-tumbnail-image">
                        <img src={imageSrc}/>
                    </div>
                    <div className="board-grid-thumbnail-caption">
                        <div className="row board-grid-thumnail-row-title">
                            <div className="col-xs-11 ">
                                {apartment.apartmentTitle}
                            </div>
                        </div>
                        <div className="row board-grid-thumnail-row">
                            <div className="col-xs-4 nopadding">
                                {apartment.summary.size ? apartment.summary.size + " מ״ר" : ""}
                            </div>
                            <div className="col-xs-3 nopadding">
                                {apartment.summary.rooms ? apartment.summary.rooms + " חדרים" : ""}
                            </div>
                            <div className="col-xs-4 nopadding">
                                {apartment.summary.price ? Number(apartment.summary.price).toLocaleString() + " ש״ח" : ""}
                            </div>
                        </div>
                        <div className="row board-grid-thumnail-row board-grid-thumnail-row-top-border">
                            <div className="col-xs-1 col-xs-offset-1 glyphicon-eye-hover">
                                 <span className="glyphicon glyphicon-eye-open board-glyphicon-eye-open"
                                       title="סיור תלת מימד"
                                       onClick={(e)=>{
                                           e.cancelBubble = true;
                                           if(e.stopPropagation) {e.stopPropagation()};
                                           this.props.openModal(apartment);}}/>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    },
    renderPcApartmentRow: function(apartment){
        return <div className={Utils.getStyleSuffixForMobile("board-grid-col-vertical")}>
                    <div className="col-xs-1 glyphicon-eye-hover">
                        <span className="glyphicon glyphicon-eye-open board-glyphicon-eye-open"
                              title="סיור תלת מימד"
                              onClick={(e)=>{this.props.openModal(apartment)}}/>
                    </div>
                    <div className="col-xs-2">
                        {apartment.summary.size}
                    </div>
                    <div className="col-xs-1">
                        {apartment.summary.rooms}
                    </div>
                    <div className="col-xs-2">
                        {apartment.summary.price ? Number(apartment.summary.price).toLocaleString() + " ש׳׳ח" : ""}
                    </div>
                    <div className="col-xs-5">
                        {apartment.apartmentTitle}
                    </div>
                    <div className="col-xs-1 glyphicon-eye-hover">
                        <span className="glyphicon glyphicon-new-window board-glyphicon-eye-open"
                              title="הצג דף דירה"
                              onClick={(e)=>{window.open("/singleapartment/" + apartment._id)}}/>
                    </div>
                </div>
    },
    renderListApartment: function(apartment, index){
        var rowClass = index === 0 ? "board-grid-row board-grid-row-first" : "board-grid-row";

        return(
            <div className={rowClass} key={apartment._id}
                 onMouseOver={(e)=>{this.colorMarker(apartment._id)}} 
                 onMouseOut={(e)=>{this.unColorMarker(apartment._id)}} >
                 
                 {Utils.isMobile() ? 
                    <MobileApartmentRow apartment={apartment}
                                        openModal={this.props.openModal}/> 
                    : this.renderPcApartmentRow(apartment)}                 
            </div>
        )
    },
    renderApartments: function(){
        // Checking what display type on
        if (this.state.gridDisplay==="thumb"){
            return(
                this.props.apartments.map(this.renderThumbApartment)
            )
        } else{
            return(
                this.props.apartments.map(this.renderListApartment)
            )
        }
    },
    renderGridTitles: function(){
        if (this.state.gridDisplay==="thumb"){
            return <div/>
        } else{
            return Utils.isMobile() ? <MobileGridTitles/> : <PcGridTitles/>
        }
    },
    render: function(){
        return(
            <div className={Utils.getStyleSuffixForMobile("board-grid")} id="board-grid" onClick={(e)=>{this.props.setOpenFilter(null)}}>
                <BoardGridHeader setGridDisplay={this.setGridDisplay} 
                                apartmentCount={this.props.apartmentCount}
                                sortMethod = {this.props.sortMethod}
                                setSortMethod={this.props.setSortMethod}/>
                {this.renderGridTitles()}
                <div className={Utils.getStyleSuffixForMobile("board-apartments")}>
                    {this.renderApartments()}
                </div>
                <BoardGridPager currentPageNumber={this.props.currentPageNumber}
                                totalPages={this.props.totalPages}
                                setPage={this.props.setPage}/>
            </div>
        );
    }
});

module.exports = BoardGrid;