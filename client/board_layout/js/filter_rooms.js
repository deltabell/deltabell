var React = require('react');

var FilterRooms = React.createClass({
    getInitialState: function(){return({selectedRadio: this.props.rooms})},
    selectRadio: function(id){
        this.setState({selectedRadio: id});
        this.props.setFilterValue("rooms",id);
    },
    render: function(){
        return(
            <div className="board-header-filter-description">
                <div className="board-header-filter-container">
                    <input type="radio" checked={this.state.selectedRadio===undefined}
                            onChange={(e)=>{this.selectRadio(undefined)}}/>
                    <label className="rooms-filter-label">הכל</label>
                    <input type="radio" checked={this.state.selectedRadio==="1"}
                            onChange={(e)=>{this.selectRadio("1")}}/>
                    <label className="rooms-filter-label">1+</label>                            
                    <input type="radio" checked={this.state.selectedRadio==="2"}
                            onChange={(e)=>{this.selectRadio("2")}}/>
                    <label className="rooms-filter-label">2+</label>                            
                    <input type="radio" checked={this.state.selectedRadio==="3"}
                            onChange={(e)=>{this.selectRadio("3")}}/>
                    <label className="rooms-filter-label">3+</label>
                    <input type="radio" checked={this.state.selectedRadio==="4"}
                            onChange={(e)=>{this.selectRadio("4")}}/>
                    <label className="rooms-filter-label">4+</label>                            
                    <input type="radio" checked={this.state.selectedRadio==="5"}
                            onChange={(e)=>{this.selectRadio("5")}}/>
                    <label className="rooms-filter-label">5+</label>
                </div>
            </div>
        )
    }
})

module.exports = FilterRooms;