// Packages
var React = require('react');

// Styles
var Styles = require('../css/board_grid_pager.css');

// Class
var BoardGridPager = React.createClass({
    pageForward: function(){
        if (this.props.currentPageNumber < this.props.totalPages)
        {
            this.props.setPage(this.props.currentPageNumber + 1);
        }
    },
    pageBackward: function(){
        if (this.props.currentPageNumber > 1)
        {
            this.props.setPage(this.props.currentPageNumber - 1);
        }
    },
    render: function(){
        var pageString = "דף " + this.props.currentPageNumber + " מתוך " + this.props.totalPages;
        return(
            <div className="board-grid-pager">
                <div className="container-fluid board-grid-pager-vertical">
                    <div className="col-xs-2 col-xs-offset-1 col-md-offset-2">
                        <button className="btn btn-default board-grid-pager-button" onClick={this.pageForward}>
                            <span className="glyphicon glyphicon-menu-left board-grid-pager-glyphicon"/>
                        </button>
                    </div>
                    <div className="col-xs-6 col-md-4 text-center">
                        {pageString}
                    </div>
                    <div className="col-xs-2">
                        <button className="btn btn-default board-grid-pager-button float-left" onClick={this.pageBackward}>
                            <span className="glyphicon glyphicon-menu-right board-grid-pager-glyphicon"/>
                        </button>
                    </div>
                </div>
            </div>
        );
    }
});

module.exports = BoardGridPager;