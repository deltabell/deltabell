// Packages
var React = require('react');
var FilterPrice = require('./filter_price.js');
var FilterRooms = require('./filter_rooms.js');
var FilterAdvanced = require('./filter_advanced.js');
var BoardMobileFilterContainer = require('./board_mobile_filter_container.js');

var Utils = require('../../common/js/utils.js');

// Styles
var Styles = require('../css/board_header.css');

// Class
var BoardHeader = React.createClass({
    resetFilters: function(){
        var filters = (JSON.parse(JSON.stringify(this.state.filters)));
        filters.priceFrom = undefined;
        filters.priceTo = undefined;
        filters.rooms = undefined;
        filters.features = [];
        this.setState({filters: filters});
        this.props.setFilters(filters);
        this.props.setOpenFilter(null);
    },
    getInitialState: function(){
        return({
            filters: {
                priceFrom: undefined,
                priceTo: undefined,
                rooms: undefined,
                features: []
            },
            showFilter: false
        })
    },
    componentDidMount: function(){
        // Setting the auto complete of the search
        autocomplete = new google.maps.places.Autocomplete(
            (document.getElementById('inputAddress')),
            {types: ['geocode'], 
             componentRestrictions: {country: 'il'}});
        // setting the autocomple place pick event
        autocomplete.addListener('place_changed', this.addressSelected);
    },
    addressSelected: function(){
        // moving the camera and setting bounds
        var place = autocomplete.getPlace();
        if (place.geometry){
            // setting zoom level according to type
            var zoomLevel = 12; // city
            if (place.types){
                place.types.includes("neighborhood") ? zoomLevel=15 : zoomLevel=zoomLevel;
                place.types.includes("route") ? zoomLevel=16 : zoomLevel=zoomLevel;
                place.types.includes("country") ? zoomLevel=7 : zoomLevel=zoomLevel;
            }
            //Setting the map to the new place
            this.props.setPrimaryLocation({lat: place.geometry.location.lat(),
                                           lng: place.geometry.location.lng(),
                                           zoom: zoomLevel});
            }
    },
    getGlyphicon: function(option){
        return(option===this.props.openFilter ? "glyphicon glyphicon-menu-up board-header-glyphicon hidden-xs" : "glyphicon glyphicon-menu-down board-header-glyphicon hidden-xs");
    },
    getGridHeaderStyle: function(option){
        return(option===this.props.openFilter ? "board-grid-header-item header-bold" : "board-grid-header-item");
    },
    setOpenFilter: function(e,filter){
        // canceling propagation
        if(e.stopPropagation) {e.stopPropagation()};

        // Setting the pressed filter state
        this.props.setOpenFilter(this.props.openFilter===filter ? null : filter);
    },
    renderAdvancedFilter: function(){
        return(
            <span className={this.getGridHeaderStyle("advanced")} onClick={(e)=>{this.setOpenFilter(e,"advanced")}}>
                <span className={this.getGlyphicon("advanced")}/>
                <button className="btn btn-default">{"חיפוש מתקדם "}</button>
                <div className="grid-header-filter-text hidden-xs hidden-sm hidden-md"><small><b>
                {this.state.filters.features.length > 0 ? this.state.filters.features.length + " מאפיינים" : ""}
                </b></small></div>
            </span>
        )
    },
    renderRoomsFilter: function(){
        return(
            <span className={this.getGridHeaderStyle("rooms")} onClick={(e)=>{this.setOpenFilter(e,"rooms")}}>
                <span className={this.getGlyphicon("rooms")}/>
                <button className="btn btn-default">{"חדרים "}</button>
                <div className="grid-header-filter-text hidden-xs hidden-sm  hidden-md"><small><b> 
                {this.state.filters.rooms ? this.state.filters.rooms + "+" : ""}
                </b></small></div>
            </span>
        )      
    },
    renderPriceFilter: function(){
        return(
            <span className={this.getGridHeaderStyle("price")} onClick={(e)=>{this.setOpenFilter(e,"price")}}>
                <span className={this.getGlyphicon("price")}/>
                <button className="btn btn-default">{"מחיר  "}</button>
                <div className="grid-header-filter-text hidden-xs hidden-sm hidden-md"><small><b> 
                {(this.state.filters.priceFrom ? " מ: " + Number(this.state.filters.priceFrom).toLocaleString() + " ש״ח" : "") +
                (this.state.filters.priceTo ? " עד: " + Number(this.state.filters.priceTo).toLocaleString() + " ש״ח" : "")}
                </b></small></div>
            </span>
        )
    },
    setFilterValue: function(filter,value){
        var filters = (JSON.parse(JSON.stringify(this.state.filters)));
        // Setting the filter
        switch(filter) {
            case "priceFrom": filters.priceFrom = value; break;
            case "priceTo": filters.priceTo = value; break;
            case "rooms": filters.rooms = value; break;
            case "features": filters.features = value; break;
        }

        // Setting the new filters
        this.setState({filters: filters})
        this.props.setFilters(filters);
    },
    setFeatureFilter: function(feature, isSet){
        var features = (JSON.parse(JSON.stringify(this.state.filters.features)));
        if (!isSet){
            var index = features.indexOf(feature);
            if (index !== -1){
                features.splice(index, 1);
                this.setFilterValue("features",features);
            }
        } else{
            if (features.indexOf(feature) === -1){
                features.push(feature);
                this.setFilterValue("features",features);
            }
        }
    },
    renderFilterDescription: function(){
        switch(this.props.openFilter) {
            case "price":
                return(
                    <FilterPrice setFilterValue={this.setFilterValue} priceFrom={this.state.filters.priceFrom} priceTo={this.state.filters.priceTo}/>
                )
            case "rooms":
                return(
                    <FilterRooms setFilterValue={this.setFilterValue} rooms={this.state.filters.rooms}/>
                )
            case "advanced":
                return(
                    <div className="board-header-filter-description">
                        <FilterAdvanced setFeatureFilter={this.setFeatureFilter} features={this.state.filters.features}/>
                    </div>
                )
        }
    },
    // Rendering the component for pc with big screen
    renderForPc: function(){
        return(
            <div>
                <div className="board-header">
                    <div className="container-fluid board-header-vertical">
                        <div className="hidden-xs col-sm-2">
                            <button title="ביטול מאפייני חיפוש" className="btn btn-default" onClick={this.resetFilters}>נקה</button>
                        </div>
                        <div className="col-xs-7 col-sm-6 nopadding board-header-filter-line">
                            {this.renderPriceFilter()}
                            {this.renderRoomsFilter()}
                            {this.renderAdvancedFilter()}
                        </div>
                        <div className="col-sm-4 col-xs-5">
                            <div className="col-xs-10 col-sm-11 nopadding">
                                <input id="inputAddress"
                                        className="input-grid-header-address"
                                        placeholder="עיר, שכונה, רחוב"/>
                            </div>
                            <div className="col-xs-2 col-sm-1 nopadding">
                                <span className="glyphicon glyphicon-search board-header-glyphicon-search"/>
                            </div>
                        </div>
                    </div>
                </div>
                <div>
                    {this.renderFilterDescription()}
                </div>
            </div>
        );
    },
    getIconName: function(){
        return this.props.componentToShow == 'map' ? "glyphicon-th-list" : "glyphicon-map-marker"
    },
    changeViewType: function(glypiconName){
        if (glypiconName.includes('map')){
            this.props.setViewType('map');
        } else if (glypiconName.includes('list')){
            this.props.setViewType('list');
        }
    },
    closeFilter: function(){
        this.setState({showFilter: false})
    },
    // Rendering filter modal if user push 'filter' icon
    renderFilterModal: function(){
        return !this.state.showFilter ? <div/> : 
            <BoardMobileFilterContainer isModalOpen={true}
                                        setFilters={this.props.setFilters}
                                        closeFilter={this.closeFilter}
                                        setFeatureFilter={this.setFeatureFilter} 
                                        filters={this.state.filters}
                                        setFilterValue={this.setFilterValue}
                                        apartmentCount={this.props.apartmentCount}
                                        resetFilters={this.resetFilters}/>
    },
    // Rendering the component for mobile or small screen
    renderForMobile: function(){
        return (<div className="board-header">
                    {this.renderFilterModal()}
                    <div className="container-fluid board-header-vertical row">
                            <div className="col-xs-2 delta-view-type-glypicon-container">
                                <span className={"glyphicon " + this.getIconName() + " board-header-glyphicon-mobile delta-view-type-glypicon"}
                                       onClick={(e)=>{this.changeViewType(this.getIconName())}}/>
                            </div>
                            <div className="col-xs-2">
                                <span className={Utils.getStyleSuffixForMobile("glyphicon glyphicon-filter board-header-glyphicon")}
                                      onClick={(e)=>{this.setState({showFilter: true})}}/>
                            </div>
                            <div className="col-xs-8">
                               <input id="inputAddress"
                                      className="input-grid-header-address"
                                      placeholder="הזן מקום חיפוש"
                                      onFocus={this.props.onInputAddressFocus}/>
                            </div>
                        </div>

                </div>
                )
    },
    render: function(){
        return <div> 
                    {Utils.isMobile() ? this.renderForMobile() : this.renderForPc()}
                </div>
    }
});

module.exports = BoardHeader;