// Packages
var React = require('react');

// Styles
var Styles = require('../css/board_grid_header.css');

// Class
var BoardGridHeader = React.createClass({
    setDisplayThumb: function(){
        this.props.setGridDisplay("thumb")
    },
    setDisplayList: function(){
        this.props.setGridDisplay("list")
    },
    getSortTitle: function(){
        switch(this.props.sortMethod){
            case "publishDate":{
                return("תאריך פרסום")
            }
            case "priceAsc":{
                return("מחיר, מהזול ליקר")
            }
            case "priceDesc":{
                return("מחיר, מהיקר לזול")
            }
            case "distance":{
                return("מרחק ממרכז המפה")
            }
        }
    },
    render: function(){
        return(
            <div className="board-grid-header">
                <div className="container-fluid board-grid-header-vertical">
                    <div className="col-xs-3 nopadding">
                        <div className="col-xs-12 nopadding">
                            <span className="dropdown-margin-left" title="תצוגת תמונות"><button className="btn btn-default board-grid-header-button"
                                    onClick={this.setDisplayThumb}>
                                <span className="glyphicon glyphicon-th board-grid-header-glyphicon"/>
                            </button></span>
                            <span title="תצוגת רשימה"><button className="btn btn-default board-grid-header-button"
                                    onClick={this.setDisplayList}>
                                <span className="glyphicon glyphicon-align-justify board-grid-header-glyphicon"/>
                            </button></span>
                        </div>
                    </div>
                    <div className="col-xs-7 result-text nopadding">
                        <span>ממויין לפי:   </span>
                        <span className="dropdown rtl dropdown-margin-right">
                            <button className="btn btn-default board-grid-header-button dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                <span className="caret dropdown-margin-left"></span>
                                {this.getSortTitle()}
                            </button>
                            <ul className="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenu1">
                                <li><a onClick={(e)=>{this.props.setSortMethod("publishDate")}}>תאריך פרסום</a></li>
                                <li><a onClick={(e)=>{this.props.setSortMethod("priceDesc")}}>מחיר, מהיקר לזול</a></li>
                                <li><a onClick={(e)=>{this.props.setSortMethod("priceAsc")}}>מחיר, מהזול ליקר</a></li>
                                <li className="hidden-xs"><a onClick={(e)=>{this.props.setSortMethod("distance")}}>מרחק ממרכז המפה</a></li>
                            </ul>
                        </span>
                     </div>
                    <div className="col-xs-2 result-text nopadding">
                        {this.props.apartmentCount + " תוצאות"}
                    </div>
                </div>
            </div>

        );
    }
});

module.exports = BoardGridHeader;