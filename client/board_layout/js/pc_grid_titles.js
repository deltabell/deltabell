React = require('react');

var PcGridTitles = React.createClass({
    render: function(){
        return(<div className="board-grid-row board-grid-title-row" key="board-grid-title">
                    <div className="board-grid-col-vertical text-center">
                        <div className="col-md-1 hidden-xs">
                            3D
                        </div>
                        <div className="col-md-2 hidden-xs">
                            גודל במ״ר
                        </div>
                        <div className="col-xs-2 col-md-1">
                            חדרים
                        </div>
                        <div className="col-xs-3 col-md-2">
                            מחיר בש״ח
                        </div>
                        <div className="col-xs-7 col-md-5">
                            שם הנכס
                        </div>
                        <div className="col-md-1 hidden-xs">
                        </div>
                    </div>
                </div>)
    }
});

module.exports = PcGridTitles;
