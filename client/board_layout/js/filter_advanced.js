var React = require('react');

// Get All Possible features array
var possibleFeatures = require('../../../common/static_data/apartment_feature_list.js').filter(function(feature){
    return (feature.visible === true)
});

var FilterAdvanced = React.createClass({
    getFeatueValue: function(featureName){
        return (this.props.features.includes(featureName) === true)
    },
    setFeatureValue: function(e){
        this.props.setFeatureFilter(e.target.value, e.target.checked);
    },
    render: function(){
        var self=this;
        return(
            <div>
                {possibleFeatures.map(function(feature){
                    return(
                        <div key={feature.key} className="col-xs-4">
                            <label className="col-xs-6 col-xs-offset-2">{feature.name}</label>
                            <input id={feature.name} type="checkbox" value={feature.name} className="col-xs-4"
                                checked={self.getFeatueValue(feature.name)}
                                onChange={self.setFeatureValue}/>
                        </div>
                    )
                })}
            </div>
        )
    }
})

module.exports = FilterAdvanced;