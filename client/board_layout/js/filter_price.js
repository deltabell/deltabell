var React = require('react');

var FilterPrice = React.createClass({
    componentDidMount: function(){
        this.refs.priceFrom.value = this.props.priceFrom;
        this.refs.priceTo.value = this.props.priceTo;
    },
    render: function(){
        return(
            <div className="board-header-filter-description">
                <div className="board-header-filter-container">
                    <input type="number" placeholder="מחיר התחלתי" className="board-header-filter-input" ref="priceFrom"
                           onChange={(e)=>{this.props.setFilterValue("priceFrom",e.target.value)}}/>
                    עד:
                    <input type="number" placeholder="מחיר מירבי" className="board-header-filter-input" ref="priceTo"
                           onChange={(e)=>{this.props.setFilterValue("priceTo",e.target.value)}}/>
                </div>
            </div>
        )
    }
})

module.exports = FilterPrice;