// Packages
var React = require('react');

// Styles
var Styles = require('../css/board_map.css');

// Adding Array Equals to prototype
// Warn if overriding existing method
if(Array.prototype.equals)
    console.warn("Overriding existing Array.prototype.equals. Possible causes: New API defines the method, there's a framework conflict or you've got double inclusions in your code.");
// attach the .equals method to Array's prototype to call it on any array
Array.prototype.equals = function (array) {
    // if the other array is a falsy value, return
    if (!array)
        return false;

    // compare lengths - can save a lot of time 
    if (this.length != array.length)
        return false;

    for (var i = 0, l=this.length; i < l; i++) {
        // Check if we have nested arrays
        if (this[i] instanceof Array && array[i] instanceof Array) {
            // recurse into the nested arrays
            if (!this[i].equals(array[i]))
                return false;       
        }           
        else if (this[i] != array[i]) { 
            // Warning - two different object instances will never be equal: {x:20} != {x:20}
            return false;   
        }           
    }       
    return true;
}
// Hide method from for-in loops
Object.defineProperty(Array.prototype, "equals", {enumerable: false});
////
openModal = function(e){
    e.cancelBubble = true; 
    if(e.stopPropagation) {e.stopPropagation()}; 
    $(document).trigger("ReactComponent:BoardLayout:openModal",e.target.dataset.apartmentid);
}
// Class
var BoardMap = React.createClass({
    componentDidMount: function(){
        //clearing points
        this.points = [];
        // Creating the Map 
        this.map = new google.maps.Map(this.refs.boardGmap, {
            center:  {lat: this.props.primaryLocation.lat, lng: this.props.primaryLocation.lng},
            zoom: this.props.primaryLocation.zoom,
            streetViewControl: false,
            mapTypeControl: false
        });

        // Setting listener to bounds change to query the apartment that within the new bounds
        var self = this;
        this.map.addListener('idle', function() {
            var bounds = self.map.getBounds();
            if (bounds){
                if (!(bounds.b.b === bounds.b.f)){
                    // set primary location
                    self.props.setPrimaryLocation({lat: self.map.getCenter().lat(),
                                                   lng: self.map.getCenter().lng(),
                                                   zoom: self.map.getZoom()});
                    // set bounds filter
                    self.props.setMapFilters({
                        ld:{lng: bounds.b.b, lat: bounds.f.b},
                        ru:{lng: bounds.b.f, lat: bounds.f.f}
                    })
                }
            } 
        });

        // init info window for selected marker
        this.infoWindow = new google.maps.InfoWindow({content: "<div/>"});
        this.map.addListener('click', function() {
            self.infoWindow.close();
            self.props.setOpenFilter(null);
        });

        if (this.props.apartments.length > 0){
           this.updateMarkers();
        }
    },
    componentDidUpdate: function(prevProps, prevState){
        if (prevProps.primaryLocation !== this.props.primaryLocation)
        {
            this.map.setCenter({lat:this.props.primaryLocation.lat, lng: this.props.primaryLocation.lng});
            this.map.setZoom(this.props.primaryLocation.zoom);
        }

        if (!this.props.apartments.equals(prevProps.apartments)){
           this.updateMarkers();
        }
    },
    updateMarkers: function(){
         this.infoWindow.close();
         var self = this;
         // Clearing the markers that were removed
         for (var i =0; i < this.points.length; i ++) {
             var pointFound = false;
             for (var j = 0; j < this.props.apartments.length & (!pointFound); j++){
                 pointFound = this.props.apartments[j]._id === this.points[i]._id;
             }
             if (!pointFound){
                 // removing the marker
                 $('#info-window-' + this.points[i]._id).off('click');
                 this.points[i].marker.setMap(null);
                 this.points[i].infoWindow.close();
                 this.points.splice(i,1);
                 i = i -1;
             }
         }

         // Adding markers that are not allready in the screen
         // only will work when bounds filter are set
         for (var i = 0; i < this.props.apartments.length; i++) {
             var markerFound = false;
             for (var j = 0; j < this.points.length & (!markerFound); j++){
                 markerFound = this.props.apartments[i]._id === this.points[j]._id;
             }
             if (!markerFound){
                 var self = this;
                 // Creating the marker
                 let marker = new google.maps.Marker({
                     position: {lng: this.props.apartments[i].lng, lat: this.props.apartments[i].lat},
                     map: this.map,
                     icon: {
                         path: google.maps.SymbolPath.CIRCLE,
                         scale: 1
                     }
                 });
                 let id = this.props.apartments[i]._id;
                 let apartment = this.props.apartments[i];
                 // Creating info window for the marker to that will show the price
                 let infoWindow = new google.maps.InfoWindow({
                     content: '<div id=info-window-' + id + ' class="db-info-window">' + 
                                 '<div id=db-info-window-price-' + id + ' class="db-info-window-price">' +
                                     '<span class="shekel">&#8362</span> ' + 
                                     '<span>' + Number(this.props.apartments[i].summary.price).toLocaleString() + '</span>' +
                                 '<span id=marker-gly-' + id + ' class="glyphicon glyphicon-triangle-bottom db-marker-glyphicon-triangle-bottom"/>' +
                              '</div>' 
                 });

                 // when the info window is ready in the dom then needs to change styles of default google api divs
                 google.maps.event.addListener(infoWindow, 'domready', function(){
                     // Styles changes for the info window
                     $(".gm-style-iw").prev("div").hide();
                     $(".gm-style-iw").next("div").hide();
                     $("#info-window-" + id).parent().parent().parent().parent().addClass("remove-height");
                     $("#info-window-" + id).hover(function(){
                         $("#db-info-window-price-" + id).css('background-color','#35089d');
                         $("#marker-gly-" + id).css('color','#35089d');
                     }, function(){
                         $("#db-info-window-price-" + id).css('background-color','#9d9e9e');
                         $("#marker-gly-" + id).css('color','#9d9e9e');
                     }),
                     $(".gm-style-iw").hover(function() {
                         $(this).parent().addClass("front");
                     }, function() {
                         $(this).parent().removeClass("front");
                     });
                     
                     // Setting on click event for the info window to open more details info window
                     $("#info-window-" + id).click(function(){
                         let price = apartment.summary.price ? Number(apartment.summary.price).toLocaleString() + " ש״ח    " : "";
                         let size = apartment.summary.size ? apartment.summary.size + " מ״ר" : "";
                         self.infoWindow.setContent(
                             '<div id=main-info-window class="main-info-window" onclick=window.open("/singleapartment/' + id + '"' + ')>' + 
                                 '<span class="main-info-window-img-section">' + 
                                     '<img title="להצגת דף דירה מפורט" class="main-info-window-img" src="' + apartment.images[0].src + '" />' + 
                                 '</span>' +
                                 '<span class="main-info-window-data-section">' +  
                                     '<div class="main-info-window-data-title">' + apartment.apartmentTitle + '</div>' + 
                                     '<div>' + price + '</div>' + 
                                     '<div>' + size + '</div>' + 
                                 '</span>' +
                                 '<span onclick="var event = arguments[0] || window.event; openModal(event)" ' + 
                                 'data-apartmentid="' + apartment._id + '" ' +  
                                 'title="סיור תלת מימד" class="glyphicon glyphicon-eye-open map-glyphicon-eye-open"/>' +

                             '</div>'
                         );
                         self.infoWindow.open(self.map, marker);
                         $(".gm-style-iw").next("div").hide();
                         var gmstyleiw = $("#main-info-window").parent().parent().parent();
                         gmstyleiw.addClass("main-info-window-iw");
                         gmstyleiw.parent().addClass("front");
                     });
                 });
                 infoWindow.open(this.map, marker);
                 this.points.push({_id: this.props.apartments[i]._id,
                                   marker: marker,
                                   infoWindow: infoWindow});
             }
         }
    },
    componentWillUnmount: function(){
        // removing all click event on info windows
        google.maps.event.clearListeners(this.map, 'idle');
        google.maps.event.clearListeners(this.map, 'click');
        for (var i =0; i < this.points.length; i ++) {
            $('#info-window-' + this.points[i]._id).off('click');
        }
    },
    render: function(){
        return(
           <div className="board-gmap-container" id="board-map">
                <div ref="boardGmap" className="board-gmap"/>
           </div>
        );
    }
});

module.exports = BoardMap;