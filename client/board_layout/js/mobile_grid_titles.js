React = require('react');

var MobileGridTitles = React.createClass({
    render: function(){
        return(<div className="board-grid-row board-grid-title-row-mobile" key="board-grid-title">
                    <div className="board-grid-col-vertical-mobile">
                        <div className="col-xs-2 col-md-1">
                            חד׳
                        </div>
                        <div className="col-xs-3 col-md-2">
                            מחיר בש״ח
                        </div>
                        <div className="col-xs-7 col-md-5">
                            שם הנכס
                        </div>
                    </div>
                </div>)
    }
});

module.exports = MobileGridTitles;
