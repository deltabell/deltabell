// Packages
var React = require('react');
var Axios = require('axios');

var BoardHeader = require('./board_header.js');
var BoardMap = require('./board_map.js');
var BoardGrid = require('./board_grid.js');
var MatterportModal = require('./matterport_modal.js');
var Utils = require('../../common/js/utils.js');
var VisitingCodeModal = require('../../common/js/visiting_code_modal.js');
var BoardMobileLayout = require('./board_mobile_layout.js')

// Styles
var Styles = require('../css/board_layout.css');

// const
const APARTMENTS_IN_PAGE=18;

// Class
var BoardLayout = React.createClass({
    getInitialState: function(){
        return ({
            primaryLocation:{lng: 35.213651,lat: 31.775902, zoom: 8},
            apartments: [],
            selectedApartment: null,
            isMatterportModalOpen: false,
            filters: {},
            mapFilters:{
                ld:{lng: null, lat: null},
                ru:{lng: null, lat: null}
            },
            currentPageNumber: 1,
            totalPages: 1,
            filteredApartments: [],
            apartmentCount: 0,
            isApartmentAccessible: false,
            sortMethod: "publishDate",
            openFilter: null,
            isMobile: Utils.isMobile()
        });
    },
    componentDidMount: function(){
        window.addEventListener("resize", this.checkScreenChange);
   
        // Loading all apartments from DB
        Axios.get('/api/apartments/all')
        .then(function (response) {
            if (response.data.apartments)    {
                var apartments = response.data.apartments.filter(function(apartment){return apartment.lat !== 0});
                this.setState({apartments: apartments});
                var filteredApartments = this.getFilteredApartments(this.state.filters, this.state.mapFilters);
                var totalPages = filteredApartments.length > 0 ? Math.round((filteredApartments.length / APARTMENTS_IN_PAGE) + 0.49999999999) : 1;
                this.setState({filteredApartments: filteredApartments,
                                apartmentCount: filteredApartments.length,
                                totalPages: totalPages})
            } else {
                this.setState({apartments: null,
                                filteredApartments: null});
            }
        }.bind(this));
        
        // Registring to be able to be changed from map info window
        $(document).on("ReactComponent:BoardLayout:openModal", this.handleOpenModal);
    },
    // Have to render component if screen width was changed significantly
    checkScreenChange: function(){
        if (Utils.isMobile() != this.state.isMobile){
            this.setState({isMobile: Utils.isMobile});
        }
    },
    setSortMethod: function(sortMethod){
        this.setState({sortMethod: sortMethod});
    },
    setPage: function(pageNumber){
        this.setState({currentPageNumber: pageNumber});
    },
    setOpenFilter: function(openFilter){
        this.setState({openFilter: openFilter});
    },
    setFilters: function(filters)
    {
        // getting the new filtered apartments
        var newFilteredApartments = this.getFilteredApartments(filters, this.state.mapFilters);
        var newTotalPages = Math.round((newFilteredApartments.length / APARTMENTS_IN_PAGE) + 0.49999999999);
        if (newTotalPages === 0) {newTotalPages=1};
        // Set the new state
        this.setState({
            currentPageNumber: this.state.currentPageNumber > newTotalPages ? newTotalPages : this.state.currentPageNumber,
            totalPages: newTotalPages,
            filteredApartments: newFilteredApartments,
            filters: filters,
            apartmentCount: newFilteredApartments.length
        })
    },
    setMapFilters: function(mapFilters)
    {
        // getting the new filtered apartments
        var newFilteredApartments = this.getFilteredApartments(this.state.filters, mapFilters);
        var newTotalPages = Math.round((newFilteredApartments.length / APARTMENTS_IN_PAGE) + 0.49999999999);
        if (newTotalPages === 0) {newTotalPages=1};
        // Set the new state
        this.setState({
            currentPageNumber: this.state.currentPageNumber > newTotalPages ? newTotalPages : this.state.currentPageNumber,
            totalPages: newTotalPages,
            filteredApartments: newFilteredApartments,
            mapFilters: mapFilters,
            apartmentCount: newFilteredApartments.length
        })
    },
    // Filttering the apartments and retuning the filtered apartment array
    getFilteredApartments: function(filters, mapFilters){
        // Filter By location
        var filteredApartments = this.state.apartments.filter(function(apartment){
            return((mapFilters.ld.lng === null) |
                  (apartment.lng >= mapFilters.ld.lng &
                   apartment.lat >= mapFilters.ld.lat &
                   apartment.lng <= mapFilters.ru.lng &
                   apartment.lat <= mapFilters.ru.lat ))
        })

        // Filter By other filters
        if (filters.priceFrom | filters.priceTo | filters.rooms){
            filteredApartments = filteredApartments.filter(function(apartment){
                return(
                    (!filters.priceFrom | apartment.summary.price >= filters.priceFrom) &
                    (!filters.priceTo | apartment.summary.price <= filters.priceTo) &
                    (!filters.rooms | apartment.summary.rooms >= filters.rooms)
                )
            })
        }

        // Filter by features
        if (filters.features){
            filteredApartments = filteredApartments.filter(function(apartment){
                for (var i = 0; i < filters.features.length; i ++)
                {
                    if (!apartment.details.features.includes(filters.features[i])){
                        return false;
                    }
                }
                return true;
            })
        }

        return filteredApartments;
    },
    getCurrentPageApartment: function(){
        // copying the array
        var tempArray =  this.state.filteredApartments.slice();
        
        // Sorting the array by chosen sort
        switch(this.state.sortMethod){
            case "publishDate":{
                tempArray.sort(function(a,b){return new Date(b.createdAt) - new Date(a.createdAt);})
                break;
            }
            case "priceAsc":{
                tempArray.sort(function(a,b){return a.summary.price - b.summary.price;})
                break;
            }
            case "priceDesc":{
                tempArray.sort(function(a,b){return b.summary.price - a.summary.price;})
                break;
            }
            case "distance":{
                // Calculating the distance between center for sort by distance function
                var centerLat = this.state.primaryLocation.lat;
                var centerLng = this.state.primaryLocation.lng;
                tempArray.sort(function(a,b){
                    return(
                        Math.sqrt(Math.pow(a.lng - centerLng, 2) + Math.pow(a.lat - centerLat, 2)) - 
                        Math.sqrt(Math.pow(b.lng - centerLng, 2) + Math.pow(b.lat - centerLat, 2))
                    );
                })
                break;
            }
        }
        
        // Returning the desired page of the array
        return tempArray.slice((this.state.currentPageNumber - 1) * APARTMENTS_IN_PAGE,
                                 this.state.currentPageNumber * APARTMENTS_IN_PAGE)
    },
    closeModal: function(){
        this.setState({isMatterportModalOpen: false,
                       selectedApartment: null,
                       isApartmentAccessible: null});
    },
    openModal: function(apartment){
        this.setState( {selectedApartment: apartment,
                        isMatterportModalOpen: true,
                    } );
    },
    setIsApartmentAccessible: function(isAccessible){
        this.setState( {isApartmentAccessible: isAccessible} );
    },
    getMatterportModal: function(){
        if (!this.state.selectedApartment){
            return <div/>
        }
        else
        {
            if (!this.state.isApartmentAccessible &&
                !Utils.doesApartmentAccessible(this.state.selectedApartment, this.props.account))
            {
                return(<VisitingCodeModal setIsApartmentAccessible={this.setIsApartmentAccessible}
                                          apartment={this.state.selectedApartment}
                                          closeModal={this.closeModal}/>);
            }
            else
            {                              
                return(<MatterportModal isModalOpen={this.state.isMatterportModalOpen}
                                        matterportKey={this.state.selectedApartment.matterportKey}
                                        onRequestClose={this.closeModal}/>);
            }
        }
    },
    setPrimaryLocation: function(location){
        this.setState({primaryLocation: location})
    },
    handleOpenModal: function(e, apartmentId){
        // Setting the apartment
        var selectedApartment = this.state.apartments.filter(function(apartment){return(apartment._id === apartmentId)})[0];
        this.openModal(selectedApartment);
    },
    componentWillUnmount: function(){
        // removing event listeners
        $(document).off("ReactComponent:BoardLayout:openModal");
        window.removeEventListener("resize", this.checkScreenChange);
    },
    getComponentsForMobile: function(){
        return <div>
                    <BoardMobileLayout setMapFilters={this.setMapFilters}
                                       setOpenFilter={this.setOpenFilter}
                                       setPrimaryLocation={this.setPrimaryLocation}
                                       openModal={this.openModal}
                                       currentPageNumber={this.state.currentPageNumber}
                                       totalPages={this.state.totalPages}
                                       setPage={this.setPage}
                                       apartmentCount={this.state.apartmentCount}
                                       setSortMethod={this.setSortMethod}
                                       sortMethod={this.state.sortMethod}
                                       getMatterportModal={this.getMatterportModal}  
                                       setFilters={this.setFilters} 
                                       primaryLocation={this.state.primaryLocation}
                                       getCurrentPageApartment={this.getCurrentPageApartment}/>
              </div>
    },
    getComponentsForPc: function(){
        return <div>  
                    {this.getMatterportModal()}   
                    <BoardHeader setFilters={this.setFilters} setPrimaryLocation={this.setPrimaryLocation}
                                 setOpenFilter={this.setOpenFilter} openFilter={this.state.openFilter}/>
                    <div className="row board-body-row">
                        <div className="hidden-xs col-sm-6 borad-body-col">
                            <BoardMap apartments={this.getCurrentPageApartment()}
                                    setMapFilters={this.setMapFilters}
                                    primaryLocation={this.state.primaryLocation}
                                    setOpenFilter={this.setOpenFilter}
                                    setPrimaryLocation={this.setPrimaryLocation}/>
                        </div>
                        <div className="col-xs-12 col-sm-6 borad-body-col">
                            <BoardGrid apartments={this.getCurrentPageApartment()} 
                                    openModal={this.openModal}
                                    currentPageNumber={this.state.currentPageNumber}
                                    totalPages={this.state.totalPages}
                                    setPage={this.setPage}
                                    apartmentCount={this.state.apartmentCount}
                                    setSortMethod={this.setSortMethod}
                                    sortMethod={this.state.sortMethod}
                                    setOpenFilter={this.setOpenFilter}/>
                        </div>
                    </div>
                </div>
    },
    render: function(){
        return <div>            
                    {Utils.isMobile() ?
                        this.getComponentsForMobile() : this.getComponentsForPc()            
                    }
              </div>
    }
});

module.exports = BoardLayout;