// Pakages
var React = require('react');
var ReactModal = require('react-modal');
var MatterportContainer = require('../../single_apartment_layout/js/matterport_container.js');

// Styles
const customStyles = {
    overlay : {
    position : 'fixed',
    top  : 0,
    left : 0,
    right : 0,
    bottom : 0,
    backgroundColor : "rgba(0, 0, 0, 0.8)",
    zIndex: "1031"
  },

  content : {
    position : 'absolute',
    top  : '100px',
    left : '10%',
    right: '10%',
    overflow  : 'auto',
    WebkitOverflowScrolling : 'touch',
    borderRadius : '4px',
    outline : 'none',
    padding : '0px',
    border: "0px",
    backgroundColor: "rgba(0, 0, 0, 0.8)",
    width: "80%",
    height: "550px"
  }
};

// Class
var MatterportModal = React.createClass({
    render: function(){
        // Setting the key
        return(
            <ReactModal
                contentLabel=""
                isOpen={this.props.isModalOpen}
                style={customStyles}
                onRequestClose={this.props.onRequestClose}>
                <MatterportContainer matterportKey={this.props.matterportKey}/>
            </ReactModal>
        )
    }
})

module.exports = MatterportModal;
  