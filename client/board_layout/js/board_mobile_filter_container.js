// Requiring the packages
var Utils = require('../../common/js/utils.js');
var React = require('react');
var Axios = require('axios');
var ReactModal = require('react-modal');
var BoardMobileFilterModal = require('./board_mobile_filter_modal.js');

Styles = require('../css/board_mobile_filter_modal.css');

const customStyles = {
    overlay : {
    position : 'fixed',
    top  : 0,
    left : 0,
    right : 0,
    bottom : 0,
    backgroundColor : "rgba(0, 0, 0, 0.8)",
    zIndex: "1031"
  },

  content : {
    position : 'absolute',
    top  : '20px',
    left : '10%',
    right: '10%',
    overflow  : 'auto',
    WebkitOverflowScrolling : 'touch',
    borderRadius : '4px',
    outline : 'none',
    border: "0px",
    padding: "0px",
    backgroundColor: "#eff0f1",
    width: "80%",
    height: "90%"
  }
};

var BoardMobileFilterContainer = React.createClass({
    render: function() {
        return(
            <ReactModal 
                contentLabel=""
                isOpen={this.props.isModalOpen}
                style={customStyles}
                onRequestClose={this.props.closeFilter}>
                <BoardMobileFilterModal setFeatureFilter={this.props.setFeatureFilter}
                                        filters={this.props.filters}
                                        setFilterValue={this.props.setFilterValue}
                                        apartmentCount={this.props.apartmentCount}
                                        closeFilter={this.props.closeFilter}
                                        resetFilters={this.props.resetFilters}/>
            </ReactModal>
        )
    }
});

module.exports = BoardMobileFilterContainer;
