// Requiring the packages
var Utils = require('../../common/js/utils.js');
var React = require('react');
var Axios = require('axios');
var ReactModal = require('react-modal');

Styles = require('../css/board_mobile_filter_modal.css');

const customStyles = {
    overlay : {
    position : 'fixed',
    top  : 0,
    left : 0,
    right : 0,
    bottom : 0,
    backgroundColor : "rgba(0, 0, 0, 0.8)",
    zIndex: "1031"
  },

  content : {
    position : 'absolute',
    top  : '20px',
    left : '10%',
    right: '10%',
    overflow  : 'auto',
    WebkitOverflowScrolling : 'touch',
    borderRadius : '4px',
    outline : 'none',
    border: "0px",
    padding: "0px",
    backgroundColor: "#eff0f1",
    width: "80%",
    height: "90%"
  }
};


var BoardMobileFilterModal = React.createClass({
    getInitialState: function(){
        return {
            openFilter: '',
            criteriasPage: 'basic'
        }
    },
    componentDidUpdate: function(){
        this.render();
    },
    closeModal: function(){
        console.log('close modal');
    },
    navigateAnotherCriterias: function(){
        console.log('navigateAnotherCriterias');
    },
    /*,
    getNavigationCriteriasDescription: function(){
        if (this.state.criteriasPage === 'advanced'){
            return 'לקריטריונים נוספים <<';
        } else {
            return 'לקריטריונים קודמים >>';
        }
    },*/
    setFeatureValue: function(featureName){
        this.props.setFeatureFilter(featureName, !this.props.filters.features.includes(featureName));
    },
    getFeatureStyleSuffix: function(featureName){
        return this.props.filters.features.includes(featureName) ? "selected-feature" : "unselected-feature"
    },
    getRoomsNumberStyle: function(roomsNumber){
        prefix = "mobile-rooms-filter-label"
        return prefix + (this.props.filters.rooms === roomsNumber ? " selected" : "");
    },
    getPossibleFeaturesOrderedByLength: function(){
        return Utils.getPossibleFeatures().sort(function(a,b){return a.name.length > b.name.length});
    },
    selectRoomsNumber: function(roomsNumber){
        this.props.setFilterValue("rooms",roomsNumber);
    },
    getCriteriasCount: function(){
        return this.props.filters.features.length
             + (this.props.filters.rooms ? 1 : 0) 
             + (this.props.filters.priceTo ? 1 : 0)
             + (this.props.filters.priceFrom ? 1: 0);
    },
    render: function(){
        self=this;
        return <div className="filter-modal-content">
                    <div className="first-filter-section">
                        <div className="row row-criterias-count">
                            <div className="col-xs-2 glyphicon glyphicon-trash board-header-glyphicon delta-trash-glyphicon"
                                 onClick={(e)=>{self.props.resetFilters()}}/>
                            <div className="col-xs-9 condition-counter-container">
                                {"נבחרו " + this.getCriteriasCount() + " קריטריונים"}
                            </div>
                        </div>
                    </div>
                    <div className="mobile-filter-criterias">
                        <div className="filter-section-title">מספר חדרים</div>
                        <div className="filter-fields-section">
                            <label className={self.getRoomsNumberStyle(undefined)}
                                    onClick={(e)=>{self.selectRoomsNumber(undefined)}}>הכל</label>
                            {['1','2','3','4','5'].map(function(apartmentCount){
                            return(
                                <label key={apartmentCount} className={self.getRoomsNumberStyle(apartmentCount)}
                                    onClick={(e)=>{self.selectRoomsNumber(apartmentCount)}}>{apartmentCount + "+"}</label>
                            )})}
                        </div>
                        <div>
                            <span className="filter-section-title">מחיר</span>
                        </div>
                        
                        <div className="filter-fields-section">
                            <div className="row mobile-board-header-filter-container">
                                <div className="col-xs-9 mobile-price-filter">
                                    <input type="number" placeholder="מחיר התחלתי" className="mobile-board-header-filter-input" ref="priceFrom"
                                    onChange={(e)=>{this.props.setFilterValue("priceFrom",e.target.value)}}/>
                                </div>
                                <div className="col-xs-2 align-right">
                                    מ:
                                </div>
                                <div className="col-xs-9 mobile-price-filter">
                                    <input type="number" placeholder="מחיר מירבי" className="mobile-board-header-filter-input" ref="priceTo"
                                    onChange={(e)=>{this.props.setFilterValue("priceTo",e.target.value)}}/>
                                </div>
                                <div className="col-xs-2 align-right">
                                    עד:
                                </div>
                            </div>
                        </div>

                        <div className="filter-fields-section">
                            <span className="filter-section-title">קריטריונים נוספים</span>
                        </div>

                        {this.getPossibleFeaturesOrderedByLength().map(function(feature){
                            return(
                                <div key={feature.key} className="col-xs-4 feature-item">
                                    <label onClick={(e)=>{self.setFeatureValue(feature.name)}}
                                        className={"feature-text " + self.getFeatureStyleSuffix(feature.name)}>
                                        {feature.name}
                                    </label>
                                </div>
                            )})}
                    </div>

                    <div className='filter-futter'>
                        <div className="col-xs-4 align-left">
                            <div className="glyphicon glyphicon-ok-circle mobile-close-filter-modal"
                                 onClick={self.props.closeFilter}/>
                        </div>
                        <div className="col-xs-8 align-right">
                            <label className="filtered-apartments-count">
                                {"מספר תוצאות: " + this.props.apartmentCount}
                            </label>
                        </div>    
                    </div>
            </div>
    }    
});

module.exports = BoardMobileFilterModal;
