Axios = require('axios');
CameraEventType = Cesium.CameraEventType;

// Private function to get 3D layer from server
var getTilesFromServer = function(x, y, z, callback){
    var location = new Cesium.Cartesian3.fromDegrees(x,y,z);
    Axios.post('/api/geo/getTilesForLocation',{x: location.x, y: location.y, z: location.z})
    .then(function (response) {
        callback(response.data.tiles);
    });
}     

var CesiumHelper = {
    // Returns estimated degrees height by lng & lat 
    getZByXandY: function(x, y, callback){
        // getting the height of the position
        var terrainProvider = new Cesium.CesiumTerrainProvider({
            url : 'https://assets.agi.com/stk-terrain/world'
        });
        var positions = [
            Cesium.Cartographic.fromDegrees(x, y)
        ];

        var promise = Cesium.sampleTerrain(terrainProvider, 11, positions);
        Cesium.when(promise, function(updatedPositions) {
            callback(updatedPositions[0].height);
        })
    },
    // Returns viewer with initial settings, 
    // Individual settings can be added to the viewer after the function returns it
    getViewer: function(options){
        // parameters
        containerDiv = options.containerDiv
        minimumZoomDistance = options.minimumZoomDistance;
        maximumZoomDistance = options.maximumZoomDistance;
        scene3DOnly = options.scene3DOnly === false ? false : true;

        var terrainModels = Cesium.createDefaultTerrainProviderViewModels();
        
        viewer = new Cesium.Viewer(containerDiv, {
	        timeline:false,
            baseLayerPicker: true,
            navigationHelpButton: false,
	        animation:false,
	        vrButton:false,
	        sceneModePicker:false,
            geocoder:false,
	        infoBox:false,
	        scene3DOnly: scene3DOnly,
            homeButton: false,
            fullscreenButton: false,
            imageryProvider: false,
	        terrainProviderViewModels: terrainModels,
	        selectedTerrainProviderViewModel: terrainModels[1]  // Select STK high-res terrain
        });
        viewer.scene.globe.depthTestAgainstTerrain = false;
        
        // set camera min\max zoop options
        if (minimumZoomDistance) {
            viewer.scene.screenSpaceCameraController.minimumZoomDistance = minimumZoomDistance;
        }
        if (maximumZoomDistance) {
            viewer.scene.screenSpaceCameraController.maximumZoomDistance = maximumZoomDistance;
        }
        
        // rotate & tilt map event triggers
        viewer.scene.screenSpaceCameraController.tiltEventTypes = [CameraEventType.MIDDLE_DRAG, 
                                                                   CameraEventType.RIGHT_DRAG]
        
        // change map zoom event triggers
        viewer.scene.screenSpaceCameraController.zoomEventTypes = [CameraEventType.WHEEL, CameraEventType.PINCH]
        
        return viewer;
    },
    // Returns tiles by lng, lat, [height]
    getTiles: function(x, y ,z, callback){
        if (!x || !y) {
            callback(null); 
        } else if (z) {
            getTilesFromServer(x, y, z, callback);
        } else {
            this.getZByXandY(x, y, function(z){
                getTilesFromServer(x, y, z, callback);
            })
        }
    },
    // Loading all tile list from server
    loadTileSets: function(viewer, tilesets){
        Axios.get('/api/configs/tiles_paths_array')
        .then(function (response) {
            for (let i = 0; i<response.data.length; i++) {
                tilesets.push(viewer.scene.primitives.add(new Cesium.Cesium3DTileset({
                    url: response.data[i].replace('data','') + 'tiles_root.json',
                    maximumScreenSpaceError: 2,
                    maximumNumberOfLoadedTiles:1000
                })));
            }
        })
    },
    fixPointPosition(point){
        point.lat = point.lat - 0.000065;
        point.lng = point.lng - 0.00005;
        return point;
    },
    // fixing the polygon positions because of coordinate transformation unaccuracy
    fixPolygonPosition: function(polygon){
        for (var j = 0; j < polygon.length; j++)
        { 
            for (var k =0; k < polygon[j].length; k ++){
                polygon[j][k][0] = polygon[j][k][0] - 0.000065;
                polygon[j][k][1] = polygon[j][k][1] - 0.00005;
            }
        }
        return(polygon);
    }
}

module.exports = CesiumHelper;