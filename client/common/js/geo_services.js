var Axios = require('axios');
var GeoConvert = require('./geo_convert.js');

// data returned as:
//  {
//      point: {N,E,lng,lat,height}
//      gush: 
//      parcel:
//      polygon: {geometry: []}
//  }

var GeoServices = {
    translatePointCoord: function(x,y,coordType){
        // Setting coordination
        let WGS_lng = 0;
        let WGS_lat = 0;
        let ITM_E = 0;
        let ITM_N = 0;
        if (coordType == "ITM"){
            ITM_E = Number(x);
            ITM_N = Number(y);
            let converted = GeoConvert.convertITMToWGS(ITM_E,ITM_N);
            WGS_lng = converted.lon;
            WGS_lat = converted.lat;
        } else {
            WGS_lng = Number(x);
            WGS_lat = Number(y);
            var converted = GeoConvert.convertWGSToITM(WGS_lat,WGS_lng);
            ITM_N = converted.N;
            ITM_E = converted.E;
        }
        return({lat: WGS_lat, lng: WGS_lng, E: ITM_E, N: ITM_N});
    },
    getGeoDataByPoint: function(point,callback){
        // calling the api to get the data
        Axios.post('/api/geo/getGeoDataByPoint',point)
        .then(function (response) {
            callback({
                point: point,
                gush: response.data.gush,
                parcel: response.data.parcel,
                polygon: response.data.polygon,
                legalArea: response.data.legalArea
            })
        });
    },
    getGeoDataByGushParcel: function(gush, parcel,notFoundFunction, callback){
        // calling the api to get the data
        Axios.post('/api/geo/getGeoDataByGushParcel',{gush: gush, parcel: parcel})
        .then(function (response) {
            if (response.data.point){
                callback({
                    point: {lat: response.data.point.lat, lng: response.data.point.lng},
                    gush: gush,
                    parcel: parcel,
                    polygon: response.data.polygon,
                    legalArea: response.data.legalArea
                });
            } else {
                notFoundFunction();
            }
        });
    },
}

module.exports = GeoServices;