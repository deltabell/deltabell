// Packages
var React = require('react');

// Styles
var Styles = require('../css/loading_icon_layout.css');

// Class
var LoadingIconLayout = React.createClass({
    render: function(){
        var iconClass="";
        if (this.props.center){
            iconClass="loading-icon-center"
        }
        return(
            <div className={iconClass}>
                <img src="/misc/loading.svg"/>
            </div>
        );
    }
});

module.exports = LoadingIconLayout;

