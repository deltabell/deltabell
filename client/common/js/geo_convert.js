//====================================================//
module.exports = {									  //
//====================================================//
// returns obj = {lat: num, lon: num}				  //
//====================================================//
    convertITMToWGS: function(E,N){return itm2wgs84(E,N)},	  //
	convertWGSToITM: function(lat,lon){return wgs842itm(lat,lon)}	  //
};													  //
//====================================================//
var PI = Math.PI;
function sin2(x){
    return Math.sin(x)*Math.sin(x);
};
function cos2(x){
    return Math.cos(x)*Math.cos(x);
};
function tan2(x){
    return Math.tan(x)*Math.tan(x);
};
function tan4(x){
    return Math.tan2(x)*Math.tan2(x);
};

var eDatum = {
	eWGS84: 0,
	eGRS80: 1,
	eCLARK80M: 2
};

function DATUM(a,b,f,esq,e,dX,dY,dZ){
	this.a = a ;	// a  Equatorial earth radius
	this.b = b;	// b  Polar earth radius
	this.f = f;	// f= (a-b)/a  Flatenning
	this.esq = esq;	// esq = 1-(b*b)/(a*a)  Eccentricity Squared
	this.e = e;	// sqrt(esq)  Eccentricity
	// deltas to WGS84
	this.dX = dX;
	this.dY = dY;
	this.dZ = dZ;
};

var Datum = [
	// WGS84 data
	new DATUM (
		6378137.0,				// a
		6356752.3142,			// b
		0.0033528106647474805,  // f = 1/298.257223563
		0.006694380004260807,	// esq
		0.0818191909289062, 	// e
		// deltas to WGS84
		0,
		0,
		0
    ),

	// GRS80 data
	new DATUM (
		6378137.0,				// a
		6356752.3141,			// b
		0.003352810681182319,	// f = 1/298.257222101
		0.00669438002290272,	// esq
		0.0818191910428276,		// e
		// deltas to WGS84
		-48,
		55,
		52
    ),

	// Clark 1880 Modified data
	new DATUM (
		6378300.789,			// a
		6356566.4116309,		// b
		0.0034075497672643507,	// f = 1/293.466
		0.006803488139112318,	// esq
		0.08248325975076590,	// e
		// deltas to WGS84
		-235,
		-85,
		264
    )
];

var gGrid = {
	gICS: 0,
	gITM: 1
};

function GRID(lon0,lat0,k0,false_e,false_n){
	this.lon0 = lon0;
	this.lat0 = lat0; 
	this.k0 = k0;
	this.false_e = false_e;
	this.false_n = false_n;
};
	
var Grid = [

	// ICS data
    new GRID
	(
		0.6145667421719,			// lon0 = central meridian in radians of 35.12'43.490"
		0.55386447682762762,		// lat0 = central latitude in radians of 31.44'02.749"
		1.00000,					// k0 = scale factor
		170251.555,					// false_easting
		2385259.0					// false_northing
    ),

	// ITM data
    new GRID
	(
		0.61443473225468920,		// lon0 = central meridian in radians 35.12'16.261"
		0.55386965463774187,		// lat0 = central latitude in radians 31.44'03.817"
		1.0000067,					// k0 = scale factor
		219529.584,					// false_easting
		2885516.9488				// false_northing = 3512424.3388-626907.390
									// MAPI says the false northing is 626907.390, and in another place
									// that the meridional arc at the central latitude is 3512424.3388
    )
];

//=================================================
// WGS84 to Israel New Grid (ITM) conversion
//=================================================
function wgs842itm(lat,lon)
{
	var latr = lat*PI/180;
	var lonr = lon*PI/180;

	// 1. Molodensky WGS84 -> GRS80
	var location  = {lat: latr, lon: lonr, olat:0, olon:0}
	Molodensky(location,eDatum.eWGS84,eDatum.eGRS80);

	// 2. Lat/Lon (GRS80) -> Local Grid (ITM)
	var ITMPoint = LatLon2Grid(location.olat,location.lon,eDatum.eGRS80,gGrid.gITM);

	// amra fix !!!
	ITMPoint.E = ITMPoint.E - 73;
	return ITMPoint;
}

//=================================================
// Israel New Grid (ITM) to WGS84 conversion
//=================================================
function itm2wgs84(E, N){
	// 1. Local Grid (ITM) -> GRS80
	var location = Grid2LatLon(N,E,gGrid.gITM,eDatum.eGRS80);

	// 2. Molodensky GRS80->WGS84
	Molodensky(location,eDatum.eGRS80,eDatum.eWGS84);
// location: lat,lon,olat,olon
	// final results
	return {lat: (location.olat*180/PI),
	        lon: (location.olon*180/PI)};
}

//====================================
// Local Grid to Lat/Lon conversion
//====================================
function Grid2LatLon(N, E,from,to)
{
	//================
	// GRID -> Lat/Lon
	//================

	var y = N + Grid[from].false_n;
	var x = E - Grid[from].false_e;
	var M = y / Grid[from].k0;

	var a = Datum[to].a;
	var b = Datum[to].b;
	var e = Datum[to].e;
	var esq = Datum[to].esq;

	var mu = M / (a*(1 - Math.pow(e,2)/4 - 3*Math.pow(e,4)/64 - 5*Math.pow(e,6)/256));
	
	var ee = Math.sqrt(1-esq);
	var e1 = (1-ee)/(1+ee);
	var j1 = 3*e1/2 - 27*e1*e1*e1/32;
	var j2 = 21*e1*e1/16 - 55*e1*e1*e1*e1/32;
	var j3 = 151*e1*e1*e1/96;
	var j4 = 1097*e1*e1*e1*e1/512;

	// Footprint Latitude
	var fp =  mu + j1*Math.sin(2*mu) + j2*Math.sin(4*mu) + j3*Math.sin(6*mu) + j4*Math.sin(8*mu); 

	var sinfp = Math.sin(fp);
	var cosfp = Math.cos(fp);
	var tanfp = sinfp/cosfp;
	var eg = (e*a/b);
	var eg2 = eg*eg;
	var C1 = eg2*cosfp*cosfp;
	var T1 = tanfp*tanfp;
	var R1 = a*(1-e*e) / Math.pow(1-(e*sinfp)*(e*sinfp),1.5);
	var N1 = a / Math.sqrt(1-(e*sinfp)*(e*sinfp));
	var D = x / (N1*Grid[from].k0);

	var Q1 = N1*tanfp/R1;
	var Q2 = D*D/2;
	var Q3 = (5 + 3*T1 + 10*C1 - 4*C1*C1 - 9*eg2*eg2)*(D*D*D*D)/24;
	var Q4 = (61 + 90*T1 + 298*C1 + 45*T1*T1 - 3*C1*C1 - 252*eg2*eg2)*(D*D*D*D*D*D)/720;
	// result lat
	var lat = fp - Q1*(Q2-Q3+Q4);

	var Q5 = D;
	var Q6 = (1 + 2*T1 + C1)*(D*D*D)/6;
	var Q7 = (5 - 2*C1 + 28*T1 - 3*C1*C1 + 8*eg2*eg2 + 24*T1*T1)*(D*D*D*D*D)/120;
	// result lon
	var lon = Grid[from].lon0 + (Q5 - Q6 + Q7)/cosfp;

    return {lat: lat, lon: lon};
}

//======================================================
// Abridged Molodensky transformation between 2 datums
//======================================================
function Molodensky(location,from,to)
{
	// from->WGS84 - to->WGS84 = from->WGS84 + WGS84->to = from->to
	var dX = Datum[from].dX - Datum[to].dX;
	var dY = Datum[from].dY - Datum[to].dY;
	var dZ = Datum[from].dZ - Datum[to].dZ;

	var slat = Math.sin(location.lat);
	var clat = Math.cos(location.lat);
	var slon = Math.sin(location.lon);
	var clon = Math.cos(location.lon);
	var ssqlat = Math.pow(slat,2);

	//dlat = ((-dx * slat * clon - dy * slat * slon + dz * clat)
	//        + (da * rn * from_esq * slat * clat / from_a)
	//        + (df * (rm * adb + rn / adb )* slat * clat))
	//       / (rm + from.h); 

	var from_f = Datum[from].f;
	var df = Datum[to].f - from_f;
	var from_a = Datum[from].a;
	var da = Datum[to].a - from_a;
	var from_esq = Datum[from].esq;
	var adb = 1.0 / (1.0 - from_f);
	var rn = from_a / Math.sqrt(1 - from_esq * ssqlat);
	var rm = from_a * (1 - from_esq) / Math.pow((1 - from_esq * ssqlat),1.5);
	var from_h = 0.0; // we're flat!

	var dlat = (-dX*slat*clon - dY*slat*slon + dZ*clat
				   + da*rn*from_esq*slat*clat/from_a +
				   + df*(rm*adb + rn/adb)*slat*clat) / (rm+from_h);

	// result lat (radians)
	location.olat = location.lat+dlat;
	// dlon = (-dx * slon + dy * clon) / ((rn + from.h) * clat);
	var dlon = (-dX*slon + dY*clon) / ((rn+from_h)*clat);
	// result lon (radians)
	location.olon = location.lon+dlon;
}

//====================================
// Lat/Lon to Local Grid conversion
//====================================
function LatLon2Grid(lat,lon,from,to)
{
	// Datum data for Lat/Lon to TM conversion
	var a = Datum[from].a;
	var e = Datum[from].e; 	// sqrt(esq);
	var b = Datum[from].b;

	//===============
	// Lat/Lon -> TM
	//===============
	var slat1 = Math.sin(lat);
	var clat1 = Math.cos(lat);
	var clat1sq = clat1*clat1;
	var tanlat1sq = slat1*slat1 / clat1sq;
	var e2 = e*e;
	var e4 = e2*e2;
	var e6 = e4*e2;
	var eg = (e*a/b);
	var eg2 = eg*eg;

	var l1 = 1 - e2/4 - 3*e4/64 - 5*e6/256;
	var l2 = 3*e2/8 + 3*e4/32 + 45*e6/1024;
	var l3 = 15*e4/256 + 45*e6/1024;
	var l4 = 35*e6/3072;
	var M = a*(l1*lat - l2*Math.sin(2*lat) + l3*Math.sin(4*lat) - l4*Math.sin(6*lat));
	//double rho = a*(1-e2) / pow((1-(e*slat1)*(e*slat1)),1.5);
	var nu = a / Math.sqrt(1-(e*slat1)*(e*slat1));
	var p = lon - Grid[to].lon0;
	var k0 = Grid[to].k0;
	// y = northing = K1 + K2p2 + K3p4, where
	var K1 = M*k0; 
	var K2 = k0*nu*slat1*clat1/2; 
	var K3 = (k0*nu*slat1*clat1*clat1sq/24)*(5 - tanlat1sq + 9*eg2*clat1sq + 4*eg2*eg2*clat1sq*clat1sq);
	// ING north
	var Y = K1 + K2*p*p + K3*p*p*p*p - Grid[to].false_n;
 
	// x = easting = K4p + K5p3, where
	var K4 = k0*nu*clat1; 
	var K5 = (k0*nu*clat1*clat1sq/6)*(1 - tanlat1sq + eg2*clat1*clat1); 
	// ING east
	var X = K4*p + K5*p*p*p + Grid[to].false_e;

	// final rounded results
	return({E:(X+0.5), N: (Y+0.5)});
}
