var possibleFeatures = require('../../../common/static_data/apartment_feature_list.js')

var mobileAgents = {
    Android: function() {
        return navigator.userAgent.match(/Android/i);
    },
    BlackBerry: function() {
        return navigator.userAgent.match(/BlackBerry/i);
    },
    iOS: function() {
        return navigator.userAgent.match(/iPhone|iPad|iPod/i);
    },
    Opera: function() {
        return navigator.userAgent.match(/Opera Mini/i);
    },
    Windows: function() {
        return navigator.userAgent.match(/IEMobile/i) || navigator.userAgent.match(/WPDesktop/i);
    },
    any: function() {
        return (mobileAgents.Android() || mobileAgents.BlackBerry() || mobileAgents.iOS() || mobileAgents.Opera() || mobileAgents.Windows());
    }
}

var Utils = {
    getFormattedDate: function(mongoDbDate, format='YYYY-MM-DD')
    {
        ISRAELY_CLOCK_GAP_IN_HOURS = 0;

        if (!mongoDbDate){
            return '';
        } else {
            // convert to date because it easier to converting
            var d = new Date(mongoDbDate); 
            // function to make day & month in 2 digits always: 1/2/2014 => 01/02/2014
            function pad(s) { return (s < 10) ? '0' + s : s; }
            
            switch(format){
                case 'YYYY-MM-DD':
                    return [d.getFullYear(), pad(d.getMonth()), pad(d.getDate())].join('-');
                case 'DD/MM/YYYY':
                    return [pad(d.getDate()), pad(d.getMonth()), d.getFullYear()].join('/');
                case 'DD/MM/YYYY':
                    return [pad(d.getDate()), pad(d.getMonth()), d.getFullYear()].join('/');
                case 'DD/MM/YYYY hh:mm':
                    date = [pad(d.getDate()), pad(d.getMonth()), d.getFullYear()];
                    time = [pad(d.getHours() + ISRAELY_CLOCK_GAP_IN_HOURS), 
                            pad(d.getMinutes())];                    
                    return time.join(':') + " " + date.join('/');               
                default:
                    console.log('unhandled date format: ' + format);
                    return '';
            }
        }            
    },
    // Get All Possible features array
    getPossibleFeatures: function(){
        return possibleFeatures.filter(function(feature){
            return (feature.visible === true);
        })
    },
    getPhotographyDateMessage: function(apartment){
        return !apartment.photographyDate ? '' :
            "תאריך צילום: " + this.getFormattedDate(apartment.photographyDate, "DD/MM/YYYY");
    },
    doesApartmentAccessible: function(apartment, account){
        if (!apartment.visitingCode){
            return true;
        }
        else if (account){
            return account.userType == 'admin' || account.id == apartment.ownerId;
        }
        else { // visitor isn't authenticated
            return false;
        }
    },
    getSuccessHtmlMessage: function(message){
        return "<font color='green'>"+message+"</font>";
    },
    getFailureHtmlMessage: function(message){
        return "<font color='red'>"+message+"</font>";
    },
    // Returns if visitor device is mobile
    isMobile: function(){
        return mobileAgents.any() != null || screen.width <= 500 || window.innerWidth <= 500;
    },
    getStyleSuffixForMobile: function(prefix){
        return Utils.isMobile() ? prefix + "-mobile" : prefix;
    }
};


module.exports = Utils;

