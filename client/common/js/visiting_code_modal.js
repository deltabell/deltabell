// Requiring the packages
var React = require('react');
var Axios = require('axios');

Styles = require('../css/visiting_code_modal.css');

// LoginPageLayout2 Class
var LoginModal = React.createClass({
    getInitialState: function(){
        return {};
    },
    componentDidMount: function(){
        $('#visitingCodeModal').modal('show');
        $('#visitingCodeModal').on('shown.bs.modal', (e)=>{
            $('#messageVisitingCode').html("");
            this.state.isAuthenticated = false;
        });

        $('#visitingCodeModal').on("hidden.bs.modal", (e)=>{
            if (!this.state.isAuthenticated && this.props.closeModal)
            {
                this.props.closeModal()
            }
        });
    },
    componentDidUpdate: function(){
        $('#visitingCodeModal').modal('show');
    },
    visitApartment: function(){
        var enteredVisitingCode = this.refs.inputVisitingCode.value;
        
        if (!enteredVisitingCode){
            $('#visitingCodeModal').modal('show');
            this.refs.messageVisitingCode.innerHTML = "<font color=\"redֿֿֿֿֿֿ\">נא הזן קוד</font>";
        }
        else if (enteredVisitingCode != this.props.apartment.visitingCode){
            this.refs.messageVisitingCode.innerHTML = "<font color=\"redֿֿֿֿֿֿ\">הקוד אינו תקין</font>";
        }
        else {
            this.state.isAuthenticated = true;
            $('#visitingCodeModal').modal('hide');
            this.props.setIsApartmentAccessible(true);
        }
    },
    getAdvertiserDetails: function(){
        var contactDetails = [this.props.apartment.contactName,
                              this.props.apartment.contactPhoneNumber,
                              this.props.apartment.contactEmail];
        contactDetails = contactDetails.filter(contactDetail => contactDetail);
        var contactDetailsString = contactDetails.join('  -  ');
        return contactDetailsString;
    },
    render: function() {
        return(<div id="visitingCodeModal" className="modal custom fade" role="dialog">
                    <div className="modal-dialog" id="visitingCodeModal">
                        <div className="modal-body">
                            <div className="well delta-visiting-code-div">
                                <div className="row form-group">
                                    <div className="col-xs-1">
                                        <button type="button" className="close" onClick={(e)=>{$('#visitingCodeModal').modal('hide');}}
                                                data-dismiss="modal" aria-hidden="true">X</button>
                                    </div>
                                </div>  
                                <div className="row form-group">                      
                                    <label>צפיה בפרטי דירה זו אפשרית רק לאחר הזנת קוד אבטחה</label>
                                </div>
                                <div className="row form-group">
                                    <input type="text" ref="inputVisitingCode" id="inputVisitingCode" placeholder="נא הזן קוד"/>
                                </div>
                                <div className="row form-group">
                                    <input type="button" onClick={this.visitApartment} className="btn btn-primary delta-btn" defaultValue="היכנס"/>
                                    <div ref="messageVisitingCode" id="messageVisitingCode"/>
                                </div>
                                <label className="receiving-code-message">על מנת לקבל קוד, נא פנה למפרסם המודעה:</label>
                                <div className="row form-group">
                                    <label>{this.getAdvertiserDetails()}</label> 
                                </div>                              
                            </div>
                        </div>
                    </div>
                </div>
                );
    }
});

module.exports = LoginModal;
