var Utils = require('../../common/js/utils.js');

// Packages
var React = require('react');

// Styles
var Styles = require('../css/sharing_component.css');

var ShareTo = {FACEBOOK: 1, TWITTER: 2, WHATSAPP: 3, EMAIL: 4};

SharingComponent = React.createClass({
    getUrlToShare: function(sharingTo){
        var apartmentUrl = "https://" + window.location.hostname + "/singleApartment/" + this.props.apartmentId;

        switch(sharingTo){
            case ShareTo.FACEBOOK:
                return "http://www.facebook.com/sharer.php?u=" + apartmentUrl;
            case ShareTo.TWITTER:
                return "https://twitter.com/share?url=" + apartmentUrl;
            case ShareTo.WHATSAPP:
                return "whatsapp://send?text=" + apartmentUrl;
            case ShareTo.EMAIL:
                return "mailto:?subject=דלתא בל - מודעת דירה&body=" + apartmentUrl;
        }
    },
    render: function(){
        return(
            <div>
               <img className="delta-sharing-icon" src="/misc/share-in-facebook.png" title="שתף דירה בפייסבוק"
                      onClick={(e)=>{
                          window.open(this.getUrlToShare(ShareTo.FACEBOOK))}}/>
                <img className="delta-sharing-icon" src="/misc/share-in-twitter.png" title="שתף דירה בטוויטר"
                      onClick={(e)=>{
                          window.open(this.getUrlToShare(ShareTo.TWITTER))}}/>
                <img className="delta-sharing-icon" src="/misc/share-in-email.png" title="שלח דירה לדואר אלקטרוני"
                      onClick={(e)=>{
                          window.open(this.getUrlToShare(ShareTo.EMAIL))}}/>
                <img className="delta-sharing-icon hidden-md hidden-lg hidden-sm"  data-action="share/whatsapp/share" src="/misc/share-in-whatsapp.png" title="שתף דירה בוואטסאפ"
                      onClick={(e)=>{
                          window.open(this.getUrlToShare(ShareTo.WHATSAPP))}}/>          
            </div>
        )
    }
})

module.exports = SharingComponent;
