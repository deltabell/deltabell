// Packages
var React = require('react');
var Axios = require('axios');
var browserHistory = require('react-router').browserHistory;
var Validator = require('bootstrap-validator');

// Styles
var Styles = require('../css/contact_us_form.css');

// Class
var ContactUsForm = React.createClass({
    getInitialState: function(){
        return({showForm: true})
    },
    componentDidMount: function(){
        $('#contactUsForm').validator();
    },
    submitForm: function(){
        if (this.refs.inputEmail.value !== ""
          & this.refs.contactValid1.innerText===""
          & this.refs.contactValid2.innerText===""
          & this.refs.contactValid3.innerText===""
          & this.refs.contactValid4.innerText===""){
              Axios.post('/contactUs',{
                email: this.refs.inputEmail.value,
                phone: this.refs.inputPhone.value,
                name: this.refs.inputName.value,
                message: this.refs.txtareaMessage.value
            })
            .then(function (response) {
                this.setState({showForm: false})
            }.bind(this));
        }
    },
    goBack: function(){
        browserHistory.push('/');
        window.scrollTo(0,0);
    },
    render: function(){
        if (this.state.showForm) {
        return(
            <form role="form" id="contactUsForm" data-toggle="validator" className="container container-contact-us-form">
                <div className="row form-group">
                        <div className="col-xs-8">
                            <input type="email" ref="inputEmail" className="form-control" 
                                   id="email" name="email" placeholder="הזן דואר אלקטרוני" required
                                   data-error="נא הזן כתובת אינטרנט אלקטרוני תקינה"/>
                            <div ref="contactValid1" className="help-block with-errors"/>
                        </div>
                        <label className="col-xs-4 control-label">דואר אלקטרוני</label>
                </div>
                <div className="row form-group">
                        <div className="col-xs-8">
                            <input type="tel" ref="inputPhone" className="form-control" 
                                   id="phone" name="phone" placeholder="הזן מספר טלפון" required
                                   data-error="נא הזן מספר טלפון"/>
                            <div ref="contactValid2" className="help-block with-errors"/>
                        </div>
                        <label className="col-xs-4 control-label">מספר טלפון</label>
                </div>
                <div className="row form-group">
                        <div className="col-xs-8">
                            <input type="text" ref="inputName" className="form-control" 
                                   id="name" name="name" placeholder="הזן שם" required
                                   data-error="שדה שם הינו שדה חובה"/>
                            <div ref="contactValid3" className="help-block with-errors"/>
                        </div>
                        <label className="col-xs-4 control-label">שם</label>
                </div>
                <div className="row form-group">
                        <div className="col-xs-8">
                            <textarea id="message" ref="txtareaMessage" name="message" 
                                      className="form-control" placeholder="הזן הודעה" rows="7" required
                                      data-error="נא פרט את הודעתך"/>
                            <div ref="contactValid4" className="help-block with-errors"/>
                        </div>
                        <label className="col-xs-4 control-label">הודעה</label>
                </div>
                <div className="row form-group">
                        <div className="col-xs-offset-2 col-xs-6">
                            <input onClick={this.submitForm} className="btn btn-primary delta-btn" defaultValue="שלח"/>
                        </div>
                </div>
            </form>
        )} else {
        return(
            <div className="container container-contact-us-form">
                <div className="row top-buffer">
                    <h2>ההודעה נשלחה בהצלחה</h2>
                    <div className="row top-buffer">
                        <div className="col-xs-12">
                            <p className="text-center">נציגנו ישוב אליכם בהקדם האפשרי</p>
                        </div>
                    </div>
                    <div className="row top-buffer">
                        <div className="col-xs-offset-5 col-xs-2">
                            <button onClick={this.goBack} className="btn btn-primary delta-btn">חזור</button>
                        </div>
                    </div>
                </div>
            </div>
        )};
    }
});

module.exports = ContactUsForm;
