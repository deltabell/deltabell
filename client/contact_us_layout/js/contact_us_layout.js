// Packages
var React = require('react');
var ContactUsForm = require('./contact_us_form.js');
var Footer = require('../../main_layout/js/footer.js')

// Styles
var Styles = require('../css/contact_us_layout.css');

var ContactUsLayout = React.createClass({
    render: function(){
        return (
            <div>
                <div className="container contact-us-container">
                    <h2>יצירת קשר עם חברת דלתא בל 770</h2>
                    <div className="container-contact-us-text">
                        חברת דלתא בל הנה חברה חדשנית בתחום נדל״ן וטכנולוגיה.
                        <br/>
                        החברה נמצאת, כרגע, בשלבי הקמה ופיתוח ושואפת לצמוח ולגדול.
                        <br/>
                        נשמח מאוד לשמוע כל שאלה, בקשה ורעיון.
                        <br/>
                        ניתן ליצור קשר במייל: 
                        <a href="mailto:info@deltabell.net"> info@deltabell.net </a>
                        <br/>
                        כמו כן, ניתן להתקשר למשרדי החברה בטלפון: 
                        <a href="tel:02-5770770"> 02-5770770 </a>
                        <br/>
                        או לחילופין, מלאו את הטופס שלפניכם ונחזור אליכם בהקדם:
                        <br/>
                    </div>
                    <ContactUsForm/>
                </div>
                <Footer/>
            </div>
        );
    }
});

module.exports = ContactUsLayout;

