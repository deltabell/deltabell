// Packages
var React = require('react');

// Styles
var Styles = require('../css/gmap_component.css');

GMapComponent = React.createClass({
    marker: {},
    componentDidMount: function(){
        // setting location
        var defaultLocation = {lat: 31.775902, lng: 35.213651};

        // Creating the Map 
        this.map = new google.maps.Map(this.refs.viewerGMap, {
            center:  defaultLocation,
            zoom: 15,
        });

        this.createMarker();
    },
    componentDidUpdate: function(){
        this.createMarker();
    },
    createMarker: function(){
        // Clearing old marker
        if (this.marker.setMap !== undefined){
            this.marker.setMap(null);
            this.marker={};
        }
        
        // creating new marker
        var newLocation = {lng: this.props.lng, lat: this.props.lat}
        this.marker = new google.maps.Marker({
            position: newLocation,
            draggable: true,
            map: this.map
        });
        this.map.setCenter(newLocation)
        this.map.setZoom(15)
        var self = this;
        // Creating the marker event
        this.dragListener = google.maps.event.addListener(this.marker, 'dragend', function (event) {
            self.props.setApartmentLocation(this.getPosition().lng(),this.getPosition().lat())
        });
    },
    render: function(){
        // Change size of map by role into tab (primary/secondary)
        styleSuffix = this.props.isPrimaryComponent ? "primary" : "secondary"
        return(
            <div className={"gmap-container-"+styleSuffix} id="apartmentViewerMap" ref="viewerGMap"/>
        )
    }
})

module.exports = GMapComponent;
