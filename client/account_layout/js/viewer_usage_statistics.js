// Packages
var React = require('react');
var Axios = require('axios');
var Utils = require('../../common/js/utils.js');

// Styles
var Styles = require('../css/viewer_usage_statistics.css');

VISIT_TYPE_MAP = "map";
VISIT_TYPE_SINGLE_APARTMENT = "apartmentPage"
VISIT_TYPE_ACCOUNT = "accountManagement"

var ViewerUsageStatistics = React.createClass({
    getInitialState: function(){
        return {
            accountVisitsByUser: [],
            visitsLog: [],
            allVisits: null,
            processedVisits: [],
            allAccountVisits: [],
            filterAdminUsage: true,
            filteredVisitsLog: [],
            allAccounts: null
        }
    },
    componentDidMount: function(){
        // Getting all visits
        Axios.get('/api/visits/getAllVisits').then(function(response){
             if (response.data.errorMessage){
                 this.setState({visitsLog: 'error'});
             } else if (response.data){
                 this.setState({allVisits: response.data.visits});
                 this.processVisitsData();
                 this.loadAccountManagementVisits();
             }
        }.bind(this)) 

        $('.nav-tabs a[href="#clientsUsage"]').tab('show');
    },
    processVisitsData: function(){
        visitsLog = this.state.visitsLog;
        allVisits = this.state.allVisits;
        
        // Getting subset visits range
        startIndex = Math.min(visitsLog.length, allVisits.length);
        endIndex = Math.min(startIndex + 299, allVisits.length);
        
        if (startIndex == endIndex){
            this.refs.btnShowMoreVisits.disabled = true;
        } else {
            allVisits.slice(startIndex, endIndex + 1).forEach(function(visit){
                visitsLog.push({
                    visitTime: Utils.getFormattedDate(visit.visitTime, "DD/MM/YYYY hh:mm"),
                    description: this.getVisitDescription(visit),
                    userType: visit.userType,
                    link: this.getRowLink(visit)
                })
            }.bind(this))
        }
        
        this.setState({visitsLog: visitsLog})
    },
    loadAccountManagementVisits: function(){
        Axios.get('/api/accounts/getAccountNames', {
            params: {
                excludeAdmin: true
            }
        }).then(function(response){
            if (!response.data){
                this.setState({accountVisitsByUser: "error"})
            } else {
                allVisits = this.state.allVisits;
                accountNames = response.data.accountNames;
                var accountVisitsByUser = this.state.accountVisitsByUser;
                
                // going on every account and init the object
                accountNames.forEach(function(account){
                    // Init the returned object
                    accountVisitsByUser.push({
                        accountId: account.accountId,
                        accountDescription: account.name + " - " + account.username,
                        lastDay: 0,
                        lastWeek: 0,
                        lastMonth: 0,
                        total: 0
                    });
                    var index = accountVisitsByUser.length  - 1;
                    var accountVisits = allVisits.filter(function(visit){
                        return visit.accountId === accountVisitsByUser[index].accountId
                               && visit.visitType === VISIT_TYPE_ACCOUNT
                    });
                    
                    accountVisits.forEach(function(visit){
                        compareDate = new Date();
                        compareDate.setDate(compareDate.getDate() - 1);
                        accountVisitsByUser[index].total = accountVisitsByUser[index].total + 1;
                        
                        formattedVisitTime = new Date(visit.visitTime)
                        if (formattedVisitTime >= compareDate){
                            accountVisitsByUser[index].lastDay = accountVisitsByUser[index].lastDay + 1;
                        };
                        compareDate.setDate(compareDate.getDate() - 6);
                        if (formattedVisitTime >= compareDate){
                            accountVisitsByUser[index].lastWeek = accountVisitsByUser[index].lastWeek + 1;
                        };
                        compareDate.setDate(compareDate.getDate() - 23);
                        if (formattedVisitTime >= compareDate){
                            accountVisitsByUser[index].lastMonth = accountVisitsByUser[index].lastMonth + 1;
                        };
                    })
                });
                accountVisitsByUser.sort(function(a,b){
                    return ((b.lastDay * 100) + (b.lastWeek * 10) + b.lastMonth) - ((a.lastDay * 100) + (a.lastWeek * 10) + a.lastMonth);
                })
                this.setState({accountVisitsByUser: accountVisitsByUser});
            }
        }.bind(this));
            
    },
    getRowLink: function(visit){
        if (visit.visitType == VISIT_TYPE_SINGLE_APARTMENT){
            return <span onClick={(e)=>{window.open('/singleApartment/' + visit.apartmentId)}}>
                        <u>{visit.apartmentTitle ? visit.apartmentTitle : "דף דירה"}</u>
                    </span>
        } else {
            return null;
        }
    },
    getVisitDescription: function(visit){
            visitor = visit.name ? visit.name : "אורח"
        switch(visit.visitType){
            case VISIT_TYPE_ACCOUNT:
                return visitor + " " + "ביקר בדף ניהול חשבון";
            case VISIT_TYPE_SINGLE_APARTMENT:                
                return visitor + " " +  "ביקר בדף דירה";
            case VISIT_TYPE_MAP:
                return visitor + " " +  "ביקר בדף מפה";
            default:
                return 'unknown visit type' 
        }
    },
    getUsageLogSection: function(){
        if (!this.state.allVisits) 
        { 
            return <div>טוען נתונים...</div> 
        }
        else if (this.state.allVisits === 'error')
        {
            return <div>תקלה טכנית בקבלת הנתונים</div>
        }
        else if (this.state.visitsLog.length == 0){
            return <div>מעבד נתונים...</div>
        }
        else 
        {
            // Remove admin usage if needed
            filteredVisitsLog = !this.state.filterAdminUsage ? this.state.visitsLog :
                this.state.visitsLog.filter(function(visit){
                    return visit.userType !== 'admin'
            })

            // Return log rows components
            return(filteredVisitsLog.map(function(visit, index){
                return <div className='delta-account-name' key={index}>
                            <label className="visit-log-row-time">{visit.visitTime}</label>
                            <label className="visit-log-row-description">{visit.description}</label>
                            {visit.link ? visit.link : <span/>}
                        </div>
            }))
        }           
    },
    changeFilteringAdminStatus: function(){
        this.setState({filterAdminUsage: !this.state.filterAdminUsage})
    },
    getClientsUsageSection: function(){
        if (this.state.accountVisitsByUser.length == 0) 
        {
            return <div>טוען נתונים...</div> 
        }
        else if (this.state.accountVisitsByUser === 'error')
        {
            return <div>תקלה טכנית בקבלת הנתונים</div>
        }
        else 
        { 
            var clientsUsageArray = this.state.accountVisitsByUser;        
            return(clientsUsageArray.map(function(account, index){
                return <div className='delta-account-name' key={index}>
                            <label>{account.accountDescription}</label>
                            <div>{'ביממה האחרונה: ' + account.lastDay}</div>
                            <div>{'בשבוע האחרון: ' + account.lastWeek}</div>
                            <div>{'בחודש האחרון: ' + account.lastMonth}</div>
                            <div>{'סה״כ: ' + account.total}</div>
                        </div>
            }.bind(this)))
        }                           
    },
    render: function(){
        return( <div className="container-viewer-usage-statistics">
                    <div className="row">
                        <div className="col-xs-12">
                            <ul className="nav nav-tabs delta-nav-tabs">
                                <li><a href="#clientsUsage" data-toggle="tab" id="clientsUsageTab">כניסה לדף ניהול חשבון</a></li>
                                <li><a href="#usageLog" data-toggle="tab" id="usageLogTab">לוג</a></li>
                            </ul>
                        </div>
                    </div>    
                    <div id="myTabContent" className="tab-content">
                        <div className="tab-pane" id="clientsUsage">
                            <div className="viewer-statistics-section">
                                {this.getClientsUsageSection()}
                            </div>
                        </div>
                        <div className="tab-pane" id="usageLog">
                            <div className="viewer-statistics-section">
                                <div className="filter-admin-visits-container">
                                    <label className="label-checkbox-filter-admin">סנן שימוש מנהל</label>
                                    <input type="checkbox" ref="checkboxFilterAdmin"
                                     checked={this.state.filterAdminUsage} onChange={this.changeFilteringAdminStatus}/>
                                </div>
                                <div className="usage-log-rows-container">
                                    {this.getUsageLogSection()}
                                </div>
                                {this.state.visitsLog.length == 0 ? <div/> :
                                    <button className="btn btn-primary delta-btn" 
                                            onClick={this.processVisitsData}
                                            ref="btnShowMoreVisits">
                                            טען עוד
                                    </button>
                                 } 
                            </div>
                        </div>
                    </div>
                </div>);          
        }
    });

module.exports = ViewerUsageStatistics;
