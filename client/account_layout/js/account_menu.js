// Packages
var React = require('react');
var Axios = require('axios');
var AccountMenuTitle = require('./account_menu_title');
var AccountMenuSubTitle = require('./account_menu_subtitle');
var Utils = require('../../common/js/utils.js');

// Styles
var Styles = require('../css/account_menu.css');


var AccountMenu = React.createClass({
    getInitialState: function(){
        return({isShowApartmentList: false,
                account: null})
    },
    componentDidMount: function(){
        this.setState({account: this.props.account})
    },
    setViewer: function(selectedViewer){
        this.props.setAccountLayoutState({selectedViewer: selectedViewer});
    },
    renderApartmentMenu: function(apartment, index){
        return(
            <div key={index}>
                <AccountMenuSubTitle subtitle={apartment.apartmentTitle}
                                     isSelected={"apartment" === this.props.selectedViewer & this.props.selectedApartmentIndex===index}
                                     onClickFunction={(e)=>{
                                        this.props.setAccountLayoutState({
                                            selectedViewer:"apartment",
                                            selectedApartmentIndex: index
                                        })
                                     }}/>
            </div>
        )
    },
    changeIsShowApartmentList: function(){
        this.setState({isShowApartmentList: !this.state.isShowApartmentList})
    },
    renderNewApartmentMenu: function(){
        return(<div className="account-menu-new-apartment" onClick={this.createNewApartment}>הוסף דירה חדשה</div>)
    },
    createNewApartment: function(){
        Axios.get('/api/apartments/createNewApartment')
            .then(function (response,err) {
                if (err){
                    console.log(err);
                } else {
                    this.props.addApartment(response.data)
                }
            }.bind(this));
    },
    getAdminOptions: function(){
        adminMenuTitles = <div>
                            <AccountMenuTitle title="רשימת תפוצה"
                                    viewerType="mailingList"
                                    isSelected={"mailingList" === this.props.selectedViewer}
                                    onClickFunc={()=>{this.setViewer("mailingList")}}/>
                            <AccountMenuTitle title="שימוש באתר"
                                    viewerType="usageStatistics"
                                    isSelected={"usageStatistics" === this.props.selectedViewer}
                                    onClickFunc={()=>{this.setViewer("usageStatistics")}}/>
                        </div>
        return this.state.account && this.state.account.userType === 'admin' ? 
            adminMenuTitles : <div/>
        
    },
    render: function(){
        return(
            <div>
                <AccountMenuTitle title="הגדרות החשבון"
                                  viewerType="settings"
                                  isSelected={"settings" === this.props.selectedViewer}
                                  onClickFunc={()=>{this.setViewer("settings")}}/>
                {this.getAdminOptions()}  
                <AccountMenuTitle title="הדירות שלי"
                                  viewerType="apartments"
                                  isSelected={"apartments" === this.props.selectedViewer}
                                  onClickFunc={this.changeIsShowApartmentList}/>                         
                {this.state.isShowApartmentList ? this.props.apartmentsList.map(this.renderApartmentMenu) : <div/>}
                {this.state.isShowApartmentList & this.props.account.userType==="admin" ? this.renderNewApartmentMenu() : <div/>}                    
            </div>
        )
    }
});

module.exports = AccountMenu;