// Packages
var React = require('react');

// Styles
var Styles = require('../css/account_menu_subtitle.css');

var AccountMenuSubTitle = React.createClass({
    render: function(){
        return(
            <div onClick={this.props.onClickFunction}
                 className={this.props.isSelected ? "account-menu-subtitle selected-menu" : "account-menu-subtitle"}>
                 {this.props.subtitle}
            </div>
        )
    }
});

module.exports = AccountMenuSubTitle;