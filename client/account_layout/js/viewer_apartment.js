// Packages
var React = require('react');
var Axios = require('axios');
var FormData = require('form-data');
var FeatureList = require('./feature_list.js');
var GMapComponent = require('./gmap_component.js');
var CesiumPlaceApartment = require('./cesium_place_apartment.js');
var SharingComponent = require('../../common/js/sharing_component');
var Utils = require('../../common/js/utils.js');
var CesiumHelper = require('../../common/js/cesium_helper.js');

// Styles
var Styles = require('../css/viewer_apartment.css');

// Constants
var MIN_PICTURE_FILE_SIZE_IN_BYTES = 1024 * 50 // 50Kb
var MAX_PICTURE_FILE_SIZE_IN_BYTES = 1024 * 1024 * 10 // 10Mb
var MAX_PICTURES_FOR_APARTMENT = 15

var ViewerApartment = React.createClass({
    getInitialState: function(){
        return{
            apartment: {},
            showLocationTab: false,
            isThreeDimensionalLayerFound: false,
            accountList: [],
            accountNames: [],
            apartmentsVisits: {}
        }
    },
    componentDidMount: function(){
        $('#apartmentDetailsForm').validator();
        this.setState({apartment:this.props.apartment})
        // init google geocoder
        this.geocoder = new google.maps.Geocoder();
        // Click details tab
        $('.nav-tabs a[href="#details"]').tab('show');

        if (this.isAdmin() && this.state.accountList.length == 0){
            //querying all the accounts from server
            Axios.get('/api/accounts/getUserList')
            .then(function (response){
                if (response.data){
                    // Adding first option for defining ownerId null
                    accountList = [{nameAndEmail: "ללא", _id: ""}];
                    // Helper array for check if any account is selected in input
                    accountNames = [];
                    accountList = accountList.concat(response.data.map(function(account){
                        // To prevent mistakes - the user name and email address are displayed
                        nameAndEmail = account.username + " - " + account.name;
                        account["nameAndEmail"] = nameAndEmail;
                        accountNames.push(nameAndEmail)
                        return account;
                    }))
                    
                    this.setState({accountList: accountList, accountNames: accountNames});
                    $("#inputAccount").on("input", (e)=>{
                        this.updateOwnerAfterSelection(e.target.value);
                    });
                }
            }.bind(this));
        }
    },
    // Handler for changes in input of apartment owner
    updateOwnerAfterSelection: function(nameAndEmail){
        // If the input content not the same as any account - ignore
        if (this.state.accountNames.includes(nameAndEmail)){
            selectedAccount = this.state.accountList.find(function(account){
                return account.nameAndEmail == nameAndEmail;
            })
            // update the state about apartment's owner just if the selected value is diferent from the current
            if (this.state.apartment.ownerId != selectedAccount._id){
                this.setStateValue(selectedAccount, "ownerItem");
            }
        } 
    },
    // Show 3D map just if exists a tails & is authorized to access the 3D data
    // This is a temporary function until finishing 3D issue development
    temporaryShow3D: function(){
        return this.state.isThreeDimensionalLayerFound &&
            ["cesium", "admin"].includes(this.props.account.userType);
    },
    componentDidUpdate: function(prevProps, prevState){
        if (this.state.showLocationTab){
            // Defining 'Enter' key to search a location
            var self = this;
            $("#serachLocationInput").keyup(function(event){
                if(event.keyCode == 13){
                    self.searchLocation();
                }
            });
            
            // this changes should be done only when map tab is open or the apartment location was changed
            if (!prevState.showLocationTab ||
                prevState.apartment.lng != this.state.apartment.lng ||
                prevState.apartment.lat != this.state.apartment.lat ||
                prevState.apartment.hgt != this.state.apartment.hgt) 
            {
                this.checkThreeDimensionalLayer();
            }
        }
    },
    componentWillReceiveProps: function(newProps){
        this.setState({apartment:newProps.apartment})
        this.refs.serachLocationInput.value = "";
        this.refs.saveInfo.innerText = "";
        this.refs.uploadingPictureResult.innerText = "";
    },
    // Update a flag if exists tails for apartment location
    checkThreeDimensionalLayer: function(){
        lng = this.state.apartment.lng; 
        lat = this.state.apartment.lat; 
        hgt = this.state.apartment.hgt;
        
        if (!lat || !lng){
            this.setState({isThreeDimensionalLayerFound: false})
        } 
        else {
            CesiumHelper.getTiles(lng, lat, hgt, function(tiles)
            {
                this.setState({isThreeDimensionalLayerFound: tiles.length > 0})                                      
            }.bind(this))
        } 
    },
    getStateValue: function(section){
        if (this.state.apartment.apartmentTitle === undefined){
            return ("");
        }
        switch(section) {
            case "rooms": return(this.state.apartment.summary.rooms); break;
            case "contact": return(this.state.apartment.summary.contact); break;
            case "size": return(this.state.apartment.summary.size); break;
            case "price": return(this.state.apartment.summary.price); break;
            case "floor": return(this.state.apartment.summary.floor); break;
            case "apartmentTitle": return(this.state.apartment.apartmentTitle); break;
            case "description": return(this.state.apartment.details.description); break;
            case "features": return(this.state.apartment.details.features); break;
            case "matterportKey": return(this.state.apartment.matterportKey); break;
            case "ownerId": return(this.state.apartment.ownerId); break;
            case "photographyDate": return(Utils.getFormattedDate(this.state.apartment.photographyDate)); break;
            case "visitingCode": return(this.state.apartment.visitingCode); break;
            case "displayInBoard": return(this.state.apartment.displayInBoard); break;
            case "ownerItem": return(this.getOwnerItem()); break;
            default: return("")
        }
    },
    setStateValue: function(e,section){
        var newApartment = (JSON.parse(JSON.stringify(this.state.apartment)));
        switch(section) {
            case "rooms": newApartment.summary.rooms = e.target.value; break;
            case "contact": newApartment.summary.contact = e.target.value; break;
            case "size": newApartment.summary.size = e.target.value; break;
            case "price": newApartment.summary.price = e.target.value; break;
            case "floor": newApartment.summary.floor = e.target.value; break;
            case "apartmentTitle": newApartment.apartmentTitle = e.target.value; break;
            case "description": newApartment.details.description = e.target.value; break;
            case "matterportKey": newApartment.matterportKey = e.target.value; break;
            case "photographyDate": newApartment.photographyDate = e.target.value; break;
            case "ownerId": newApartment.ownerId = e.target.value; break;
            case "visitingCode": newApartment.visitingCode = e.target.value; break;
            case "displayInBoard": newApartment.displayInBoard = e.target.checked; break;
            case "ownerItem": newApartment.ownerId = e._id; break;
        }
        if (section.search("feature") !== -1){
            var featureName = section.substr(7);
            if (newApartment.details.features.includes(featureName)){
                newApartment.details.features = newApartment.details.features.filter(function(feature){return feature !== featureName}); 
            } else{
                newApartment.details.features.push(featureName);
            }
        }
        this.setState({apartment:newApartment});
    },
    renderImagesSection: function(){
        if (this.state.apartment.images)
        {
            var self=this;         
            return(this.state.apartment.images.map(function(image,index,images){
                if (index === images.length - 1 & index%2===0)
                {
                    return(
                        <div key={index} className="row viewer-apartment-image-section">
                            <div className="col-xs-6 col-xs-offset-6">
                                <div className="col-xs-3 col-xs-offset-1">
                                    <button onClick={(e)=>{self.delteImage(index)}}
                                            className="btn btn-default logo-delete-btn">הסר</button>
                                    <button className="btn btn-default logo-delete-btn" onClick={(e)=>self.moveImageUp(index)}>
                                        <span title="העלה מיקום תמונה" className="glyphicon glyphicon-arrow-up viewer-apartment-glyphicon"/>
                                    </button>
                                </div>
                                <img className="col-xs-7 img-responsive " src={image.src}/>
                            </div>
                        </div>
                    )
                } else {
                    if (index !== images.length - 1 & index%2===0){
                        return(
                            <div key={index} className="row viewer-apartment-image-section">
                                <div className="col-xs-6">
                                    <div className="col-xs-3 col-xs-offset-1">
                                        <button onClick={(e)=>{self.delteImage(index + 1)}}
                                                className="btn btn-default logo-delete-btn">הסר</button>
                                        <button className="btn btn-default logo-delete-btn" onClick={(e)=>self.moveImageUp(index + 1)}>
                                            <span title="העלה מיקום תמונה" className="glyphicon glyphicon-arrow-up viewer-apartment-glyphicon"/>
                                        </button>
                                    </div>
                                    <img className="col-xs-7 img-responsive " src={images[index + 1].src}/>
                                </div>
                                <div className="col-xs-6">
                                    <div className="col-xs-3 col-xs-offset-1">
                                        <button onClick={(e)=>{self.delteImage(index)}}
                                                className="btn btn-default logo-delete-btn">הסר</button>
                                        <button className="btn btn-default logo-delete-btn" onClick={(e)=>self.moveImageUp(index)}>
                                            <span title="העלה מיקום תמונה" className="glyphicon glyphicon-arrow-up viewer-apartment-glyphicon"/>
                                        </button>
                                    </div>
                                    <img className="col-xs-7 img-responsive " src={images[index].src}/>
                                </div>
                            </div>
                        )
                    }
                }
            }))
        };
        return(<div/>);
    },
    delteImage: function(imageIndex){
        var newApartment = (JSON.parse(JSON.stringify(this.state.apartment))); 
        newApartment.images.splice(imageIndex, 1 );
        this.setState({apartment: newApartment});
    },
    moveImageUp: function(imageIndex){
        if (imageIndex > 0){
            var newApartment = (JSON.parse(JSON.stringify(this.state.apartment))); 
            var newImagesOrder = this.state.apartment.images.slice(0,imageIndex - 1);
            newImagesOrder.push(this.state.apartment.images[imageIndex]);
            newImagesOrder.push(this.state.apartment.images[imageIndex -1]);
            newImagesOrder = newImagesOrder.concat(this.state.apartment.images.slice(imageIndex + 1));
            newApartment.images = newImagesOrder;
            this.setState({apartment: newApartment});
        }
    },
    addImage: function(filePath){
        var newApartment = (JSON.parse(JSON.stringify(this.state.apartment)));
        newApartment.images.push({src: filePath});
        this.setState({apartment: newApartment});
    },
    saveApartment: function(){
        // Saving the apartment and updating parent state
        if (this.state.apartment.images.length < 5 | 
            this.refs.valid1.innerText != "" | 
            this.refs.valid2.innerText != ""){
            this.refs.saveInfo.innerText = "אנא הכנס לפחות 5 תמונות ומלא את כל שדות החובה";
        } else {
            var newApartment = (JSON.parse(JSON.stringify(this.state.apartment)));
            for (var i = 0; i < newApartment.images.length; i++)
            {
                newApartment.images[i].src = newApartment.images[i].src.split('/').pop();
            }
            newApartment.sketchFile = newApartment.sketchFile.split('/').pop();
            Axios.post('/api/apartments/saveApartment',{
                apartment: newApartment
            })
            .then(function (response) {
                if (response.data.message==="saved"){
                    this.props.saveApartment(this.state.apartment);
                    this.refs.saveInfo.innerText = "הדירה נשמרה בהצלחה";
                } else {
                    this.refs.saveInfo.innerText = "ארעה שגיאה במהלך השמירה";
                }

            }.bind(this));            
        }
    },
    deleteApartment: function(apartmentId){
        // deleting apartment from DB
        Axios.post('/api/apartments/deleteApartment',{
            apartmentId: apartmentId
        })
        .then(function (response) {
            if (response.data.message="deleted"){this.props.deleteCurrentApartment();}
        }.bind(this));
    },
    reverseChanges: function(){
        this.setState({apartment:this.props.apartment})
    },
    handleImageUpload: function(e){
        if (this.refs.imageUploader.files.length > 0)
        {
            var errorMessage
            fileToUpload = this.refs.imageUploader.files[0];
            if (fileToUpload.size < MIN_PICTURE_FILE_SIZE_IN_BYTES ||
                fileToUpload.size > MAX_PICTURE_FILE_SIZE_IN_BYTES) 
            {
                    errorMessage = "על התמונה להיות בגודל בין " + 
                        MIN_PICTURE_FILE_SIZE_IN_BYTES / 1024 + " קילו בייט" + " ל-" + 
                        MAX_PICTURE_FILE_SIZE_IN_BYTES / 1024 / 1024 + " מגה בייט";
            } 
            else if (this.state.apartment.images.length > MAX_PICTURES_FOR_APARTMENT) 
            {
                errorMessage = "מספר תמונות מקסימלי להעלאה: " + MAX_PICTURES_FOR_APARTMENT;
            } 
            else 
            {
                var fd = new FormData();
                fd.append('apartmentId', this.state.apartment._id)
                fd.append('file', this.refs.imageUploader.files[0]);
                Axios.post('/api/files/uploadApartmentFile', fd)
                .then(function (response) {
                    this.addImage(response.data.filePath)
                }.bind(this));   
            }
            
            this.refs.uploadingPictureResult.innerHTML = errorMessage ? 
                Utils.getFailureHtmlMessage(errorMessage) : Utils.getSuccessHtmlMessage("התמונה הועלתה בהצלחה");
        }
    },
    handleSkecthUpload: function(e){
        if (this.refs.sketchUploader.files.length > 0){
            var fd = new FormData();
            fd.append('apartmentId', this.state.apartment._id)
            fd.append('file', this.refs.sketchUploader.files[0]);
            Axios.post('/api/files/uploadApartmentFile', fd)
            .then(function (response) {
                this.changeSkecthFile(response.data.filePath)
            }.bind(this));
         }
    },
    renderSketchDeleteButton: function(){
        if (this.state.apartment.sketchFile){
            return(
                <button onClick={(e)=>{this.changeSkecthFile("")}} 
                        className="btn btn-default">הסר</button>
            )
        } else {
            return (<div/>)
        }
    },
    changeSkecthFile: function(newSketchFile){
        var newApartment = (JSON.parse(JSON.stringify(this.state.apartment))); 
        newApartment.sketchFile=newSketchFile;
        this.setState({apartment: newApartment});
    },
    setNewLocation: function(lng, lat){
        var newApartment = (JSON.parse(JSON.stringify(this.state.apartment))); 
        newApartment.lng=lng;
        newApartment.lat=lat;
        newApartment.hgt=null;
        newApartment.cameraHeading=null;
        this.setState({apartment: newApartment});
    },
    getGoogleMapComponent: function() {
        return <GMapComponent  lat={this.state.apartment.lat} 
                               lng={this.state.apartment.lng}
                               setApartmentLocation={this.setApartmentLocation}
                               isPrimaryComponent={!this.temporaryShow3D()}/>                        
    },
    // rendering the main map for tab location - cesium / google map
    renderMainMapComponent: function() {
        if (!this.state.apartment.lat || !this.state.apartment.lng){
            return(<div/>)
        } else if (this.temporaryShow3D()) {              
            return(<CesiumPlaceApartment lat={this.state.apartment.lat} 
                                         lng={this.state.apartment.lng}
                                         hgt={this.state.apartment.hgt}
                                         cameraHeading={this.state.apartment.cameraHeading}
                                         setApartmentLocation={this.setApartmentLocation}/>)  
        } else {
            return this.getGoogleMapComponent();
        }
    },
    // If main map is cesium, rendering google map as a secondary map
    renderSecondaryMapComponent: function() {
        if (this.temporaryShow3D()) {
            return this.getGoogleMapComponent();
        } else {
            return (<div/>)
        }                   
    },
    // Search The location entered and handle results
    searchLocation: function(){
        var me = this;
        this.geocoder.geocode( { 'address': this.refs.serachLocationInput.value}, function(results, status) {
            if (status == 'OK') {
                // Setting the primary location of the main layout
                me.setNewLocation(results[0].geometry.location.lng(),results[0].geometry.location.lat());
            } else {
                // what happans if didn't find location
            }
        })
    },
    openApartmentWindow: function(apartmentId){
        window.open("/singleapartment/" + apartmentId);
    },
    openTourWindow: function(apartmentId){
        window.open("/showTour/" + apartmentId);
    },
    isAdmin: function(){
        return this.props.account.userType==="admin";
    },
    getOwnerItem: function(){
        // if no apartment owner id or account list still not loaded - return null
        if (!this.state.apartment.ownerId || this.state.accountList.length == 0){
            return null;
        } else {
            return this.state.accountList.find(function(account){
                return account._id == this.state.apartment.ownerId;
            }.bind(this))
        }
    },
    getAccountListItems: function(){
        return(
            this.state.accountList.map(function(account, index){
                return(
                    <option value={account.nameAndEmail} key={index}/>
                )
            })
        )
    },
    renderAdminSection: function(){
        if (this.isAdmin()){
            return(
                <div className="viewer-apartment-section">
                    <div className="row form-group delta-owner-controls">
                        <div className="col-xs-4 col-xs-offset-1">
                            {this.state.accountList.length == 0 ? <div>נא המתן...</div> :
                                <div>
                                    <input type="text" className="account-input" id="inputAccount" list="accountList"/>
                                    <datalist id="accountList">
                                        {this.getAccountListItems()}
                                    </datalist>
                                </div>
                            }                      
                        </div>
                        <label className="col-xs-1 control-label">שינוי:</label>
                        <label className="col-xs-4 control-label">{this.getOwnerItem() ? this.getOwnerItem().nameAndEmail : ""}</label>
                        <label className="col-xs-2 control-label" >בעל המודעה:</label>
                    </div>
                    <div className="row form-group">
                        <div className="col-xs-3 col-xs-offset-1">
                            <input className="form-control" type="date" ref="inputPhotographyDate" id="inputPhotographyDate"
                                   value={this.getStateValue("photographyDate")} onChange={(e)=>{this.setStateValue(e,"photographyDate")}}/>
                        </div>
                        <label className="col-xs-1 control-label">תאריך צילום:</label>
                        <div className="col-xs-4 col-xs-offset-1">
                            <input className="form-control" type="text" ref="inputMatterportKey" id="inputMatterportKey"
                                   value={this.getStateValue("matterportKey")} onChange={(e)=>{this.setStateValue(e,"matterportKey")}}/>
                        </div>
                        <label className="col-xs-2 control-label">קוד matterport:</label>
                    </div>
                    <div className="row">
                        <div className="col-xs-2 col-xs-offset-1">
                            <button onClick={(e)=>{this.deleteApartment(this.state.apartment._id)}} className="btn btn-danger float-left">מחק דירה</button>
                        </div>
                    </div>
                </div>
            )
        } else {
            return(<div/>);
        }
    },
    getApartmentDetailsTab: function(){
        return(<div role="form" id="apartmentDetailsForm" data-toggle="validator" className="viewer-apartment-section">
            <div className="row">
                <div className="col-xs-3 form-group">
                    <div className="col-xs-8">
                        <input className="form-control" type="number" ref="inputRooms"id="inputRooms"
                               value={this.getStateValue("rooms")} onChange={(e)=>{this.setStateValue(e,"rooms")}}/>
                    </div>
                    <label className="col-xs-4 control-label">חדרים:</label>
                </div>
                <div className="col-xs-3 form-group">
                    <div className="col-xs-9">
                        <input className="form-control" type="number" ref="inputPrice"id="inpuPrice"
                               value={this.getStateValue("price")} onChange={(e)=>{this.setStateValue(e,"price")}}/>
                    </div>
                    <label className="col-xs-3 control-label">מחיר:</label>
                </div>
                <div className="col-xs-6 form-group">
                    <div className="col-xs-10">
                        <input className="form-control" type="text" ref="inputApartmentTitle" id="inputApartmentTitle"
                               required data-error="שדה חובה"
                               value={this.getStateValue("apartmentTitle")} onChange={(e)=>{this.setStateValue(e,"apartmentTitle")}}/>
                        <div ref="valid1" className="help-block with-errors"/>
                    </div>
                    <label className="col-xs-2 control-label">כותרת:</label>
                </div>
            </div>
            <div className="row">
                <div className="col-xs-5 form-group">
                    <div className="col-xs-8">
                        <input className="form-control" type="text" ref="inputContact"id="inputContact"
                               required data-error="שדה חובה"
                               value={this.getStateValue("contact")} onChange={(e)=>{this.setStateValue(e,"contact")}}/>
                        <div ref="valid2" className="help-block with-errors"/>
                    </div>
                    <label className="col-xs-4 control-label">איש קשר:</label>
                </div>
                <div className="col-xs-3 form-group">
                    <div className="col-xs-8">
                        <input className="form-control" type="number" ref="inputFloor"id="inputFloor"
                               value={this.getStateValue("floor")} onChange={(e)=>{this.setStateValue(e,"floor")}}/>
                    </div>
                    <label className="col-xs-4 control-label">קומה:</label>
                </div>
                <div className="col-xs-4 form-group">
                    <div className="col-xs-6">
                        <input className="form-control" type="number" ref="inputSize"id="inputSize"
                               value={this.getStateValue("size")} onChange={(e)=>{this.setStateValue(e,"size")}}/>
                    </div>
                    <label className="col-xs-6 control-label">גודל במ״ר:</label>
                </div>
            </div>
            <div className="row">
                <div className="col-xs-12 form-group">
                    <div className="col-xs-6">
                        <div className="row">
                            <FeatureList features={this.getStateValue("features")} setStateValue={this.setStateValue}/>
                        </div>
                    </div>
                    <div className="col-xs-5">
                                <textarea className="form-control" ref="inputDescription"id="inputDescription" rows="7"
                                          value={this.getStateValue("description")} onChange={(e)=>{this.setStateValue(e,"description")}}/>
                    </div>
                    <label className="col-xs-1 control-label">תיאור:</label>
                </div>
            </div>
        </div>);
    },
    getImagesTab: function(){
        return(
            <div className="viewer-apartment-section">
                <div className="row">
                    <label className="col-xs-12 control-label">תמונות:</label>
                </div>
                {this.renderImagesSection()}
                 <div className="row upload-picture-result" ref="uploadingPictureResult"/>
                <div className="row">
                    <div className="col-xs-2 col-xs-offset-5">
                        <label className="btn btn-primary delta-btn custom-file-upload">
                            <input type="file" ref="imageUploader" onChange={this.handleImageUpload}/>
                            הוסף תמונה
                        </label>
                    </div>
                </div>
            </div>);
    },
    setApartmentLocation: function(lng, lat, hgt, cameraHeading){
        var newApartment = (JSON.parse(JSON.stringify(this.state.apartment))); 
        newApartment.lng = lng;
        newApartment.lat = lat;
        // because hgt & cameraHeading is optionally - 
        // if is undefined when code do JSON.stringify(apartment) - this paramerters removed
        // then, after saving new place, hgt & cameraHeading remaind as before update
        // therefore need to set null instead undefined
        newApartment.hgt = hgt ? hgt : null;
        newApartment.cameraHeading = cameraHeading ? cameraHeading : null;
        
        this.setState({apartment: newApartment});
    },
    // Returns instructions how to mark apartment location
    getExplanationText: function(){
        if (this.temporaryShow3D()) {
            // Cesium 3D map
            return <span>
                    <p>
                        <small><strong>לסימון מיקום הדירה:</strong></small>
                        <br/>סובב את המפה לזוית ראייה נוחה, לחץ לחיצה כפולה במיקום המדויק
                    </p>
                    <p>
                        <small><strong>להגדלה/הקטנה:</strong></small>
                        <br/>השתמש בלחצן הגלילה בעכבר
                    </p>
                    <p>
                        <small><strong>להזזת המפה:</strong></small>
                        <br/> לחיצה שמאלית וגרירה לכיוון הרצוי
                    </p>
                    <p>
                        <small><strong>להטיית המפה:</strong></small>
                        <br/>לחיצה שמאלית + ctrl וגרירה לכיוון המבוקש
                    </p>
                </span>
        } else {
            // Google map
            return <p>
                       <small><strong>לסימון מיקום מדוייק:</strong></small>
                       <br/>חפש כתובת, לאחר הצגת המפה - גרור את הסמן למיקום הרצוי
                   </p>
        }                                      
            
    }, 
    getLocationTab: function(){
        return(
            <div className="viewer-apartment-section">
                <div className="row viewer-apartment-location-section">
                    <div className="col-xs-4 location-left-side">
                        <div className="delta-explanation">
                            {this.state.showLocationTab ? this.getExplanationText() : <div/>}
                        </div>
                        <div className="delta-secondary-tab">
                            {this.state.showLocationTab ? this.renderSecondaryMapComponent() : <div/>}
                        </div>
                    </div>
                    <div className="col-xs-8 location-right-side">
                        <div className="row delta-search-address-controls">
                            <div className="col-xs-1 col-xs-offset-3 btn-search-container">
                                <button className="btn btn-primary delta-btn" onClick={this.searchLocation}>חפש</button>
                            </div>
                            <div className="col-xs-8">
                                <input id="serachLocationInput" ref="serachLocationInput" className="form-control" type="text" placeholder="הזן כתובת"/>
                            </div>
                        </div>
                        <div className="delta-main-map-external-container">
                            {this.state.showLocationTab ? this.renderMainMapComponent() : <div/>}
                        </div>
                    </div>
                </div>
            </div>);
    }, 
    getSketchSection: function(){
        return(<div className="viewer-apartment-section">
                    <div className="row">
                        <div className="col-xs-6 col-xs-offset-6">
                            <div className="row">
                                    <label className="control-label">תשריט:</label>
                            </div>
                            <div className="row viewer-apartment-image-section">
                                <div className="col-xs-10 col-xs-offset-1"  style={{minHeight: "150px"}}>
                                    <img className="col-xs-12 img-responsive " src={this.state.apartment.sketchFile}/>
                                </div>
                            </div>
                            <div className="row ">
                                <div className="col-xs-4 col-xs-offset-2">
                                    <label className="btn btn-primary delta-btn custom-file-upload">
                                        <input type="file" ref="sketchUploader" onChange={this.handleSkecthUpload}/>
                                        בחר קובץ
                                    </label>
                                </div>
                                <div className="col-xs-2">
                                    {this.renderSketchDeleteButton()}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>);
    },
    getStatisticsSection: function(){
        apartmentId = this.state.apartment._id;
        if (!(apartmentId in this.state.apartmentsVisits))
        {
            //  Data about apartment's visits still not loaded from server
            this.loadApartmentVisits();
            innerDiv = <div>טוען נתונים...</div>
        }
        else 
        {
            apartmentVisits = this.state.apartmentsVisits[apartmentId];
            if (apartmentVisits === 'error')
            {
                innerDiv = <div>תקלה טכנית בשליפת הנתונים</div>
            } 
            else // Data about apartment's visits loaded from server
            { 
                innerDiv =  <div>
                                <div className="row form-group">
                                     <label className="col-xs-4 col-xs-offset-8">מספר צפיות בדף דירה</label>
                                </div>
                                <div className="row form-group">
                                    <label className="col-xs-3 col-xs-offset-5">{apartmentVisits['last 24 hours']}</label>
                                    <label className="col-xs-3">ביממה האחרונה</label>
                                </div>
                                <div className="row form-group">
                                    <label className="col-xs-3 col-xs-offset-5">{apartmentVisits['last 7 days']}</label>
                                    <label className="col-xs-3">בשבוע האחרון</label>
                                </div>
                                <div className="row form-group">
                                    <label className="col-xs-3 col-xs-offset-5">{apartmentVisits['last 30 days']}</label>
                                    <label className="col-xs-3">בחודש האחרון</label>
                                </div>
                                <div className="row form-group">
                                    <label className="col-xs-3 col-xs-offset-5">{apartmentVisits['whole visits']}</label>
                                    <label className="col-xs-3">מספר כניסות כולל</label>
                                </div>
                                
                            </div>
            }
        } 

        return <div className="viewer-apartment-section">
                    {innerDiv}
                    <button className="btn btn-primary delta-btn delta-btn-refresh-visits" onClick={this.loadApartmentVisits}>רענן</button>
                </div>
    },
    loadApartmentVisits: function(){
        apartmentId = this.state.apartment._id;
        if (apartmentId in this.state.apartmentsVisits){
            delete this.state.apartmentsVisits[apartmentId];
            this.setState(apartmentsVisits, this.state.apartmentsVisits)
        }
        Axios.get('/api/visits/getApartmentVisits/' + apartmentId)
            .then(function(response){
                apartmentsVisits = this.state.apartmentsVisits;
                if (response.data.errorMessage){
                    apartmentsVisits[apartmentId] = 'error';
                } else {                    
                    apartmentsVisits[apartmentId] = response.data.visitSummary;
                }
                this.setState({apartmentsVisits: apartmentsVisits});
            }.bind(this));
    },
    getSecuritySection: function(){
        return(<div className="viewer-apartment-section">
                    <div className="row form-group">
                        <div className="col-xs-3 col-xs-offset-6">
                            <input type="checkbox" ref="inputDisplayInBoard" id="inputDisplayInBoard"
                                checked={this.getStateValue("displayInBoard")} onChange={(e)=>{this.setStateValue(e,"displayInBoard")}}/>
                        </div>
                        <label className="col-xs-3">האם להציג בלוח?</label>
                    </div>
                    <div className="row form-group">
                        <div className="col-xs-3 col-xs-offset-6">
                            <input className="form-control" type="text" ref="input" id="inputVisitingCode"
                                   value={this.getStateValue("visitingCode")} onChange={(e)=>{this.setStateValue(e,"visitingCode")}}/>
                        </div>
                        <label className="col-xs-3">קוד כניסה לעמוד דירה</label>
                    </div>
                    <div className="row form-group">
                        --------------
                    </div>
                    <div className="row form-group">
                        <small>* ניתן לגשת לדירה שאינה מוצגת בלוח, באמצעות קישור ישיר בלבד</small>
                    </div>
                    <div className="row form-group">
                        <small>** קוד כניסה מאפשר סיור תלת מימדי וגלישה לדף דירה רק לאחר הזנת הקוד</small>
                    </div>
              </div>)
    },
    getSavingSection: function(){
        var facebookSharePrefix = "http://www.facebook.com/sharer.php?u=";
        var twitterSharePrefix = "https://twitter.com/share?url=";
        var apartmentPageUrl = "https://deltabell.co.il/singleApartment/" + this.props.apartment._id;
        return(
            <div className="saving-section">
                <div className="row">
                    <div className="col-xs-2">
                        <button onClick={this.saveApartment} className="btn btn-primary delta-btn">שמור שינויים</button>
                    </div>
                    <div className="col-xs-2">
                        <button onClick={this.reverseChanges} className="btn btn-default">בטל שינויים</button>
                    </div>
                    <div className="col-xs-4">
                        <SharingComponent apartmentId={this.state.apartment._id}/>
                    </div>
                     <div className="col-xs-2">
                        <button onClick={(e)=>{this.openTourWindow(this.state.apartment._id)}} className="btn btn-default">התחל סיור</button>
                    </div>
                    <div className="col-xs-2">
                        <button onClick={(e)=>{this.openApartmentWindow(this.state.apartment._id)}} className="btn btn-default">הצג דף דירה</button>
                    </div>
                </div>
                <div className="row">
                    <div className="col-xs-4" ref="saveInfo"/>
                </div>
            </div>);
    },
    render: function(){
        managementNav = !this.isAdmin() ? <div/> : <li><a href="#management" data-toggle="tab" id="tabRegister">ניהול</a></li>
        return(
            <div className="container-viewer-apartment">
                <div className="row">
                    <div className="col-xs-12">
                        <ul className="nav nav-tabs delta-nav-tabs">
                            <li><a href="#details" data-toggle="tab" onClick={(e)=>{this.setState({showLocationTab: false})}}>פרטים</a></li>
                            <li><a href="#images" data-toggle="tab" onClick={(e)=>{this.setState({showLocationTab: false})}}>תמונות</a></li>
                            <li><a href="#location " data-toggle="tab" onClick={(e)=>{this.setState({showLocationTab: true})}}>מיקום</a></li>
                            <li><a href="#statistics" data-toggle="tab" onClick={(e)=>{this.setState({showLocationTab: false})}}>סטטיסטיקה</a></li>
                            <li><a href="#sketch" data-toggle="tab" onClick={(e)=>{this.setState({showLocationTab: false})}}>תשריט</a></li>
                            <li><a href="#security" data-toggle="tab" onClick={(e)=>{this.setState({showLocationTab: false})}}>אבטחה</a></li>
                            {managementNav}
                        </ul>
                    </div>
                </div>
                <div id="myTabContent" className="tab-content">
                    <form data-toggle="validator" role="form" className="tab-pane active" id="details">
                        {this.getApartmentDetailsTab()}
                    </form>
                    <div className="tab-pane" id="images">
                        {this.getImagesTab()}
                    </div>
                    <div className="tab-pane delta-tab-content" id="location">
                        {this.getLocationTab()}
                    </div>
                    <div className="tab-pane" id="sketch">
                        {this.getSketchSection()}
                    </div>
                    <div className="tab-pane" id="statistics">
                        {this.getStatisticsSection()}
                    </div>
                    <div className="tab-pane" id="security">
                        {this.getSecuritySection()}
                    </div>
                    <div className="tab-pane" id="management">
                        {this.renderAdminSection()}
                    </div>
                </div>

                {this.getSavingSection()}

            </div>
        )
    }
});

module.exports = ViewerApartment;
