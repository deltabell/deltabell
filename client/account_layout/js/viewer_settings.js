// Packages
var React = require('react');
var Axios = require('axios');
var FormData = require('form-data');
var Validator = require('bootstrap-validator');

// Styles
var Styles = require('../css/viewer_settings.css');

var ViewerSettings = React.createClass({
    getInitialState: function(){
        return{
            account: {}
        }
    },
    componentDidMount: function(){
        $('#passchangeform').validator();
        this.setState({account:this.props.account})
        $('.nav-tabs a[href="#accountSettings"]').tab('show');
    },
    componentWillReceiveProps: function(newProps){
        this.setState({account:newProps.account})
    },
    renderLogoComponent: function(){
        if (this.state.account.logoFilePath){
            return(
                <img className="img-responsive logo-image" src={this.state.account.logoFilePath}/>
            )
        } else {
            return (<div/>)
        }
    },
    renderLogoDeleteButton: function(){
        if (this.state.account.logoFilePath){
            return(
                <button onClick={this.deleteLogo} 
                        className="btn btn-default logo-delete-btn ">הסר</button>
            )
        } else {
            return (<div/>)
        }
    },
    changeLogo: function(newLogoFilePath){
        var newAccount = (JSON.parse(JSON.stringify(this.state.account)));
        newAccount.logoFilePath = newLogoFilePath;
        this.setState({account: newAccount})
    },
    deleteLogo: function(){
        Axios.get('/api/accounts/deleteLogoFilePath')
            .then(function (response,err) {
                if (err){
                    console.log(err);
                } else {
                    this.changeLogo("");
                }
            }.bind(this));
    },
    changePassword: function(){
        if (this.refs.passValid1.innerText === ""
            & this.refs.passValid2.innerText === ""
            & this.refs.passValid3.innerText === "")
        {
            if (this.refs.inputNewPassword2.value === ""){
                this.refs.changePasswordStatusMessage.innerHTML = "<font color='red'>נא חזור שנית על הסיסמה</font>"
            }
            else
            {
                Axios.post('/api/accounts/changePassword',
                {
                   username: this.state.account.username,
                   currentPassword: this.refs.inputCurrentPassword.value,
                   newPassword: this.refs.inputNewPassword1.value
                }).then(function(response){
                    var data = response.data;
                    var message_color = data.isSuccess ? 'green' : 'red';
                    this.refs.changePasswordStatusMessage.innerHTML =
                        "<font color='"+(message_color)+"'>"+(data.message)+"</font>";

                }.bind(this))
            }
        }
    },
    updateAccountDetails: function(){
        if (this.refs.inputAccountName.value === ''){
            this.refs.savingMessage.innerHTML =
                "<span class=\"delta-failure-message\">שם חשבון הינו שדה חובה</span>";
        }
        else {
            Axios.post('/api/accounts/update', {
                username: this.state.account.username,
                details: {
                    name: this.refs.inputAccountName.value,
                    contactName: this.refs.inputContactName.value,
                    contactEmail: this.refs.inputContactEmail.value,
                    contactPhoneNumber: this.refs.inputContactPhoneNumber.value
                }
            }).then(function(response){
                var data = response.data;
                if (data.isSuccess){
                    this.refs.savingMessage.innerHTML = "<span class='delta-success-message'>"+data.message+"</span>";
                    this.setState({account: {name: this.refs.inputAccountName.value,
                                            contactName: this.refs.inputContactName.value,
                                            contactEmail: this.refs.inputContactEmail.value,
                                            contactPhoneNumber: this.refs.inputContactPhoneNumber.value}});
                    this.props.refreshAccountDetails();
                } else {
                    this.refs.savingMessage.innerHTML = "<span class='delta-failure-message'>"+data.message+"</span>";
                }
            }.bind(this))

        }
    },
    handleLogoUpload: function(){
        if (this.refs.logoUploader.files.length > 0){
            var fd = new FormData();
            fd.append('file', this.refs.logoUploader.files[0]);
            Axios.post('/api/files/uploadAccountLogoFile', fd)
            .then(function (response) {
                this.changeLogo(response.data.filePath)
            }.bind(this));
         }
    },
    getAccountSettingsElements : function(){
        return( <div>
                    <div className="viewer-settings-section">
                        <div className="delta-first-section-title">
                            פרטי התחברות
                        </div>
                        <div className="row">
                            <div className="col-xs-2 col-xs-offset-4">
                                    <input  className="form-control"
                                            type="text"
                                            ref="inputAccountName"
                                            defaultValue={this.props.account.name}/>
                            </div>
                            <label className="col-xs-1 control-label">שם:</label>
                            <label className="col-xs-3 control-label">{this.props.account.username}</label>
                            <label className="col-xs-2 control-label">דואר אלקטרוני:</label>
                        </div>
                    </div>
                    <form role="form" data-toggle="validator" className="viewer-settings-section" id="passchangeform">
                        <div className="delta-section-title">
                            שינוי סיסמה
                        </div>
                        <div className="row form-group">
                            <div className="col-xs-6 col-xs-offset-4">
                                <input  className="form-control"
                                        type="password"
                                        ref="inputCurrentPassword"
                                        id="inputCurrentPassword"
                                        required
                                        data-error="שדה סיסמה הינו חובה"/>
                                <div ref="passValid1" className="help-block with-errors"/>
                            </div>
                            <label className="col-xs-2 control-label">סיסמה נוכחית</label>
                        </div>
                        <div className="row form-group">
                            <div className="col-xs-6 col-xs-offset-4">
                                <input  className="form-control"
                                        type="password"
                                        ref="inputNewPassword1"
                                        id="inputNewPassword1"
                                        required
                                        data-error="שדה סיסמה הינו חובה"/>
                                <div ref="passValid2" className="help-block with-errors"/>
                            </div>
                            <label className="col-xs-2 control-label">סיסמה חדשה</label>
                        </div>
                        <div className="row form-group">
                            <div className="col-xs-6 col-xs-offset-4">
                                <input  className="form-control"
                                        type="password"
                                        ref="inputNewPassword2"
                                        id="inputNewPassword2"
                                        data-match="#inputNewPassword1"
                                        data-match-error="הסיסמה אינה מתאימה לסיסמה שהוזנה בתחילה"
                                        />
                                <div ref="passValid3" className="help-block with-errors"/>
                            </div>
                            <label className="col-xs-2 control-label">סיסמה חזרה</label>
                        </div>
                        <div className="row">
                            <div className="col-xs-offset-2 col-xs-6">
                                <div ref="changePasswordStatusMessage" className="help-block"/>
                            </div>
                            <div className="col-xs-2">
                                    <input type="button" onClick={this.changePassword} className="btn btn-primary delta-btn" defaultValue="שנה סיסמה"/>
                            </div>
                        </div>
                    </form>
                </div> ); 
    },
    getViewSettingsElements: function(){
       return (<div>
                    <div className="viewer-settings-section">
                        <div className="delta-first-section-title">
                            פרטי יצירת קשר - להצגה באתר
                        </div>
                        <div className="row">
                            <div className="col-xs-2 col-xs">
                                <input  className="form-control"
                                        type="text"
                                        ref="inputContactName"
                                        defaultValue={this.props.account.contactName}/>
                            </div>
                            <label className="col-xs-1 control-label">שם:</label>
                            <div className="col-xs-2">
                                <input  className="form-control"
                                        type="text"
                                        ref="inputContactPhoneNumber"
                                        defaultValue={this.props.account.contactPhoneNumber}/>
                            </div>
                            <label className="col-xs-1 control-label">טלפון:</label>
                            <div className="col-xs-4">
                                <input  className="form-control"
                                        type="text"
                                        ref="inputContactEmail"
                                        defaultValue={this.props.account.contactEmail}/>
                            </div>
                            <label className="col-xs-2 control-label">דואר אלקטרוני:</label>
                        </div>
                    </div>
                    <div className="viewer-settings-section">
                        <div className="delta-section-title">
                            לוגו
                        </div>
                        <div className="row">
                            <div className="col-xs-6 col-xs-offset-4">
                                {this.renderLogoComponent()}
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-xs-offset-2 col-xs-8">
                                {this.renderLogoDeleteButton()}
                                <label className="btn btn-primary delta-btn custom-file-upload">
                                    <input type="file" ref="logoUploader" onChange={this.handleLogoUpload}/>
                                    בחר קובץ
                                </label>
                            </div>
                        </div>
                    </div> 
       </div> );                
    },
    render: function(){
        return(
            <div id="containerViewerSettings" className="container-viewer-settings">
                <div className="row">
                    <div className="col-xs-12">
                        <ul className="nav nav-tabs delta-nav-tabs">
                            <li><a href="#accountSettings" data-toggle="tab" id="tabLogin">הגדרות חשבון</a></li>
                            <li><a href="#viewSettings" data-toggle="tab" id="tabRegister">הגדרות תצוגה</a></li>
                        </ul>
                    </div>
                </div>
                <div id="myTabContent" className="tab-content">
                    <div className="tab-pane" id="accountSettings">
                        {this.getAccountSettingsElements()}
                    </div>
                    <div className="tab-pane" id="viewSettings">
                        {this.getViewSettingsElements()}
                    </div>
                </div>
                <div className="row">
                     <div className="col-xs-1 col-xs-offset-1">
                            <input type="button" onClick={this.updateAccountDetails} className="btn btn-primary delta-btn" defaultValue="שמור שינויים"/>
                        </div>
                        <div className="block-help col-xs-4" style={{textAlign:"left"}} ref="savingMessage"/>
                </div>
            </div>
        )
    }
});

module.exports = ViewerSettings;