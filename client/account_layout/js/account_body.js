// Packages
var React = require('react');
var ViewerSettings = require('./viewer_settings.js');
var ViewerApartment = require('./viewer_apartment.js');
var ViewerMailingList = require('./viewer_mailing_list.js');
var ViewerUsageStatistics = require('./viewer_usage_statistics.js');

// Styles
var Styles = require('../css/account_body.css');

var AccountBody = React.createClass({
    render: function(){
        switch(this.props.selectedViewer) {
            case "settings":
                return(<ViewerSettings account={this.props.account}
                                       refreshAccountDetails={this.props.refreshAccountDetails}/>)
                break;
            case "apartment":
                return(<ViewerApartment apartment={this.props.apartment}
                                        account={this.props.account}
                                        saveApartment={this.props.saveApartment}
                                        deleteCurrentApartment={this.props.deleteCurrentApartment}/>)
                break;
            case "mailingList":
                return(<ViewerMailingList/>)
                break;
            case "usageStatistics":
                return(<ViewerUsageStatistics/>);
                break;
            default:
                return(<div/>)
        }
    }
});

module.exports = AccountBody;