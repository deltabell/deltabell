// Packages
var React = require('react');
Cesium.BingMapsApi.defaultKey = 'Aro7P5Pdawzes8_XJZzLo6opiVjrWdxqv6fpTqJT9F6_Rlapr8ZQf8x8IYSkW1el';

var CesiumHelper = require('../../common/js/cesium_helper.js');

// Styles
var Styles = require('../css/cesium_place_apartment.css');

var CesiumPlaceApartment = React.createClass({
    setDoubleClickEvent: function(){
        // Mouse over the globe to see the cartographic position
        handler = new Cesium.ScreenSpaceEventHandler(this.viewer.scene.canvas);
        var self = this;
        handler.setInputAction(function(clickedPosition) {
            var scene = self.viewer.scene;
            
            // pickedOvject for getting the tile
            if (scene.pickPositionSupported) {
                var cartesian = self.viewer.scene.pickPosition(clickedPosition.position);
                var cartographic = Cesium.Cartographic.fromCartesian(cartesian);
                if (Cesium.defined(cartesian)) {
                    self.props.setApartmentLocation(Number(Cesium.Math.toDegrees(cartographic.longitude)), 
                                                    Number(Cesium.Math.toDegrees(cartographic.latitude)),
                                                    Number(cartographic.height),
                                                    this.viewer.camera.heading)
                }
            }    
        }.bind(this), Cesium.ScreenSpaceEventType.LEFT_DOUBLE_CLICK);        
    },
    componentDidUpdate: function(prevProps){
        if (this.props.lng != prevProps.lng ||
            this.props.lat != prevProps.lat ||
            this.props.hgt != prevProps.hgt)
        {
            // moving the camera to the location recieved
            if (lng !== 0)
            {
                this.getHeight(function(height){
                    var camera = this.viewer.camera;
                    var center = new Cesium.Cartesian3.fromDegrees(this.props.lng,
                                                                   this.props.lat,
                                                                   height);
                    var sphere = new Cesium.BoundingSphere(center);
                    var self = this;
                    camera.flyToBoundingSphere(sphere, {

                        offset: new Cesium.HeadingPitchRange(this.getCameraHeading(), Cesium.Math.toRadians(-45.0),175),
                        complete: function() {self.cameraCenter = center}
                    });
                    
                    if (this.props.hgt){
                        this.markExactLocation();
                    } else {
                       this.viewer.entities.removeAll();
                    }
               }.bind(this))
            }
        }
    },
    componentDidMount: function(){
        this.tilesets = []
        
        viewerOptions = {containerDiv: this.refs.aptCesiumMap,
                         minimumZoomDistance: 20.0,
                         maximumZoomDistance: 1000.0}
        this.viewer = CesiumHelper.getViewer(viewerOptions); 

        this.tiles = [];
        
        this.setDoubleClickEvent();
                    
        CesiumHelper.loadTileSets(this.viewer, this.tiles);
        
        this.getHeight(function(height){
            var center = new Cesium.Cartesian3.fromDegrees(this.props.lng, this.props.lat, height)
            var boundingSphere = new Cesium.BoundingSphere(center);
            this.viewer.camera.flyToBoundingSphere(boundingSphere, {
                duration: 0,
                offset: new Cesium.HeadingPitchRange(this.getCameraHeading(), Cesium.Math.toRadians(-67.5), 100)
            }); 

            if (this.props.hgt) {          
                this.markExactLocation()
            } else {
                this.viewer.entities.removeAll();
            }
        }.bind(this))
    },
    // Getting heading of camera for apartment - pre defined or default (0)
    getCameraHeading: function(){
        if (this.props.cameraHeading) {
            return this.props.cameraHeading;
        } else {
            return this.viewer.camera.heading;
        }
    },
    // If exact location of apartment is defined - mark the exact location
    markExactLocation: function(){
        // Clean previous points
        this.viewer.entities.removeAll();
        
        if (this.props.hgt){
            // Adding point
            this.viewer.entities.add({
                position : Cesium.Cartesian3.fromDegrees(this.props.lng, this.props.lat, this.props.hgt+40),
                billboard: {
                    image: "/misc/house_marker.png",
                    height: 25,
                    width: 25,
                    color: Cesium.Color.fromCssColorString('#35089d')
                }
            });
            
            // Adding line above the point
            this.viewer.entities.add({
                polyline : {
                    positions : Cesium.Cartesian3.fromDegreesArrayHeights([this.props.lng, this.props.lat, this.props.hgt,
                                                                            this.props.lng, this.props.lat, this.props.hgt + 40]),
                    width : 0.8,
                    material : new Cesium.PolylineOutlineMaterialProperty({
                        color : Cesium.Color.WHITE,
                        outlineWidth : 0
                    })
                }
            });
        }
    },
    // get exact or astumated height of location
    getHeight: function(callback){
        if (this.props.hgt){
            callback(this.props.hgt);
        } else { //create astumated height
            CesiumHelper.getZByXandY(this.props.lng, this.props.lat, function(height){
                callback(height);
            }.bind(this))
        }
    },
    render: function(){
        return(
            <div ref="aptCesiumMap" className="cesium-container"/>
        )
    }
})

module.exports = CesiumPlaceApartment;