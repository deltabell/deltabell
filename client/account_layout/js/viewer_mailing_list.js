// Packages
var React = require('react');
var Axios = require('axios');
var Utils = require('../../common/js/utils.js');

// Styles
var Styles = require('../css/viewer_mailing_list.css');

var ViewerMailingList = React.createClass({
    getInitialState: function(){
        return {
            allMailingList: {},
            filteredEmails: [],
            attachedFiles: []
        }
    },
    displayEmails: function(){
        var mailingList = this.state.allMailingList;
        // Before first loading
        if (mailingList === {}){
            this.refs.emailsContainer.innerHTML = "טוען...";
        } 
        // After loading if was a failure
        else if (mailingList === "error")
        {
            this.refs.emailsContainer.innerHTML = Utils.getFailureHtmlMessage("תקלה טכנית בקבלת הרשימה מהשרת");   
        } 
        // Aftar successful loading
        else 
        {
            var filteredList = [];
            if (this.refs.checkboxAccounts.checked) {
                filteredList = filteredList.concat(mailingList.accounts);
            }
            if (this.refs.checkboxOwners.checked) {
                filteredList = filteredList.concat(mailingList.apartmentOwners);
            }
            if (this.refs.checkboxContacts.checked) {
                filteredList = filteredList.concat(mailingList.contacts);
            }
            filteredList = Array.from(new Set(filteredList));
            this.refs.emailsContainer.innerHTML = filteredList.join('<br>');
            this.refs.sumFiltered.innerText = "סך הכל: " + filteredList.length;
            this.setState({filteredEmails: filteredList});
        }

    },
    componentDidMount: function(){
        $('.nav-tabs a[href="#targetSelection"]').tab('show');
        Axios.get('/api/accounts/getMailingList').then(
            function(response){
                if (response.data.mailingList){
                    this.setState({allMailingList: response.data.mailingList});
                } else {
                    this.setState({allMailingList: "error"});
                }                
            }.bind(this))       
    },
    removeFileInIndex: function(fileIndex){
        var newArray = this.state.attachedFiles;
        newArray.splice(fileIndex, 1);
        this.setState({attachedFiles: newArray});
    },
    handleFileUpload: function(){
        if (this.refs.fileUploader.files.length > 0){
            var fd = new FormData();
            var filesCount = this.refs.fileUploader.files.length;
            fd.append('file', this.refs.fileUploader.files[filesCount-1]);
            Axios.post('/api/files/uploadTempFile', fd)
            .then(function (response) {
                newArray = this.state.attachedFiles;
                newArray.push(response.data.filePath);
                this.setState({attachedFiles: newArray});
            }.bind(this));
         }
    },
    getAttachedFileDiv: function(){
        if (this.state.attachedFiles.length == 0)
        {
            return <div/>
        }
        else
        {
            return(this.state.attachedFiles.map(function(filePath, index){
                var fileName = filePath.substring(filePath.lastIndexOf('/')+1);
                return(
                    <div className="row delta-attached-file" key={index}>
                        <a className="col-xs-2" onClick={(e)=>{this.removeFileInIndex(index)}}>הסר</a>
                        <span className="col-xs-10">{fileName}</span>
                    </div>
                )}.bind(this)))
        }
    },
    sendEmail: function(){
        var errorMessage
        if (this.refs.inputSubject.value === "") {
            errorMessage = "נא הזן נושא"
        } else if (this.refs.inputContent.value === "") {
            errorMessage = "נא הזן את תוכן ההודעה"
        } else if (this.state.filteredEmails.length == 0) {
            errorMessage = "לא נבחרו נמענים, נא בחר ב׳רשימת תפוצה׳"
        }
        
        if (errorMessage) {
            this.refs.sendingStatus.innerHTML = "<font color='red'>" + errorMessage + "</font>"
        } else {
            var emailDetails = {
                subject: this.refs.inputSubject.value,
                mailingList: this.state.filteredEmails,
                text: this.refs.inputContent.value,
                attachedFiles: this.state.attachedFiles
            };
            Axios.post('api/accounts/sendEmail', emailDetails)
                .then(function(response){
                    var message_color = response.data.isSuccess ? 'green' : 'red';   
                    this.refs.sendingStatus.innerHTML =
                        "<font color='"+(message_color)+"'>"+(response.data.message)+"</font>";                 
             }.bind(this))
        }
    },
    render: function(){
        return( <div className="container-viewer-mailing-list">
                    <div className="row">
                        <div className="col-xs-12">
                            <ul className="nav nav-tabs delta-nav-tabs">
                                <li><a href="#targetSelection" data-toggle="tab" id="tabTargetSelection">בחירת רשימת תפוצה</a></li>
                                <li><a href="#emailContent" data-toggle="tab" id="tabEmailContent">שליחת דואר אלקטרוני</a></li>
                            </ul>
                        </div>
                    </div>    
                    <div id="myTabContent" className="tab-content">
                        <div className="tab-pane" id="targetSelection">
                            <div className="viewer-mailing-list-section">
                                <div className="row">
                                    <div className="col-xs-4 col-xs-offset-4 delta-emails" ref='emailsContainer'/>
                                    <div className="col-xs-4">
                                        <div className="row delta-group-selection">
                                            <div className="col-xs-9"><label>בעלי דירות</label></div>
                                            <div className="col-xs-3 delta-group-checkbox">
                                                <input type="checkbox" name="groupType" value="apartmentOwners"
                                                       ref="checkboxOwners" onClick={this.displayEmails}/>
                                            </div>
                                        </div>
                                        <div className="row delta-group-selection">
                                            <div className="col-xs-9"><label>בעלי חשבון</label></div>
                                            <div className="col-xs-3 delta-group-checkbox">
                                                <input type="checkbox" name="groupType" value="accounts" 
                                                       ref="checkboxAccounts" onClick={this.displayEmails}/>
                                            </div>
                                        </div>
                                        <div className="row delta-group-selection">
                                            <div className="col-xs-9"><label>אנשי קשר</label></div>
                                            <div className="col-xs-3 delta-group-checkbox">
                                               <input type="checkbox" name="groupType" value="accountsAndContacts"
                                                      ref="checkboxContacts" onClick={this.displayEmails}/>
                                            </div>                                           
                                        </div>
                                        <div className="row">
                                            <div className="col-xs-5 col-xs-offset-7 delta-group-checkbox" ref="sumFiltered"/>
                                        </div>                                       
                                    </div>
                                </div> 
                            </div>   
                        </div>
                        <div className="tab-pane" id="emailContent">
                            <div className="viewer-mailing-list-section">
                                <div className="row form-group">
                                    <div className="col-xs-6 col-xs-offset-3">
                                        <input className="form-control" type="text" ref="inputSubject" id="inputSubject"/>
                                    </div>
                                    <label className="col-xs-2 control-label">נושא:</label>
                                </div>
                                <div className="row form-group">
                                    <div className="col-xs-6 col-xs-offset-3">
                                        <textarea className="form-control" ref="inputContent" id="inputContent" rows="12"/>
                                    </div>
                                    <label className="col-xs-2 control-label">טקסט:</label>
                                </div>
                                <div className="row form-group">
                                    <div className="col-xs-3 col-xs-offset-4">
                                        {this.getAttachedFileDiv()}
                                    </div>
                                    <div className="col-xs-2">
                                        <label className="btn btn-primary delta-btn custom-file-upload">
                                            <input type="file" ref="fileUploader" onChange={this.handleFileUpload}/>
                                            צרף קובץ
                                        </label>
                                    </div>
                                    <label className="col-xs-2 control-label">קובץ מצורף:</label>
                                </div>
                                <div className="row form-group">
                                    <div className="col-xs-6 col-xs-offset-3">
                                        <div ref="sendingStatus"/>
                                    </div>
                                    <input onClick={this.sendEmail} className="btn btn-primary delta-btn col-xs-2" defaultValue="שלח"/>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>);             
        }
    });

module.exports = ViewerMailingList;
