// Packages
var React = require('react');

// Styles
var Styles = require('../css/feature_list.css');

// Get All Possible features array
var possibleFeatures = require('../../../common/static_data/apartment_feature_list.js').filter(function(feature){
    return (feature.visible === true)
});

var FeatureList = React.createClass({
    getFeatueValue: function(featureName){
        return (this.props.features.includes(featureName) === true)
    },
    render: function(){
        var self=this;
        return (
            <div>
                {possibleFeatures.map(function(feature){
                    var featureName="feature" + feature.name
                    return(
                        <div key={feature.key} className="col-xs-4">
                            <input id={"feature" + feature.name} type="checkbox" value={feature.name} className="col-xs-4"
                                checked={self.getFeatueValue(feature.name)}
                                onClick={(e)=>{self.props.setStateValue(e,featureName)}}/>
                            <label className="col-xs-8">{feature.name}</label>
                        </div>
                    )
                })}
            </div>
        )
    }
})

module.exports = FeatureList;