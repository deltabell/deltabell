// Packages
var React = require('react');
var AutoSuggest = require('react-autosuggest');
var Axios = require('axios');

// Styles
var Styles = require('../css/accounts_autocomplete.css');

// Class
var AccountsAutoComplete = React.createClass({
    getInitialState: function(){
        return({
            value: "",
            suggestions: [],
            allAcounts: []
        })
    },
    componentDidMount: function(){
        //querying all the accounts from server
        Axios.get('/api/accounts/getUserList')
        .then(function (response) {
            if (response.data){
                this.setState({allAcounts: response.data})
            }
        }.bind(this));
    },
    // returning all emails that match
    getSuggestions: function(obj){
        const inputValue = obj.value.trim().toLowerCase();
        const inputLength = inputValue.length;
        return inputLength === 0 ? [] : this.state.allAcounts.filter(account =>
            account.username.toLowerCase().slice(0, inputLength) === inputValue
        );
    },
    getSuggestionValue: function(suggestion){return(suggestion._id)},
    renderSuggestion: function(suggestion){
        return(
            <span>{suggestion.username + ": " + suggestion.name}</span>
        )
    },
    onChange: function(e, obj){
        this.setState({value: obj.newValue})
    },
    onSuggestionsFetchRequested: function(value){
        this.setState({suggestions: this.getSuggestions(value)});
    },
    onSuggestionsClearRequested: function(){
        this.setState({suggestions: []});
    },
    render: function()
    {
        var inputProps = {
            value: this.state.value,
            onChange: this.onChange
        }
        var theme = {
            input: 'form-control',
        }
        return(
            <AutoSuggest
                suggestions={this.state.suggestions}
                onSuggestionsFetchRequested={this.onSuggestionsFetchRequested}
                onSuggestionsClearRequested={this.onSuggestionsClearRequested}
                getSuggestionValue={this.getSuggestionValue}
                renderSuggestion={this.renderSuggestion}
                inputProps={inputProps}
                theme={theme}/>
        );
    }
});

module.exports = AccountsAutoComplete;