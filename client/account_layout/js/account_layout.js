// Packages
var React = require('react');
var Axios = require('axios');
var browserHistory = require('react-router').browserHistory;
var AccountBody = require('./account_body.js');
var AccountMenu = require('./account_menu.js')

// Styles
var Styles = require('../css/account_layout.css');

// Class
var AccountLayout = React.createClass({
    getInitialState: function(){
        return({
            accountApartments: [],
            selectedViewer: "settings",
            selectedApartmentIndex: -1
        })
    },
    componentDidMount: function(){
        if (!this.props.account){
            this.props.setLoginModalState(true, '/account');
        } 
        else 
        {
            this.loadData()
        }
    },
    loadData: function(){
        Axios.put('/api/visits/addAccountManagmentVisit');
            
        // Getting all of the account apartments
        Axios.get('/api/apartments/myApartments')
            .then(function (response) {
                const data = response.data;
                if (data){
                    this.setState({accountApartments: data})
                } else {
                    this.setState({accountApartments: null});
                }
            }.bind(this));
    },
    componentDidUpdate: function(prevProps){
        if (!prevProps.account && this.props.account){
            this.loadData();
        }
    },
    apartmentsTitlesList: function(){
        return(
            this.state.accountApartments.map((apartment,index)=>{
                return({
                    apartmentTitle: apartment.apartmentTitle,
                    apartmentIndex: index
                })
            })
        )
    },
    setAccountLayoutState: function(newState){
        this.setState(newState);
    },
    getSelectedApartment: function(){
        if (this.state.selectedViewer !== "apartment"){
            return undefined;
        }
        return(this.state.accountApartments[this.state.selectedApartmentIndex])
    },
    saveApartment: function(apartment){
        var newAccountApartments = this.state.accountApartments;
        newAccountApartments[this.state.selectedApartmentIndex] = apartment;
        this.setState({
            accountApartments: newAccountApartments
        })
    },
    addApartment: function(apartment){
        var apartmentsArray = this.state.accountApartments;
        apartmentsArray.push(apartment);
        this.setState({accountApartments: apartmentsArray})
    },
    deleteCurrentApartment: function(){
        var apartmentsArray = this.state.accountApartments;
        apartmentsArray.splice(this.state.selectedApartmentIndex,1);
        this.setState({accountApartments: apartmentsArray,
                       selectedViewer: "settings",
                       selectedApartmentIndex: -1})
    },
    render: function(){
        // when account is not set then return empty div
        if (!this.props.account)
        {
            return <div/>
        }
        else
        {
            return (
                <div className="row account-layout-row">
                    <div className="col-xs-9 account-layout-col">
                        <AccountBody selectedViewer={this.state.selectedViewer}
                                     apartment={this.getSelectedApartment()}
                                     saveApartment={this.saveApartment}
                                     account={this.props.account}
                                     deleteCurrentApartment={this.deleteCurrentApartment}
                                     refreshAccountDetails={this.props.refreshAccountDetails}/>
                    </div>
                    <div className="col-xs-3 account-layout-col account-layout-menu-col">
                        <AccountMenu apartmentsList={this.apartmentsTitlesList()}
                                     setAccountLayoutState={this.setAccountLayoutState}
                                     selectedViewer={this.state.selectedViewer}
                                     account={this.props.account}
                                     selectedApartmentIndex={this.state.selectedApartmentIndex}
                                     addApartment={this.addApartment}/>
                    </div>
                </div>
            )
        }
    }
});

module.exports = AccountLayout;