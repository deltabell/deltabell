// Packages
var React = require('react');

// Styles
var Styles = require('../css/account_menu_title.css');

var AccountMenuTitle = React.createClass({
    render: function(){
        return(
            <div key={this.props.viewerType}
                 onClick={(e)=>{this.props.onClickFunc ? this.props.onClickFunc() : undefined}}
                 className={this.props.isSelected ? "account-menu-title selected-menu" : "account-menu-title"}>
                <label>
                    {this.props.title}
                </label>
            </div>
        )
    }
});

module.exports = AccountMenuTitle;