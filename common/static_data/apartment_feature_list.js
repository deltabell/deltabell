module.exports =[
    {name: 'מיזוג', visible: true, key:'featureAir'},
    {name: 'מעלית', visible: true, key:'featureEle'},
    {name: 'מרפסת', visible: true, key:'featureBal'},
    {name: 'סורגים', visible: true, key:'featureBar'},
    {name: 'מרפסת שמש', visible: true, key:'featureSun'},
    {name: 'חניה', visible: true, key:'featurePar'},
    {name: 'ממ״ד', visible: true, key:'featureMAM'},
    {name: 'מחסן', visible: true, key:'featureSto'},
    {name: 'משופצת', visible: true, key:'featureBui'}
];