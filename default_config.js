var config = {};

config.connection_string = 'mongodb://deltabell_v2:hruakho770@ds253324.mlab.com:53324/deltabell';
config.hostname = 'localhost';
config.https_port = 443;
config.http_port = 8000;
config.show_map = false;
config.info_email_user = "info@deltabell.net";
config.storage_root_path = '/root/deltabell/';
config.info_email_password = "123456info";
config.tiles_paths_array = [
    'data/3DTiles/Ir_Yamim/',
    'data/3DTiles/Akko/',
    'data/3DTiles/Jerusalem_1/',
    'data/3DTiles/Jerusalem_2/'
];
config.isProduction = function(){return false;}
config.mapiToken="5a4b8472-b95b-4687-8179-0ccb621c7990"; // localhost
//config.mapiToken="a7c192ce-d773-49c6-9920-13cd82804c7f"; // www.deltabell.co.il
config.scrap_server_ip = 'localhost'; // dev
//config.scrap_server_ip = '139.162.228.11'; // prod
config.scrap_server_port = 31361;
module.exports = config;
