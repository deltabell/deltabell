var mongoose = require('mongoose');

// User Schema
var StreetSchema = mongoose.Schema({
    city: {type: String, required: true},
    streetName1: {type: String,required: false},
    buildingNumber: {type: String,required: false},
    buildingLetter: {type: String,required: false},
    streetName2: {type: String,required: false},
    buildingNumber2: {type: String,required: false},
    floorNumber: {type: Number,required: false},
    entranceNumber: {type: Number,required: false},
    apartmentsNumber: {type: Number,required: false},
    businessNumber: {type: Number,required: false},
    loc:{type: {type: String}, coordinates: []}
}, { versionKey: false });

var Street = module.exports = mongoose.model('Street', StreetSchema);
