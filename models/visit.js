var mongoose = require('mongoose');

// User Schema
var VisitSchema = mongoose.Schema({
	accountId: {
		type: String,
		required: false
	},
	visitTime: {
		type: Date,
		required: true
	},
	username: {
		type: String,
		required: false
	},
	name: {
		type: String,
		required: false
	},
    userType: {
        type: String,
        required: false
    },
    visitType: {
        type: String,
        required: true
    },
    apartmentId: {
        type: String,
        required: false
    },
    apartmentTitle: {
        type: String,
        required: false
    },
    navigatorDetails: {
        type: String,
        required: false
    }
}, { versionKey: false });

var Visit = module.exports = mongoose.model('Visit', VisitSchema);