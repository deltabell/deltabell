var Account = require('./account.js');
var Utils = require('../server/utils.js');

// To run the script
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var ApartmentSchema = Schema({
    matterportKey: {
        type: String,
        default: ""
    },
    apartmentTitle: {
        type: String,
        default: "נא מלא פרטי הדירה"
    },
    photographyDate: {
        type: Date
    },
    createdAt: {
        type: Date,
        default: Date.now() // UTC time
    },
    summary: {
        price: {
            type: Number,
            default: 0
        },
        rooms: {
            type: Number,
            default: 0
        },
        floor: {
            type: Number,
            default: 0
        },
        size: {
            type: String,
            default: 0
        },
        contact: {
            type: String,
            default: ""
        }
    },
    details: {
        description: {
            type: String,
            default: ""
        },
        features: {
            type: Array,
            default: []
        }
    },
    lat: {
        type: Number,
        default: 0
    },
    lng: {
        type: Number,
        default: 0
    },
    hgt: {
        type: Number,
        default: 0
    },
    cameraHeading: {
        type: Number,
        default: 0.0
    },
    sketchFile: {
        type: String,
        default: ""
    },
    gush: {
        type: String,
        default: ""
    },
    section: {
        type: String,
        default: ""
    },
    images: {
        type: Array,
        default: []
    },
    ownerId: {
        type: String, 
        default: ""
    },
    visitingCode: {
        type: String,
        default: ""
    },
    displayInBoard: {
        type: Boolean,
        default: true
    },
    contactName: {
        type: String,
        default: ""
    },
    contactPhoneNumber: {
        type: String,
        default: ""
    },
    contactEmail: {
        type: String,
        default: ""
    }
}, { versionKey: false });


ApartmentSchema.methods.updateDetails = function(params, callback) {
    Apartment.update({ _id: this._id }, { $set: params}, function(err, result) {
        callback(err);
	});
}

ApartmentSchema.methods.getOwner = function(callback) {
    if (!this.ownerId) {
        callback(null, null);
    } else {
        Account.getAccountById(this.ownerId, callback);
    }
}	

var Apartment = module.exports = mongoose.model('Apartment', ApartmentSchema);

// Return full file path by file name or empty if the file name is empty
function getFullFilePath(apartment_id, file_name) {
    return file_name === "" ? "" : "/apartments_data/" + apartment_id + "/" + file_name;
};

function setApartmentFilePaths(apartment){
    if (apartment) {
        // Combine file name with real file path for apartment file paths
        apartment.images = apartment.images.map(function (element){
            if (element["src"] == "")
            {
                return {"src":""};
            }
           else {
                return {"src": getFullFilePath(apartment.id, element["src"])};
            }
        })
        apartment.sketchFile = getFullFilePath(apartment.id, apartment.sketchFile);
    }
}

module.exports.createApartment = function(newApartment, callback){
    newApartment.save(callback);
};

module.exports.getApartmentById = function(id, callback){
    Apartment.findById(id, function(err, apartment) {
        setApartmentFilePaths(apartment)
        callback(err, apartment);
    });
};

module.exports.getMailingList
// Returns all apartments that the user is allowed to watch
module.exports.getAllApartments = function(includePrivate, callback){
    // filtering hidden apartments
    condition = includePrivate ? {} : {displayInBoard: true}; 
    Apartment.find(condition, function(err, apartments){
        if (apartments)
        {
            apartments.forEach(function(apartment){
                setApartmentFilePaths(apartment);
            });
        }
        callback(err, apartments);
    })
};

// Returns all the apartments that the user is managing
module.exports.getApartmentsByUserID = function(userId, isAdmin, callback){
    var query = {}
    if (!isAdmin){
        query={ownerId: userId};
    }
    Apartment.find(query).sort('apartmentTitle').exec(function(err, apartments){
        if (apartments)
        {
            apartments.forEach(function(apartment){
                setApartmentFilePaths(apartment);
            });
        }
        callback(err, apartments);
    })
};

var inneritanceFields = ["contactName", "contactPhoneNumber", "contactEmail"];

var loadInneritanceData = function(updatedApartment, oldData, callback){
    if (!updatedApartment.ownerId) {
        callback(null, updatedApartment);
    }
    else if (oldData.ownerId == updatedApartment.ownerId)
    {
        callback(null, updatedApartment);
    }
    else
    {
        Account.getAccountById(updatedApartment.ownerId, function(err, account){
            if (err){
                callback(err, null);
            }
            else
            {
                contactDetails = {};
                for (index in inneritanceFields)
                {
                    fieldName = inneritanceFields[index];
                    contactDetails[fieldName] = account[fieldName];
                }
                updatedApartment.updateDetails(contactDetails, function(err){
                    if (err) {
                        callback(err, null);
                    } else {
                        Apartment.getApartmentById(updatedApartment._id, function(err, apartment){
                            callback(null, apartment);
                        })
                    }
                });
            }
        });
    }
}

// Update apartment
module.exports.saveApartment = function(updatedData, callback){
    var query = {_id: updatedData._id};
    Apartment.findOneAndUpdate(query, updatedData, {upsert:true}, function(err, oldData){
        loadInneritanceData(oldData, updatedData, function(err, dataToReturn){
             callback(err, dataToReturn);
        })
        sendEmailAbountJoinApartment(oldData, updatedData);
    })   
};

var sendEmailAbountJoinApartment = function(oldData, updatedData){
    // If apartment's owner changed
    if (updatedData.ownerId && updatedData.ownerId != oldData.ownerId) 
    {
        Account.getAccountById(updatedData.ownerId, function(err, account){
            var text = "ב״ה" + "\n\n" +
                "שלום" + " " + account.name + ",\n\n" + 
                "קושרה לחשבונך מודעת דירה. נא עדכן את פרטיה דרך ׳החשבון שלי׳ באתר" + "\n\n" +
                "להתראות! דלתא בל" + "\n" + 
                "https://deltabell.co.il";
            email = account.username;

            Utils.sendEmail(
                {subject: "דלתא בל - שיוך מודעת דירה", 
                 emailTarget: email, 
                 text: text, 
                 callback: function(err, success){
                                if (!success) {
                                    Utils.printError('sending email about create a relation apartment to owner', err);
                                }
                            }
                }
            )
        })            
    }
};

// Update apartment
module.exports.deleteApartmentByID = function(apartmentId, callback){
    var query = {_id: apartmentId};
    Apartment.remove(query, function(err){
        callback(err);
    })
};

