var mongoose = require('mongoose');
var bcrypt = require('bcryptjs');
var PasswordGenerator = require('password-generator');

// User Schema
var AccountSchema = mongoose.Schema({
	username: {
		type: String,
		index:true,
		required: true
	},
	password: {
		type: String,
		required: true
	},
	name: {
		type: String,
		required: true
	},
	userType:{
		type: String,
		required: true,
		default: 'regular'
	},
	logoFilePath:{
		type: String
	},
	isActive:{
		type: Boolean,
		required: true,
		default: false
	},
	contactName:{
		type: String
	},
	contactEmail: {
		type: String
	},
	contactPhoneNumber: {
		type: String
	}
}, { versionKey: false });

AccountSchema.methods.activate = function() {
    this.isActive = true; this.save();
};

AccountSchema.methods.setDefaultValues = function() {
	this.contactName = this.contactName ? this.contactName : this.name;
	this.contactEmail = this.contactEmail ? this.contactEmail: this.username;
};

AccountSchema.methods.isAdmin = function() {
	return this.userType === 'admin';
};

// Returns basic data about account for use in client side
AccountSchema.virtual('publicDetails').get(function () {
    return {name: this.name,
			userType: this.userType,
			username: this.username,
			id: this._id,
            logoFilePath: this.logoFilePath,
			contactEmail: this.contactEmail,
			contactName: this.contactName,
			contactPhoneNumber: this.contactPhoneNumber};
});

AccountSchema.methods.setNewPassword = function(password, callback) {
	bcrypt.genSalt(10, function(err, salt) {
    	bcrypt.hash(password, salt, function (err, hash) {
        	if (err)
			{
				callback(err)
			}
			else {
                this.password = hash;
                this.save();
                callback(null)
            }
        }.bind(this))
    }.bind(this))
}

// Return full file path by file name or empty if the file name is empty
function getFullFilePath(account_id, file_name) {
    return file_name === "" ? "" : "/accounts_data/" + account_id + "/" + file_name;
};

function setAccountFilePaths(account){
    if (account) {
        account.logoFilePath = getFullFilePath(account.id, account.logoFilePath);
    }
}

var Account = module.exports = mongoose.model('Account', AccountSchema);

module.exports.createAccount = function(newAccount, callback){
	newAccount.setDefaultValues();
	bcrypt.genSalt(10, function(err, salt) {
        bcrypt.hash(newAccount.password, salt, function (err, hash) {
            newAccount.password = hash;
            newAccount.save(callback);
        })
    })
}

module.exports.getGeneratedPassword = function(){
	return PasswordGenerator(12, false);
}

module.exports.getAccountByUsername = function(username, callback){
	var query = {username: username};
	Account.findOne(query, function(err,account){
		setAccountFilePaths(account);
		callback(err,account)
	});
}

module.exports.getAccountById = function(id, callback){
	Account.findById(id, function(err,account){
		setAccountFilePaths(account);
		callback(err,account)
	});
}

module.exports.getAccountByIds = function(ids, callback){
	Account.find({
		'_id': { $in: ids }
	}, callback);
}

module.exports.comparePassword = function(candidatePassword, hash, callback){
	bcrypt.compare(candidatePassword, hash, function(err, isMatch) {
    	if(err) throw err;
    	callback(null, isMatch);
	});
}

// Update the account logo file path
module.exports.updateLogoFilePath = function(id, newLogoFilePath, callback){
    	var account = Account.findById(id, function(err, account){
		account.logoFilePath=newLogoFilePath;
		account.save(callback);
    })
};

// Returns all accounts
module.exports.getAllAccounts = function(callback){
    Account.find({}, function(err, accounts){
        callback(err, accounts);
    })
};

module.exports.getMailingList = function(callback){
	Account.find({}, 'username contactEmail', function(err, mailingList){
		callback(mailingList);
	});
};

// Updating account details.
module.exports.updateByUserName = function(username, details, callback){
	Account.getAccountByUsername(username, function(err, account){
		if (account)
		{
			Account.update({ _id: account._id }, { $set: details}, function(err, account) {
                if (err) {
                    callback(err, null);
                } else {
                    // return the updated account if no errors while updating
                    Account.getAccountByUsername(username, function(err, account){
                    	callback(null, account);
                    });
				}
            });
		}
		else {
        	callback('account with user name not exists', null);
		}
	}.bind(details))
}