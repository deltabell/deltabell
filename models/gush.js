var mongoose = require('mongoose');

// User Schema
var GushSchema = mongoose.Schema({
    shapeid: {type: Number,required: true},
	GUSH_NUM: {type: Number,required: true},
    LOCALITY_N: {type: String,required: false},
    COUNTY_NAM: {type: String,required: false},
    REGION_NAM: {type: String,required: false},
    SHAPE_Leng: {type: Number,required: false},
    SHAPE_Area: {type: Number,required: false},
    loc:{type: {type: String}, coordinates: []}
}, { versionKey: false });

var Gush = module.exports = mongoose.model('Gush', GushSchema);

