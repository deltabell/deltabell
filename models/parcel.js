var mongoose = require('mongoose');

// User Schema
var ParcelSchema = mongoose.Schema({
	gushNumber: {type: Number,required: true},
	parcelNumber: {type: Number,required: true},
	legalArea:{type: Number,required: false,},
    loc:{type: {type: String}, coordinates: []}
}, { versionKey: false });

var Parcel = module.exports = mongoose.model('Parcel', ParcelSchema);
