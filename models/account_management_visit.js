var mongoose = require('mongoose');

// User Schema
var AccountManagementVisitSchema = mongoose.Schema({
	accountId: {
		type: String,
		required: true
	},
	visitTime: {
		type: Date,
		required: true
	},
	username: {
		type: String,
		required: true
	},
	name: {
		type: String,
		required: true
	}
}, { versionKey: false });

var AccountManagementVisit = module.exports = mongoose.model('AccountManagementVisit', AccountManagementVisitSchema);