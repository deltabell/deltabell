// Packages
var router = require('express').Router();
var Config = require('../server/utils.js').getConfig();
var Axios = require('axios');

// getting mmi data from their website
router.post('/getMMIData', function(req,res){
    if (req.body) {
        let gush = req.body.gush;
        let parcel = req.body.parcel;
        var scrap_server_url = 'http://' + Config.scrap_server_ip + ':' + Config.scrap_server_port.toString() + '/getMMIData';
        Axios.post(scrap_server_url,{
            gush: gush,
            parcel: parcel
        }).then(function (response) {
            res.json(response.data);
        });
    } else {
        res.json({gush: null, parcel: null, result: []});
    }
});

// getting mavat data from their website
router.post('/getMavatData', function(req,res){
    if (req.body) {
        let gush = req.body.gush;
        let parcel = req.body.parcel;
        var scrap_server_url = 'http://' + Config.scrap_server_ip + ':' + Config.scrap_server_port.toString() + '/getMavatData';
        /*Axios.post(scrap_server_url,{
            gush: gush,
            parcel: parcel
        }).then(function (response) {
            res.json(response.data);
        });*/
        res.json({gush: null, parcel: null, result: {}});
    } else {
        res.json({gush: null, parcel: null, result: {}});
    }
});

module.exports = router;