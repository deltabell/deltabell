var router = require('express').Router();
var Apartment = require('../models/apartment.js');
var fs = require('fs');
var Utils = require('../server/utils');
var Config = Utils.getConfig();

// Getting all apartments that user is allowed to see
router.get('/all', function(req,res){
    var includePrivate = req.user && req.user.isAdmin();
    Apartment.getAllApartments(includePrivate, function(err,apartments){
        // If apartments data was returned
        if (apartments) {
            res.json({apartments: apartments});
        }
        else {
            res.json({message: "no apartments were found"});
        }
    });
});

// Getting all apartments that user is manging
router.get('/myApartments', function(req,res){
    if (req.user){
        var userId = req.user._id;
        var isAdmin = req.user && req.user.isAdmin();
        Apartment.getApartmentsByUserID(userId, isAdmin, function(err,apartments){
            // If apartments data was returned
            if (apartments) {
                res.json(apartments);
            } else {
                res.json({message: "no apartments were found"});
            }
        })
    } else {
        return(res.json({message: "user not connected"}));
    }
});

// Used to create new apartment
router.get('/createNewApartment', function(req,res){
    var newApartment = new Apartment();
    // Create the new Apartment Folder
    fs.mkdir(Config.storage_root_path + "data/apartments_data/" + newApartment._id, function(error){
        if (error){
            res.json({message: "new apartment creation failed"});
        }
        else {
            newApartment.save(function(err){
                if (err){
                    res.json({message: "new apartment creation failed"});
                } else {
                    res.json(newApartment);
                }
            });
        }
    });
});

router.post('/saveApartment', function(req,res){
    var apartment = req.body.apartment;
    Apartment.saveApartment(apartment, function(err){
        if (err){
            console.log(err);
            res.json({message:"save failed"})
        } else {
            res.json({message:"saved"})
        }
    });
});

router.post('/deleteApartment', function(req,res){
    var apartmentId = req.body.apartmentId;
    Apartment.deleteApartmentByID(apartmentId, function(err){
        if (err){
            console.log(err);
            res.json({message:"deletion failed"})
        } else {
            res.json({message:"deleted"})
        }
    });
});

// Getting apartment by id
router.get('/:id', function(req, res) {
    var apartmentId = req.params.id;

    // If it's demo apartment - get it from static file
    Apartment.getApartmentById(apartmentId, function (err, apartment) {
        if (err) {
            console.log(err)
            res.json({message: "apartment id not found"});
        }
        // If apartment data was returned
        else {
            res.json(apartment);
        }
    });
});

module.exports = router;

