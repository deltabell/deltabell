var router = require('express').Router();
var fs = require('fs');
var Axios = require('axios');
var Parcel = require('../models/parcel.js');
var Gush = require('../models/gush.js');
var Street = require('../models/street.js');
var Config = require('../server/utils.js').getConfig();
var GeoConvert = require('../client/common/js/geo_convert.js');
var tilesArray = Config.tiles_paths_array;

var getDistanceBetweenPoints = function(first, second){
    xDifferent = first.x - second.x;
    yDifferent = first.y - second.y;
    return Math.sqrt(xDifferent*xDifferent + yDifferent*yDifferent);
}

router.post('/getTilesForLocation', function(req,res) {
    rootTiles = [];
    var returnedArray = [];
    var positionLocation = {x: req.body.x, y: req.body.y};
    tilesArray.forEach(function(directory)
    {
        var tiles = JSON.parse(fs.readFileSync(Config.storage_root_path + directory + 'tiles_root.json', 'utf8'));
        var radius = tiles.root.boundingVolume.sphere[3];
        var rootLocation = {x: tiles.root.boundingVolume.sphere[0], 
                            y: tiles.root.boundingVolume.sphere[1]};

        // Checking which tile has the point
        if (getDistanceBetweenPoints(rootLocation, positionLocation) <= radius) { 
            // get nearest tail        
            var nearestLocation = null;
            var minDistance = Number.MAX_VALUE;
            tiles.root.children.forEach(function(tile){
                tileLocation = {x: tile.children[0].boundingVolume.sphere[0],
                                y: tile.children[0].boundingVolume.sphere[1],};
                distance = getDistanceBetweenPoints(tileLocation, positionLocation);
                if (distance < minDistance) {
                    tileLocation["url"] = tile.children[0].content.url;
                    tileLocation["radius"] = tile.children[0].boundingVolume.sphere[3];
                    minDistance = distance; nearestLocation = tileLocation;
                } 
            })

            // Making sure the position into nearestLocation
            tileRadius = nearestLocation.radius;
            xMaxLimit = nearestLocation.x + tileRadius; 
            xMinLimit = nearestLocation.x - tileRadius;
            yMaxLimit = nearestLocation.y + tileRadius; 
            yMinLimit = nearestLocation.y - tileRadius;
            if (positionLocation.x > xMaxLimit || positionLocation.x < xMinLimit || positionLocation.y > yMaxLimit || positionLocation.y < yMinLimit) {
                return; //Continue to next root file
            }

            // Defining limit of file numbers
            nearestPath = nearestLocation.url;
            nearestFirstNumber = Number(nearestPath.slice(11,14));
            nearestSecondNumber = Number(nearestPath.slice(16,19));
            firstMax = nearestFirstNumber + 3;
            firstMin = nearestFirstNumber - 3;
            secondMax = nearestSecondNumber + 3;
            secondMin = nearestSecondNumber - 3;
            
            // Filtering tiles by file numbers
            relevantTiles = tiles.root.children.filter(function(tile){
                path = tile.children[0].content.url;
                tailFirst = Number(path.slice(11,14));
                tailSecond = Number(path.slice(16,19));
                return (tailFirst >= firstMin &&
                        tailFirst <= firstMax &&
                        tailSecond >= secondMin &&
                        tailSecond <= secondMax);
            });
            
            // Push relevant tiles paths to returned array
            relevantTiles.forEach(function(tile){
                var path = directory + tile.children[0].content.url;
                returnedArray.push(path.replace('data/','/')); 
            });
        }
    })
    res.json({tiles: returnedArray})
});

// Retrning all streets data in polygon
router.post('/getStreetsByPolygon', function(req,res){
    var polygon = req.body.polygon;
    Street.find({loc: {$geoIntersects: {$geometry: {type: 'Polygon', coordinates: polygon}} }}, function (err, streets) {
        if (err) {
            res.json({streets: [], message: "error in query", err: err});
        } else {
            res.json({streets: streets});
        }
    });
});

// Finding the parcel of location sent
router.post('/getGeoDataByPoint', function(req,res){
    // First searching in our Database
    Parcel.find({loc: {$geoIntersects: {$geometry: {type: 'Point', coordinates:[req.body.lat,req.body.lng]}} }}, function (err, docs) {
        if (docs.length > 0){
            // return parcel from db
            res.json({
                gush: docs[0].gushNumber,
                parcel: docs[0].parcelNumber,
                polygon: docs[0].loc.coordinates,
                legalArea: docs[0].legalArea
            })
        } else {
            // searching in mapi
            var mapi_url = 'http://' + Config.scrap_server_ip + ':' + Config.scrap_server_port.toString() + '/getMapiData';
            Axios.post(mapi_url,{
                E: req.body.E,
                N: req.body.N
            }).then(function (response) {
                res.json(response.data);
            });
        }
    });
});

// Finding the location and polygon of requested gush parcel
router.post('/getGeoDataByGushParcel', function(req,res){
    var query = {gushNumber: Number(req.body.gush), parcelNumber: Number(req.body.parcel)}
    // First searching in our Database
    Parcel.find(query, function (err, docs) {
        if (docs.length > 0){
            // calulating point
            var maxlng=0;
            var minlng=99;
            var maxlat=0;
            var minlat=99;
            var polygon = docs[0].loc.coordinates;

            for (var i=0; i < polygon.length; i++){
                for (var j=0; j <polygon[i].length; j++){
                    var point = {lat: polygon[i][j][0], lng: polygon[i][j][1]};
                    if (point.lat > maxlat){maxlat=point.lat};
                    if (point.lat < minlat){minlat=point.lat};
                    if (point.lng > maxlng){maxlng=point.lng};
                    if (point.lng < minlng){minlng=point.lng};
                }
            }
            var avglat = (maxlat + minlat) / 2;
            var avglng = (maxlng + minlng) / 2;
            res.json({
                point: {lat: avglat, lng: avglng},
                polygon: polygon,
                legalArea: docs[0].legalArea
            })
        } else {
            res.json({
                point: null,
                polygon: null,
                legalArea: null
            })
        }
    });
});

// Activate account - when user click on confirm link in its email
router.get('/getGushData/:gushId', function(req, res){
    var gushId = Number(req.params.gushId);
    var query = {GUSH_NUM: gushId};
    Gush.find(query, function (err, docs) {
        if (docs.length > 0){
            res.json(docs[0]);
        } else {
            res.json(null);
        }
    });
})

module.exports = router;