var router = require('express').Router();
var Utils = require('../server/utils.js');
var Config = Utils.getConfig();

// getting the contact us request and sending the mail to the lits of mails below
router.post('/', function(req, res) {
    // Creating Authentication to gmail

    // Creating message
    var text = "התקבלה הודעת צור קשר מהאתר" + "\n";
    text +=  "פרטי ההודעה:" + "\n";
    text += "email: " + req.body.email + "\n";
    text += "phone: " + req.body.phone + "\n";
    text += "name: " + req.body.name + "\n";
    text += "message: " + req.body.message + "\n";

    var subject = 'הודעת צור קשר מ: ' + req.body.name;

    Utils.sendEmail(
        {subject: subject, 
         emailTarget: 'info@deltabell.net',
         text: text, 
         callback: function(error){
            if(error){
                console.log("error from contact us form")
                console.log(error);
                res.json({message:"error"});
            }else{
                // redirecting the client to the success form
                res.json({message:"message sent"});
            };
        }});
});

module.exports = router;

