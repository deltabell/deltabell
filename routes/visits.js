// Packages
var router = require('express').Router();

// Modules
var AccountManagementVisit = require('../models/account_management_visit.js');
var Apartment = require('../models/apartment.js');
var Account = require('../models/account.js');
var Visit = require('../models/visit.js');

// Helper
var Utils = require('../server/utils.js')

VISIT_TYPE_MAP = "map";
VISIT_TYPE_SINGLE_APARTMENT = "apartmentPage"
VISIT_TYPE_ACCOUNT = "accountManagement"

var saveVisit = function(req, res, visitType, params, callback){
    // set customated params by visit type
    var newVisit = new Visit(params);
    
    newVisit.visitType = visitType;
    newVisit.navigatorAgent = req.body.navigatorAgent;
    newVisit.visitTime = Date.now()
        
    user = req.user
    if (user){
        newVisit.accountId = user._id;
	    newVisit.username = user.username;
		newVisit.name = user.name;
        newVisit.userType = user.userType;
    }
    
    newVisit.save(function(err, visit){
        if (err) {
            Utils.printError(err, 'adding visit at ' + visitType);
            res.writeHead(400, 'Technical error');
            res.send();
        } else {
            res.sendStatus(200);
        }
    });
}

// Saving map page visits
router.put('/addMapPageVisit', function(req, res){
    saveVisit(req, res, VISIT_TYPE_MAP, {});  
});


// Saving visit of account in account management page
router.put('/addAccountManagmentVisit', function(req, res){
    saveVisit(req, res, VISIT_TYPE_ACCOUNT, {});
});

// Saving visit details in singleApartment page
router.put('/addApartmentVisit/:apartmentId', function(req, res){
    apartmentId = req.params.apartmentId
    Apartment.getApartmentById(apartmentId, function(err, apartment){
        if (err){
            Utils.printError(err, 'getting apartmentId: ' + apartmentId + ' for saving visit');
            res.writeHead(400, 'Technical error');
            res.send();  
        } else {
            params = {apartmentId: req.params.apartmentId,
                      apartmentTitle: apartment.apartmentTitle}
            saveVisit(req, res, VISIT_TYPE_SINGLE_APARTMENT, params)
        }
    })    
});

// Getting client visits to account pages, returns also accounts without visits
router.get('/getAccountManagmentVisits', function(req, res){
    Account.find({}, function(err, accounts){
        if (err)
        {
            res.json({errorMessage: "technical error"});
            Utils.printError(err, 'getting all accounts');
        } 
        else 
        {
            var notAdminAccounts = accounts.filter(function(account){return account.userType !== "admin"});
            AccountManagementVisit.find({}, function(err, allVisits){
                if (err){
                    res.json({errorMessage: "technical error"});
                    Utils.printError(err, 'getting all managment visits');
                } else {
                    var accountVisitsByUser = [];
                    // going on every account and init the object
                    notAdminAccounts.forEach(function(account){
                        // Init the returned object
                        accountVisitsByUser.push({
                            accountId: account._id.toString(),
                            accountDescription: account.name + " - " + account.username,
                            lastDay: 0,
                            lastWeek: 0,
                            lastMonth: 0,
                            total: 0
                        });
                        var index = accountVisitsByUser.length  - 1;
                        var accountVisits = allVisits.filter(function(visit){
                            return visit.accountId === accountVisitsByUser[index].accountId
                        });
                        accountVisits.forEach(function(visit){
                            compareDate = new Date();
                            compareDate.setDate(compareDate.getDate() - 1);
                            accountVisitsByUser[index].total = accountVisitsByUser[index].total + 1;
                            if (visit.visitTime >= compareDate){
                                accountVisitsByUser[index].lastDay = accountVisitsByUser[index].lastDay + 1;
                            };
                            compareDate.setDate(compareDate.getDate() - 6);
                            if (visit.visitTime >= compareDate){
                                accountVisitsByUser[index].lastWeek = accountVisitsByUser[index].lastWeek + 1;
                            };
                            compareDate.setDate(compareDate.getDate() - 23);
                            if (visit.visitTime >= compareDate){
                                accountVisitsByUser[index].lastMonth = accountVisitsByUser[index].lastMonth + 1;
                            };
                        })
                    });
                    accountVisitsByUser.sort(function(a,b){
                        return ((b.lastDay * 100) + (b.lastWeek * 10) + b.lastMonth) - ((a.lastDay * 100) + (a.lastWeek * 10) + a.lastMonth);
                    })
                    res.json({accountVisitsByUser: accountVisitsByUser});
                }
            })
        }
    });
})

// Getting all visits, the analysis executes in client side
router.get('/getAllVisits', function(req, res){
    // Getting account management visits
    Visit.find({}, function(err, visits){
        if (err) {
            res.json({errorMessage: "technical error"});
            Utils.printError(err, "getting all visits");
        } else {
            // sorting visits by date from late to early
            visits.sort(function(a, b){
                return (new Date(a.visitTime) < new Date(b.visitTime)) ? 1 : -1;
            });
            res.json({visits: visits});
        }
    })
})

// Getting apartment visit history
router.get('/getApartmentVisits/:apartmentId', function(req, res){ 
    var apartmentId = req.params.apartmentId;
    
    const TITLE_LAST_DAY = 'last 24 hours';
    const TITLE_LAST_WEEK = 'last 7 days';
    const TITLE_LAST_MONTH = 'last 30 days';
    const TITLE_WHOLE_VISITS = 'whole visits';

    // Default visits response
    var visitSummary = {[TITLE_LAST_DAY]: 0,
                        [TITLE_LAST_WEEK]: 0,
                        [TITLE_LAST_MONTH]: 0,
                        [TITLE_WHOLE_VISITS]: 0}
    
    // condition for not admin visitors
    var condition = {apartmentId: apartmentId,
                     visitType: VISIT_TYPE_SINGLE_APARTMENT,
                        $or: [{userType: { $exists: false }},
                              {userType: { $ne: 'admin'}}]
                    }

    // Get apartment's visits
    Visit.find(condition, function(err, visits){
        if (err) {
            Utils.printError(err, 'getting visits for apartment id: ' + apartmentId);
            res.json({errorMessage: 'technical error'})
        } 
        // when no visits
        else if (visits.length == 0) {
            res.json({visitSummary: visitSummary});        
        } 
        // when exists visits
        else {
            Apartment.getApartmentById(apartmentId, function(err, apartment){
                apartment.getOwner(function(err, owner){
                    if (owner){
                        // Filtering visits of apartment's owner
                        visits = visits.filter(function(visit){
                            return visit.username != owner.username;
                        })
                    } 
                    if (visits.length > 0) {
                        visitSummary[TITLE_WHOLE_VISITS] = visits.length;
                    
                        var daysBefore = {[TITLE_LAST_DAY]    :   1,
                                          [TITLE_LAST_WEEK]   :   7,
                                          [TITLE_LAST_MONTH]  :   30}

                        for (var title in daysBefore) {
                            fromDate = new Date(); 
                            fromDate.setDate(fromDate.getDate() - daysBefore[title]);
                            // get visits count for given last days number
                            visitSummary[title] = visits.filter(function(visit){
                                return visit.visitTime >= fromDate;
                            }).length
                        }
                    }
                    res.json({visitSummary: visitSummary})
                })
            })
            
        }
    })                          
});

module.exports = router;