var router = require('express').Router();
var utils = require('../server/utils.js');
var Config = utils.getConfig();

// getting the config from server by it's name
router.get('/:configName', function(req, res) {
    var configName = req.params.configName;
    res.json(Config[configName]);
});

module.exports = router;

