var router = require('express').Router();
var multer = require('multer');
var utils = require('../server/utils.js');
var Config = utils.getConfig();
var Account = require('../models/account');

var storage = multer.diskStorage({
    destination: function (req, file, cb) {
        var folder = "";
        switch(req.url) {
            case "/uploadApartmentFile": folder='data/apartments_data/' + req.body.apartmentId; break;
            case "/uploadAccountLogoFile": folder='data/accounts_data/' + req.user._id; break;
            case "/uploadTempFile": folder='data/temp/'; break;
            default: break;
        }
        cb(null, Config.storage_root_path + folder);
    },
    filename: function (req, file, cb) {
        cb(null, file.originalname)
    }
});
var upload = multer({ storage: storage })

router.post('/uploadApartmentFile', upload.single('file'), function(req,res){
    var fileName = '/apartments_data/' + req.body.apartmentId + '/' + req.file.originalname;
    res.json({message: "file uploaded", filePath: fileName})
});

router.post('/uploadAccountLogoFile', upload.single('file'), function(req,res){
    var fileName = '/accounts_data/' + req.user._id + '/' + req.file.originalname;
    // saving the new logo file name to account
    Account.updateLogoFilePath(req.user._id, req.file.originalname, function(err, account){
        if (err){
            console.log(err);
            res.json({message:"save failed"})
        } else {
            res.json({message: "file uploaded", filePath: fileName})
        }
    });
});

router.post('/uploadTempFile', upload.single('file'), function(req,res){
    res.json({message: "file uploaded", 
              filePath: '/data/temp/' + req.file.originalname});
});

module.exports = router;
