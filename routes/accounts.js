var express = require('express');
var router = express.Router();
var passport = require('passport');
var LocalStrategy = require('passport-local').Strategy;
var ExpressValidator = require('express-validator');
var Account = require('../models/account');
var Apartment = require('../models/apartment');
var Utils = require('../server/utils');
var Config = Utils.getConfig();
var Fs = require('fs');

// Validation - returns error if username in request already exist
router.get('/isUsernameNotExists', function(req,res){
    Account.getAccountByUsername(req.query.username, function(err, account){
        if (account){
            res.writeHead(400, 'WRONG! Username already exists.');
            res.send();
        }
        else {
            res.sendStatus(200);
        }
    })
})

router.get('/getAccountNames', function(req, res){
    query = req.query.excludeAdmin ? { userType: { $ne: 'admin' } } : {}
    console.log(query);
    Account.find(query, function(err, accounts){
        if (err){
            res.json({message: 'technical error'});
            Utils.printError(err, "getting all accounts");
        } else {
            accountNames = [];
            accounts.forEach(function(acc){
                accountNames.push({ accountId: acc._id,
                                    name: acc.name,
                                    username: acc.username });
            })
            res.json({accountNames: accountNames});
        }
    })   
})

// getting mailing list: account's email, contact email 
router.get('/getMailingList', function(req, res){
    Account.getMailingList(function(mailingList){
        accountsEmails = mailingList.map(function(accountEmails) { 
            return accountEmails.username;
        });
        contactsEmails = mailingList.map(function(accountEmails) {
            return accountEmails.contactEmail;
        });
        Apartment.getAllApartments(includePrivate=true, function(err, apartments){
            contactsEmails = contactsEmails.concat(
                apartments.map(function(apartment) {return apartment.contactEmail }));
            ownerIds = apartments.map(function(apartment) { return apartment.ownerId }).filter(function(id){return id});
            
            Account.getAccountByIds(ownerIds, function(err, accounts){
                if (err) {
                    Utils.printError(err, 'getting mailing list');
                    return res.json({errorMessage: 'failure while getting mailing list'}); 
                }
                else {
                    ownersEmails = accounts.map(function(account) { return account.username })
                    
                    returnArray = {'accounts'       : accountsEmails,
                                'contacts'       : contactsEmails,
                                'apartmentOwners': ownersEmails};  

                    for (groupType in returnArray) {
                        // Filter undefined email or invalid value
                        filteredArray = returnArray[groupType].filter(function(email) {
                            return email && email.includes('@')});
                        // Put filtered again, with unique values
                        returnArray[groupType] = Array.from(new Set(filteredArray));
                    }  

                    return res.json({mailingList: returnArray});
                }                                      
            })
        })
    })
})

// Activate account - when user click on confirm link in its email
router.get('/activate/:accountId', function(req, res){
    var account_id = Utils.getReversedString(req.params.accountId);
    Account.getAccountById(account_id, function (err, account){
        if (err){
            console.log(err);
            return res.redirect('/confirmationAccount/failure/' +
                "מצטערים, אישור החשבון נכשל. נא פנה אלינו דרך 'צור קשר'");
        }
        else if (!account){
            return res.redirect('/confirmationAccount/failure/' +
                "מצטערים, עבר זמן רב מידי מאז יצירת החשבון, נא צור חשבון חדש");
        }
        // There is account, no error
        else {
            account.activate();
            req.logIn(account, function (err) {
                console.log(err);
            })
            return res.redirect('/confirmationAccount/success/' + "ברכותינו! חשבונך אושר בהצלחה");
        }
    })
})

router.post('/sendEmail', function(req, res){
    var subject = req.body.subject.value
    var mailingList = req.body.mailingList
    var text = req.body.text
    var attachedFiles = req.body.attachedFiles
    Utils.sendEmail(
        {subject: subject, 
         emailTarget: mailingList.join(','), 
         text: text, 
         attachedFiles: attachedFiles, 
         callback: function(err){
                if (err)
                {
                    console.log(err);
                    return res.json({isSuccess: false, message: err});
                }
                else {
                    return res.json({isSuccess: true, message: "האימייל נשלח בהצלחה"});
                }
            }
        })                     
})

// Register User
router.post('/register', function(req, res){
    var name = req.body.nameOfUser;
    var username = req.body.username;
    var password = req.body.password;

    var newAccount = new Account({
        name: name,
        username: username,
        password: password,
        isActive: false
    });

    Account.createAccount(newAccount, function(err, account){
        if(err){
            return res.json({isRegistrationSuccess: false,
                             message: err});
        }
        else if (account) {
            Fs.mkdir(Config.storage_root_path + "data/accounts_data/" + account._id, function(err){
                if (err){
                    console.log(err);
                    return res.json({isRegistrationSuccess: false,
                                     message: 'הרישום נכשל מסיבה טכנית, נא פנה אלינו באמצעות ׳צור קשר׳'});
                }
                else {
                    var text = get_activation_email_message(account);
                    var callbackFunction = function(err)
                    {
                        if (err)
                        {
                            console.log(err);
                            return res.json({isRegistrationSuccess: false,
                                             message: 'הרישום נכשל מסיבה טכנית, נא פנה אלינו באמצעות ׳צור קשר׳'});
                        }
                        else {
                            return res.json(
                                {
                                    isRegistrationSuccess: true,
                                    account: req.user,
                                    message: "הרישום בוצע בהצלחה, להשלמת הרישום נא לחץ על הקישור שנשלח לכתובת דואר האלקטרוני אותה ציינת"
                                } );
                        }
                    }
                    options = {
                        subject: ' deltabell סיום הרשמה' +  ' - ' + account.name, 
                         emailTarget: account.username, 
                         text: text,
                         callback: callbackFunction
                    }
                    Utils.sendEmail(options);
                }
            });
        }
        else
        {
            return res.json(
                {isRegistrationSuccess: false,
                    message: 'הרישום נכשל מסיבה שאינה ידועה, נא פנה אלינו באמצעות ׳צור קשר׳'});
        }
    });
});

passport.serializeUser(function(user, done) {
  done(null, user.id);
});

passport.deserializeUser(function(id, done) {
  Account.getAccountById(id, function(err, user) {
    done(err, user);
  });
});

passport.use(new LocalStrategy(
    function(username, password, done) {
        Account.getAccountByUsername(username, function(err, account){
            if(err) throw err;
            if(!account){
                return done(null, false, {message: 'Unknown User'});
            }

            Account.comparePassword(password, account.password, function(err, isMatch){
                if(err) throw err;
                if(isMatch){
                    return done(null, account);
                } else {
                    return done(null, false, {message: 'Invalid password'});
                }
            });
        });
    }));

router.post('/changePassword', function(req, res){
    var username = req.body.username;
    var currentPassword = req.body.currentPassword;
    var newPassword = req.body.newPassword;

    Account.getAccountByUsername(username, function(err, account) {
        if (err){
            return res.json({isSuccess: false, message: "אירעה תקלה טכנית בעת שינוי הסיסמה"});
        }
        else if (!account){
            return res.json({isSuccess: false, message: "שם משתמש לא קיים"});
        }
        // There is account
        else
        {
            Account.comparePassword(currentPassword, account.password, function(err, isMatch) {
                if (err) {
                    return res.json({isSuccess: false, message: "אירעה תקלה טכנית בעת שינוי הסיסמה"});
                }
                if (!isMatch) {
                    return res.json({isSuccess: false, message: "הסיסמה הנוכחית שהזנת אינה נכונה"});
                }
                else {
                    account.setNewPassword(newPassword, function(err){
                        if (err)
                        {
                            return res.json({isSuccess: false, message: "אירעה תקלה טכנית בעת שינוי הסיסמה"});
                        }
                        else {
                            return res.json({isSuccess: true, message: "הסיסמה שונתה בהצלחה"});
                        }
                    })
                }
            })
        }
    })

});

router.post('/login', function(req, res, next) {
    passport.authenticate('local', function(err, user, info) {
        if (err)
        {
            res.json({isAccountExists: false});
        }
        if (!user) {
            return res.json({isAccountExists: false});
        }
        else if (user) {
            if (user.isActive) {
                req.logIn(user, function(err) {
                    if (err) {
                        res.json({isAccountExists: false});
                    }
                    return res.json({isAccountExists: true, account: req.user.publicDetails});
                });
            }
            else {
                var text = get_activation_email_message(user);
                Utils.sendEmail(
                    {subject: 'deltabell סיום הרשמה', 
                     emailTarget: user.username, 
                     text: text, 
                     callback: function(err){
                        if (err)
                        {
                            Utils.printError('send_complete_registration_email', err);
                        }
                        // account exists but not activated
                        return res.json({isAccountExists: true, account: null});
                     }
                })
            }
        }
    })(req, res, next);
});

function get_activation_email_message(account){
    var reversed_id = Utils.getReversedString(account._id.toString());
    var url_to_activate = "http://" + Config.hostname + "/api/accounts/activate/" + reversed_id;

    // Creating message
    var text =  "ב׳׳ה" + "\n";
    text +=  "ברכות לרגל הרשמתך לאתר deltabell" + "\n";
    text +=  "להשלמת ההרשמה, נא לחץ על הקישור הבא: " + "\n";
    text += url_to_activate;

    return text;
}

router.get('/logout', function(req, res){
	req.logout();
    res.json({message: "logout executed successfully"});
});

// This function calls from page Forgot password
router.get('/generateNewPassword', function(req, res){
    var email = req.query.email;
    Account.getAccountByUsername(email, function(err, account){
        if (err){
            console.log('error in sendPasswordToEmail: ' + err.message);
            res.json({isPasswordSent: false, message: "שליחת הסיסמה נכשלה, נא פנה אלינו באמצעות דף ׳צור קשר׳"})
        }
        else if (!account) {
            res.json({isPasswordSent: false, message: "מצטערים, לא קיים חשבון עבור כתובת דואר אלקטרוני זו"})
        }
        // All o.k.
        else {
            var generatedPassword = Account.getGeneratedPassword();
            account.setNewPassword(generatedPassword, function(err){
                var text =  "ֿב״ה" + "\n" +
                    "הסיסמה החדשה שלך: " + generatedPassword + "\n" +
                    "להתראות, דלתא בל"
                Utils.sendEmail({subject: "הסיסמה שלך לדלתא בל", 
                                 emailTarget: email, 
                                 text: text, 
                                 callback: function(err, success){
                                    if (success) {
                                        res.json({isPasswordSent: true, message: "הסיסמה החדשה נשלחה לכתובת האימייל אותה ציינת. להתראות!"})
                                    }
                                    else {
                                        res.json({isPasswordSent: false, message: "שליחת הסיסמה נכשלה, נא פנה אלינו באמצעות דף ׳צור קשר׳"})
                                    }
                                }})
            });
        }
    })
})

router.get('/getMyAccount', function(req, res){
    if(req.user){
        res.json({isAccountExists: true, account: req.user.publicDetails});
    }
    else
    {
        res.json({isAccountExists: false});
    }
})


// Delete the logo file path from account
router.get('/deleteLogoFilePath', function(req,res){
    Account.updateLogoFilePath(req.user._id, "", function(err, account){
        if (err){
            console.log(err);
            res.json({message:"deletion failed"})
        } else {
            res.json({message: "logoFilePath deleted"})
        }
    });
});


// Generic function to update account details.
router.post('/update', function(req, res) {
    var username = req.body.username;
    var details = req.body.details;
    Account.updateByUserName(username, details, function(err, account){
        if (err)
        {
            res.json({isSuccess: false, message: "כשל בשמירת פרטי החשבון"});
        }
        else {
            // The function returns updatedAccount if all no errors
            res.json({isSuccess: true, message: "פרטי חשבון נשמרו בהצלחה",
                      updatedAccount: account.publicDetails});
        }
    })
});


// gets list of accounts for auto complete purpose
router.get('/getUserList', function(req,res){
    Account.getAllAccounts(function(err, accounts){
        if (err){
            console.log(err);
            res.json({message:"getting accounts failed"})
        } else {
            res.json(accounts);
        }
    });
});

module.exports = router;