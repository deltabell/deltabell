// Packages
var Path = require('path');
var Express = require('express');
var BodyParser = require('body-parser');
var Passport = require('passport');
var Mongoose = require('mongoose');
var Session = require('express-session');
var fs = require('fs');
var https = require('https');
var utils = require('./server/utils.js');
var MongoStore = require('connect-mongo')(Session);

// Setting Configurations
var Config = utils.getConfig();

// Set up the mongodb connection
Mongoose.connection.on('error', console.error.bind(console, 'connection error:'));
Mongoose.connection.on('open',function(ref){console.log('connected to mongo server')});
Mongoose.connect(Config.connection_string, {server : {reconnectTries: Number.MAX_VALUE} });

if (Config.isProduction()) {
    // Catch all unhandled error
    process.on('uncaughtException', function (err) {
        // Send email about error
        var options = {
            subject: 'Error on production server, catched', 
            emailTarget: 'oriel181@gmail.com,deltabell.israel@gmail.com', 
            text: 'b.h.\n\nstacktrace:\n' + err.stack, 
            callback: function(err) { 
                if (err) { utils.printError('sending an email about error', err); }
                else { console.log('Email sent about error'); } 
            } 
        }
        utils.sendEmail(options);  
    });
}

// App
var app = Express();
//Defines all of the the static directories - needs to be first for performance
app.use(Express.static(Path.join(Config.storage_root_path, '/data/')));
app.use(Express.static(Path.join(Config.storage_root_path, '/data/cesium_3D/')));
app.use(Express.static(Path.join(__dirname, '/public/')));

// Express Session
// 2.4.17 zushe - added a param saveUninitialized: false, for decrease the count of stored session
// This param tells express to not save request simple requests (more: https://github.com/expressjs/session#saveuninitialized)

app.use(Session({
    secret: 'Deltabell Ltd',
    saveUninitialized: true,
    resave: true,
    store: new MongoStore({ mongooseConnection: Mongoose.connection }),
    saveUninitialized: false,
}));

// Passport init
app.use(Passport.initialize());
app.use(Passport.session());

// configure app to use BodyParser()
// this will let us get the data from a POST calls
app.use(BodyParser.urlencoded({ extended: true }));
app.use(BodyParser.json());

// Setting the SSL configuration
//require('ssl-root-cas').addFile('server.crt');
//app.enable('trust proxy');

// Add a handler to inspect the req.secure flag (see
// http://expressjs.com/api#req.secure). This allows us
// to know whether the request was via http or https.

// These api functions called in background from 'map' page. 
// 'http' page can't create 'https' request
// Because 'map' should work with 'http', server knows to not replace 'http' to 'https' when are called
API_FUNCTIONS_ENABLED_HTTP = ['getGeoDataByPoint',
                              'getGeoDataByGushParcel',
                              'getMyAccount',
                              'addAccountManagmentVisit',
                              'tiles_paths_array',
                              'getTilesForLocation',
                              'getMMIData',
                              'getStreetsByPolygon',
                              'getMavatData',
                              'getGushData',
                              'isUsernameNotExists',
                              'login',
                              'apartments/all',
                              'logout',
                              'addMapPageVisit']

var isUrlBlockedForHttp = function(reqUrl){
    return !reqUrl.toLowerCase().includes('/map') &&
        API_FUNCTIONS_ENABLED_HTTP.every(function(element, index, array){
            return !reqUrl.includes(element);
        })
}

// app.use (function (req, res, next) {
//     var redirectTo;
    
//     if (req.secure) {
//         if (req.url.toLowerCase().includes('/map')){
//             // Map page have to works without https
//             redirectTo = 'http:/';
//         } 
//     } else {
//         if (isUrlBlockedForHttp(req.url))
//         {
//             redirectTo = 'https:/';
//         }
//     }
//     if (redirectTo){ 
//         res.redirect(redirectTo + req.headers.host + req.url); 
//     } else { 
//         // continue with original url
//         next(); 
//     }
// })
var options = {
    key: fs.readFileSync('server/keys/deltabell.co.il.key'),
    cert: fs.readFileSync('server/keys/deltabell.co.il.crt')
}


// Register our Routes
app.use('/api/accounts',    require('./routes/accounts.js'));
app.use('/api/apartments',  require('./routes/apartments.js'));
app.use('/api/configs',  require('./routes/configs.js'));
app.use('/api/files',  require('./routes/files.js'));
app.use('/api/geo',  require('./routes/geo.js'));
app.use('/contactUs',       require('./routes/contactUs.js'));
app.use('/api/crawlers',  require('./routes/crawlers.js'));
app.use('/api/visits', require('./routes/visits.js'));
 
// sending the main page of the app
app.get('*', function (req, res) {
    utils.loadOrReplaceMetaTags(req.url, function(updatedFileStream){
        if (updatedFileStream) {
            res.end(updatedFileStream);
        } else {
            res.sendFile(Path.join(__dirname, './index.html'));
        }
    });
});

// Listening to ssl
https.createServer(options, app).listen(Config.https_port, function () {
    console.log("Listening on port " + Config.https_port.toString());
});

app.listen(Config.http_port, function(){
    console.log("Listening on port " + Config.http_port.toString());
});




