var GovMapGisApiIsIE = navigator.appVersion.indexOf('MSIE') >= 0;
//var govmapUrl = getScriptHostUrl();
//var urlWithProtocol = getUrlWithProtocol();
var govmapUrl = "www.govmap.gov.il";
var urlWithProtocol = "http://www.govmap.gov.il";
var mapIframeId = "";
var sClientSessionGuid;
var mapLoaded = false;
var bMapInProcess = false;
var bMapReceiverAttached = false;
var dictApiCallStatus = {};
var aApiCallDelegate = new Array();
var runingDelegateTick = 0;
var handleApiIntervalId = null;
var apiCriticalSection = false;
var dictPostMsgRequest = {};
var RENDERER_TYPE = {
    SIMPLE_RENDERER: 0,
    SIMPLE_PICTURE_RENDERER: 1,
    JENKS_NATURAL_BREAKS_RENDERER: 2,
    EQUAL_INTERVAL: 3,
    QUANTILE: 4
};
function validJSON(json)
{
    return (/^[\],:{}\s]*$/.test(json.replace(/\\["\\\/bfnrtu]/g, '@').
            replace(/"[^"\\\n\r]*"|true|false|null|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?/g, ']').
            replace(/(?:^|:|,)(?:\s*\[)+/g, '')));
}

function postMapRequest(oRequest)
{
    oRequest.requestId = getRequestId(oRequest.requestType);
    getMapIframeWindow().postMessage(JSON.stringify(oRequest), '*');
}

function getMapIframeWindow()
{
    if (mapIframeId == "")
    {
        mapIframeId = getMapIframeId();
    }

    return document.getElementById(mapIframeId).contentWindow;
}

function getMapIframeId()
{
    var mapUrl = urlWithProtocol + "/map.aspx";
    var iframes = document.getElementsByTagName("iframe");
    for (var i = 0, len = iframes.length; i < len; i++)
    {
	    if (!iframes[i].getAttribute("src"))
			continue;
			
        var src = iframes[i].getAttribute("src").toLowerCase();
        if (src.indexOf(mapUrl) == -1)
            continue;

        return iframes[i].id;
    }

    return "";
}

function handleApiQueue()
{
    if (!doProcess())
        return;

    if (aApiCallDelegate.length == 0)
    {
        stopApiQueueProcessing();
        return;
    }

    apiCriticalSection = true;
    var delegateObj = aApiCallDelegate[0];
    aApiCallDelegate.splice(0, 1);
    delegateObj.callback();
    
    apiCriticalSection = false;
}

function getRequestId(requestType)
{
    bMapInProcess = true;
    runingDelegateTick = (new Date).getTime();
    var requestId = requestType + runingDelegateTick;
    dictApiCallStatus[requestId] = true;
    setTimeout(function () { checkProcessStatus(requestId); }, 10000);

    return requestId;
}

function checkProcessStatus(requestId)
{
    if (dictApiCallStatus.hasOwnProperty(requestId) &&
        dictApiCallStatus[requestId] == true)
    {
        bMapInProcess = false;
        delete dictApiCallStatus[requestId];
    }    
}

function addDelegateToQueue(requestParams, requestType)
{
    aApiCallDelegate.push({ "callback": function () { postMapRequest(requestParams); }, "requestType": requestType });
    startApiQueueProcessing();        
}

function startApiQueueProcessing()
{
    if (handleApiIntervalId == null)
        handleApiIntervalId = setInterval(handleApiQueue, 100);
}

function stopApiQueueProcessing()
{
    if (handleApiIntervalId != null)
    {
        clearInterval(handleApiIntervalId);
        handleApiIntervalId = null;
    }
}

function doProcess()
{
    return !bMapInProcess && mapLoaded && bMapReceiverAttached;
}

function GovMapGisApi_receiver(evt)
{
    if (urlWithProtocol.indexOf(evt.origin) == 0)
    {
        if (!validJSON(evt.data))
            return;

        var obj = JSON.parse(evt.data);
        var callbackFunction = null;

        switch (obj.context)
        {
            case "rainRadar":
                callbackFunction = eval(obj.data.callback);
                callbackFunction(obj.data.result);

                break;
            case "APIGetIntersectsFeatures":
            case "AddressToLotParcel":
            case "LotParcelToAddress":                
            case "apiWatchGPSLocation":
                try
                {
                    callbackFunction = eval(obj.data.callback);
                    callbackFunction(obj.data.result);
                }
                catch (e)
                {
                }         
                break;

            case "GetXY":
                try
                {
                    CallbackFunction = eval(obj.data.callback);
                    CallbackFunction(obj.data.x, obj.data.y);
                }
                catch (e) {
                }

                break;

            case "Geocode":
                try {
                    CallbackFunction = eval(obj.data.callback);
                    CallbackFunction(obj.data.x,
                                     obj.data.y,
                                     obj.data.resultCode,
                                     obj.data.multipleResult);
                }
                catch (e) {
                }

                break;

            case "GeocodeResultCodeOnly":
                try
                {
                    CallbackFunction = eval(obj.data.callback);
                    CallbackFunction(obj.data.resultCode);
                }
                catch (e) {
                }
                
                break;

            case "DrawRectangle":
                try
                {
                    CallbackFunction = eval(obj.data.callback);
                    CallbackFunction(obj.data.xmin,
                                     obj.data.ymin,
                                     obj.data.xmax,
                                     obj.data.ymax);
                }
                catch (e)
                {
                }

                break;
            case "ExtentChange":                
                try
                {
                    CallbackFunction = eval(obj.data.callback);
                    CallbackFunction(obj.data.xmin,
                                     obj.data.ymin,
                                     obj.data.xmax,
                                     obj.data.ymax,
                                     obj.data.scale);
                }
                catch (e) {
                }

                break;

            case "DrawCircle":
                try
                {
                    CallbackFunction = eval(obj.data.callback);
                    CallbackFunction(obj.data.centerX,
                                     obj.data.centerY,
                                     obj.data.radius);
                }
                catch (e) {
                }

                break;

            case "DrawPolygon":
                try
                {
                    CallbackFunction = eval(obj.data.callback);
                    CallbackFunction(obj.data.polygonWKT);
                }
                catch (e) {
                }

                break;

            case "VisibleLayers":
                try
                {
                    CallbackFunction = eval(obj.data.callback);
                    CallbackFunction(obj.data.visibleLayers);
                }
                catch (e) {
                }

                break;

            case "SelectFeatures":
                try
                {
                    CallbackFunction = eval(obj.data.callback);
                    CallbackFunction(obj.data.ids);
                }
                catch (e) {
                }

                break;

            case "MapLoaded":                
                sClientSessionGuid = obj.data.client_guid;
                mapLoaded = true;
                break;

            case "RequestProcessed":
                checkProcessStatus(obj.data.requestId);
                break;

            case "MapReceiverAttached":
                bMapReceiverAttached = true;
                break;
 
        }
    }
}

if (!GovMapGisApiIsIE) 
{
    window.addEventListener('message', GovMapGisApi_receiver, false);
}
else {
    window.attachEvent('onmessage', GovMapGisApi_receiver);
}

function GovMapGisApi_AddUserPoints(UserKey, XArray, YArray, SymbolUrlArray, SymbolTypeArray, SymbolWidthArray, SymbolHeightArray, SymbolAnchorOffsetXArray, SymbolAnchorOffsetYArray, BubbleContentUrlArray, TooltipTextArray, ClearExisting, BubbleHeight, BubbleWidth)
{
    var request =
        {
            'requestType': 'AddUserPoints',
            'xArray': XArray,
            'yArray': YArray,
            'symbArray': SymbolUrlArray,
            'symbTypArray': SymbolTypeArray,
            'SymbolWidthArray': SymbolWidthArray,
            'SymbolHeightArray': SymbolHeightArray,
            'SymbolAnchorOffsetXArray': SymbolAnchorOffsetXArray,
            'SymbolAnchorOffsetYArray': SymbolAnchorOffsetYArray,
            'BubbleContentUrlArray': BubbleContentUrlArray,
            'TooltipTextArray': TooltipTextArray,
            'ClearExisting': ClearExisting,
            'UserKey': UserKey,
            'BubbleHeight': BubbleHeight,
            'BubbleWidth':BubbleWidth
    };    

    if (!doProcess())
    {
        addDelegateToQueue(request, "AddUserPoints");
        return;
    }

    postMapRequest(request);
}

function GovMapGisApi_AddUserLines(UserKey, WKTArray, SymbolsJson, BubbleContentUrl, UrlContentsArray, TooltipTextArray, ClearExisting, BubbleHeight, BubbleWidth)
{
    var request =
        {
            'requestType': 'AddUserLines',
            'WKTArray': WKTArray,
            'SymbolsJson': SymbolsJson,
            'BubbleContentUrl': BubbleContentUrl,
            'UrlContentsArray':UrlContentsArray,
            'TooltipTextArray': TooltipTextArray,
            'ClearExisting': ClearExisting,
            'UserKey': UserKey,
            'BubbleHeight': BubbleHeight,
            'BubbleWidth': BubbleWidth
        };

    if (!doProcess())
    {
        addDelegateToQueue(request, "AddUserLines");
        return;
    }

    postMapRequest(request);
}
function GovMapGisApi_AddUserPolygons(UserKey, WKTArray, SymbolsJson, BubbleContentUrl, UrlContentsArray, TooltipTextArray, ClearExisting, BubbleHeight, BubbleWidth)
   
{
    var request =
        {
            'requestType': 'AddUserPolygons',
            'WKTArray': WKTArray,
            'SymbolsJson': SymbolsJson,
            'BubbleContentUrl': BubbleContentUrl,
            'UrlContentsArray': UrlContentsArray,
            'TooltipTextArray': TooltipTextArray,
            'ClearExisting': ClearExisting,
            'UserKey': UserKey,
            'BubbleHeight': BubbleHeight,
            'BubbleWidth': BubbleWidth
        };

    if (!doProcess())
    {
        addDelegateToQueue(request, "AddUserPolygons");
        return;
    }

    postMapRequest(request);
}
function GovMapGisApi_SetDefaultTool()
{
    var request = {
        "requestType": "SetDefaultTool"
    };

    if (!doProcess())
    {
        addDelegateToQueue(request, "SetDefaultTool");
        return;
    }

    postMapRequest(request);
}

function GovMapGisApi_GetXY(UserKey, CallbackFunctionName)
{
    var request = {
        "requestType": "GetXY",
        "UserKey": UserKey,
        "CallbackFunctionName": CallbackFunctionName
    };

    if (!doProcess())
    {
        addDelegateToQueue(request, "GetXY");
        return;
    }

    postMapRequest(request);
}

function GovMapGisApi_GeocodeString(UserKey, keyWord, CallbackFunctionName)
{
    var request = {
        "requestType": "GeocodeString",
        "UserKey": UserKey,
        "keyWord": keyWord,
        "CallbackFunctionName": CallbackFunctionName,
        "searchType": "0",
        "showBubble": false
    };

    if (!doProcess())
    {
        addDelegateToQueue(request, "GeocodeString");
        return;
    }

    postMapRequest(request);
}

function GovMapGisApi_GetSearchAccuracy(keyWord, CallbackFunctionName)
{
    var request = {
        "requestType": "GeocodeResultCodeOnly",
        "keyWord": keyWord,
        "CallbackFunctionName": CallbackFunctionName,
        "searchType": "2"
    };

    if (!doProcess())
    {
        addDelegateToQueue(request, "GeocodeResultCodeOnly");
        return;
    }

    postMapRequest(request);
}

function GovMapGisApi_GeocodeAndZoom(keyWord, CallbackFunctionName, showBubble)
{
    var request = {
        "requestType": "GeocodeAndZoom",
        "keyWord": keyWord,
        "CallbackFunctionName": CallbackFunctionName,
        "showBubble": (showBubble != null) ? showBubble : false,
        "searchType": "1"
    };

    if (!doProcess())
    {
        addDelegateToQueue(request, "GeocodeAndZoom");
        return;
    }

    postMapRequest(request);
}

function GovMapGisApi_ZoomToXYAndScaleLevel(X, Y, Level, bAddMarker)
{
    var request = {
        "requestType": "ZoomToXYAndScaleLevel",
        "x": X,
        "y": Y,
        "level": Level,
        "bAddMarker": bAddMarker
    };

    if (!doProcess())
    {
        addDelegateToQueue(request, "ZoomToXYAndScaleLevel");
        return;
    }

    postMapRequest(request);
}

function GovMapGisApi_SearchByFieldAndValue(layerID, fieldName, fieldValue, showBubble)
{
    var request = {
        "requestType": "SearchByFieldNameAndValue",
        "layerID": layerID,
        "fieldName": fieldName,
        "fieldValue": fieldValue,
        "showBubble": (showBubble != null) ? showBubble : false
    };

    if (!doProcess())
    {
        addDelegateToQueue(request, "SearchByFieldNameAndValue");
        return;
    }

    postMapRequest(request);
}

//Topocad JS API
function GovMapGisApi_DrawRectangle(UserKey, CallbackFunctionName, bClearCurrentDrawing, bContinousMode)
{
    var request = {
        "requestType": "DrawRectangle",
        "UserKey": UserKey,
        "CallbackFunctionName": CallbackFunctionName,
        "bClearCurrentDrawing": bClearCurrentDrawing,
        "bContinousMode": bContinousMode
    };

    if (!doProcess())
    {
        addDelegateToQueue(request, "DrawRectangle");
        return;
    }

    postMapRequest(request);
}

function GovMapGisApi_DrawCircle(UserKey, CallbackFunctionName, bClearCurrentDrawing, bContinousMode)
{
    var request = {
        "requestType": "DrawCircle",
        "UserKey": UserKey,
        "CallbackFunctionName": CallbackFunctionName,
        "bClearCurrentDrawing": bClearCurrentDrawing,
        "bContinousMode": bContinousMode
    };

    if (!doProcess())
    {
        addDelegateToQueue(request, "DrawCircle");
        return;
    }

    postMapRequest(request);
}

function GovMapGisApi_DrawPolygon(UserKey, CallbackFunctionName, bClearCurrentDrawing, bContinousMode)
{
    var request = {
        "requestType": "DrawPolygon",
        "UserKey": UserKey,
        "CallbackFunctionName": CallbackFunctionName,
        "bClearCurrentDrawing": bClearCurrentDrawing,
        "bContinousMode": bContinousMode
    };

    if (!doProcess())
    {
        addDelegateToQueue(request, "DrawPolygon");
        return;
    }

    postMapRequest(request);
}

function GovMapGisApi_ClearDrawing()
{
    var request = {
        "requestType": "ClearDrawing"
    };

    if (!doProcess())
    {
        addDelegateToQueue(request, "ClearDrawing");
        return;
    }

    postMapRequest(request);
}

function GovMapGisApi_SubscribeToVisibleLayersChange(CallbackFunctionName)
{
    var request = {
        "requestType": "VisibleLayerChange",
        "CallbackFunctionName": CallbackFunctionName
    };

    if (!doProcess())
    {
        addDelegateToQueue(request, "VisibleLayerChange");
        return;
    }

    postMapRequest(request);
}

function GovMapGisApi_SetActiveBackground(BGType)
{
    var request = {
        "requestType": "Set_BG",
        "bgType": BGType
    };

    if (!doProcess())
    {
        addDelegateToQueue(request, "Set_BG");
        return;
    }

    postMapRequest(request);
}

function GovMapGisApi_SearchByFieldAndValues(layerID, fieldName, fieldValues, bHighlight)
{
    var request = {
        "requestType": "SearchByFieldNameAndValues",
        "layerID": layerID,
        "fieldName": fieldName,
        "fieldValues": fieldValues.join(","),
        "bHighlight": bHighlight
    };

    if (!doProcess())
    {
        addDelegateToQueue(request, "SearchByFieldNameAndValues");
        return;
    }

    postMapRequest(request);
}


function GovMapGisApi_SearchFeature(QueryName, fieldValues, bFlash, arrAdditionalLayers, UserKey, FillColor, OutLineColor)
{
    var request = {
        "requestType": "SearchFeature",
        "queryName": QueryName,
        "fieldValues": fieldValues.join(','),
        "arrAdditionalLayers": arrAdditionalLayers.join(','),
        "bFlash": bFlash,
        "UserKey": UserKey,
        "fillColor": FillColor,
        "outLineColor": OutLineColor
    };

    if (!doProcess())
    {
        addDelegateToQueue(request, "SearchFeature");
        return;
    }

    postMapRequest(request);
}

function GovMapGisApi_SelectFeaturesOnMap(CallbackFunctionName, QueryName, ContinousMode, bClearCurrentDrawing, UserKey, FillColor, OutLineColor)
{
    var request = {
        "requestType": "SelectFeaturesOnMap",
        "queryName": QueryName,
        "continousMode": ContinousMode,
        "bClearCurrentDrawing": (!bClearCurrentDrawing) ? false : true,
        "callbackFunctionName": CallbackFunctionName,
        "UserKey": UserKey,
        "fillColor": FillColor,
        "outLineColor": OutLineColor
    };

    if (!doProcess())
    {
        addDelegateToQueue(request, "SelectFeaturesOnMap");
        return;
    }

    postMapRequest(request);
}

function GovMapGisApi_ZoomToDrawing()
{
    var request = {
        "requestType": "ZoomToDrawing"
    };

    if (!doProcess())
    {
        addDelegateToQueue(request, "ZoomToDrawing");
        return;
    }

    postMapRequest(request);
}

function GovMapGisApi_SubscribeToMapExtentChange(CallbackFunctionName)
{
    var request = {
        "requestType": "ExtentChange",
        "callbackFunctionName": CallbackFunctionName
    };

    if (!doProcess())
    {
        addDelegateToQueue(request, "ExtentChange");
        return;
    }

    postMapRequest(request);
}

function GovMapGisApi_FilterLayer(layerID, whereClause, zoomToExtent)
{
    var request = {
        "requestType": "FilterLayer",
        "layerID": layerID,
        "whereClause": whereClause,
        "zoomToExtent": zoomToExtent
    };

    if (!doProcess())
    {
        addDelegateToQueue(request, "FilterLayer");
        return;
    }

    postMapRequest(request);
}

function GovMapGisApi_DisplayPolygon(WKTarray, ClearCurrentDrawing, OutLineColor, FillColor, NameArray)
{
    var request = {
        "requestType": "DisplayPolygon",
        "WKTarray": WKTarray,
        "nameArray": NameArray,
        "clearCurrentDrawing": ClearCurrentDrawing,
        "fillColor": FillColor,
        "outLineColor": OutLineColor
    };

    if (!doProcess())
    {
        addDelegateToQueue(request, "DisplayPolygon");
        return;
    }

    postMapRequest(request);
}

function GovMapGisApi_DisplayCircle(CircleArray, ClearCurrentDrawing, OutLineColor, FillColor, NameArray)
{
    var request = {
        "requestType": "DisplayCircle",
        "circleArray": CircleArray,
        "nameArray": NameArray,
        "clearCurrentDrawing": ClearCurrentDrawing,
        "fillColor": FillColor,
        "outLineColor": OutLineColor
    };

    if (!doProcess())
    {
        addDelegateToQueue(request, "DisplayCircle");
        return;
    }

    postMapRequest(request);
}


function GovMapGisApi_DeleteGraphicByName(NameArray, ClearCurrentDrawing)
{
    var request = {
        "requestType": "DeleteGraphicByName",
        "nameArray": NameArray,
        "clearCurrentDrawing": ClearCurrentDrawing
    };

    if (!doProcess())
    {
        addDelegateToQueue(request, "DeleteGraphicByName");
        return;
    }

    postMapRequest(request);
}

function GovMapGisApi_SwitchLayersVisibility(arrSwitchOnLayers, arrSwitchOffLayers, UserKey)
{
    var request = {
        "requestType": "SwitchLayerVisibility",
        "arrSwitchOnLayers": arrSwitchOnLayers,
        "arrSwitchOffLayers": arrSwitchOffLayers,
        "UserKey": (UserKey == undefined) ? "" : UserKey
    };

    if (!doProcess())
    {
        addDelegateToQueue(request, "SwitchLayerVisibility");
        return;
    }

    postMapRequest(request);
}

function GovMapGisApi_AddressToLotParcel(UserKey, Address, CallbackFunctionName)
{
    var request = {
        "requestType": "AddressToLotParcel",
        "UserKey": UserKey,
        "addressValue":Address,
        "CallbackFunctionName": CallbackFunctionName
    }

    if (!doProcess())
    {
        addDelegateToQueue(request, "AddressToLotParcel");
        return;
    }

    postMapRequest(request);
}

function GovMapGisApi_LotParcelToAddress(UserKey, LotParcel, CallbackFunctionName)
{
    var request = {
        "requestType": "LotParcelToAddress",
        "UserKey": UserKey,
        "LotParcel": LotParcel,        
        "CallbackFunctionName": CallbackFunctionName
    };

    if (!doProcess())
    {
        addDelegateToQueue(request, "LotParcelToAddress");
        return;
    }

    postMapRequest(request);
}

function GovMapGisApi_ShowMeasure(hideMeasure) {
    var request = {
        "requestType": "ShowMeasure",
        "isHide": (hideMeasure == undefined) ? false : hideMeasure
    };

    if (!doProcess()) {
        addDelegateToQueue(request, "ShowMeasure");
        return;
    }

    postMapRequest(request);
}


function GovMapGisApi_GetIntersectsFeatures(UserKey, SearchType, SearchParams, DestinationLayerName, FieldName, CallbackFunctionName) {
    var request = {
        "requestType": "APIGetIntersectsFeatures",
        "UserKey": UserKey,
        "SearchType": SearchType,
        "SearchParams": SearchParams,
        "DestinationLayerName": DestinationLayerName,
        "FieldName": FieldName,
        "CallbackFunctionName": CallbackFunctionName
    };

    if (!doProcess()) {
        addDelegateToQueue(request, "APIGetIntersectsFeatures");
        return;
    }

    postMapRequest(request);
}

function GovMapGisApi_GetLegend(UserKey, parameters)
{
    var request =
        {
            'requestType': 'GetLegend',
            'UserKey': UserKey,
            'parameters': parameters
        };

    if (!doProcess())
    {
        addDelegateToQueue(request, "GetLegend");
        return;
    }

    postMapRequest(request);
}

function GovMapGisApi_AddOpenSourceLayer(UserKey, parameters)
{
    var request =
        {
            'requestType': 'CreateRenderer',
            'UserKey': UserKey,
            'parameters': parameters
        };

    if (!doProcess())
    {
        addDelegateToQueue(request, "CreateRenderer");
        return;
    }

    postMapRequest(request);
}

function GovMapGisApi_RefreshResource(resourceName)
{
    var request = {
        "requestType": "RefreshResource",
        "resourceName": resourceName
    };

    if (!doProcess())
    {
        addDelegateToQueue(request, "RefreshResource");
        return;
    }

    postMapRequest(request);
}

function GovMapGisApi_RefreshResourceByLayerName(layerName)
{
    var request = {
        "requestType": "RefreshResourceByLayerName",
        "layerName": layerName
    };

    if (!doProcess())
    {
        addDelegateToQueue(request, "RefreshResourceByLayerName");
        return;
    }

    postMapRequest(request);
}

function showRainRadar(callbackName, autoPlayRadar)
{
    var request = {
        "requestType": "showRainRadar",
        "callbackName": callbackName,
        "playRadar": autoPlayRadar
    };

    if (!doProcess())
    {
        addDelegateToQueue(request, "showRainRadar");
        return;
    }

    postMapRequest(request);
}

function playRadar()
{
    var request = {
        "requestType": "playRadar"
    };

    if (!doProcess())
    {
        addDelegateToQueue(request, "playRadar");
        return;
    }

    postMapRequest(request);
}

function pauseRadar()
{
    var request = {
        "requestType": "pauseRadar"
    };

    if (!doProcess())
    {
        addDelegateToQueue(request, "pauseRadar");
        return;
    }

    postMapRequest(request);
}

function setRadarOpacity(value)
{
    var request = {
        "requestType": "setRadarOpacity",
        "opacity": value
    };

    if (!doProcess())
    {
        addDelegateToQueue(request, "setRadarOpacity");
        return;
    }

    postMapRequest(request);
}

function GovMapGisApi_WatchGPSLocation(showMarker, callback)
{
    var requestType = "apiWatchGPSLocation";
    var request = {
        "requestType": requestType,
        "showMarker": showMarker,
        "callbackName": callback
    };

    if (!doProcess())
    {
        addDelegateToQueue(request, requestType);
        return;
    }

    postMapRequest(request);
};

function GovMapGisApi_StopGPSLocationWatch()
{
    var requestType = "stopGPSLocationWatch";
    var request = {
        "requestType": requestType
    };

    if (!doProcess())
    {
        addDelegateToQueue(request, requestType);
        return;
    }

    postMapRequest(request);
};

function GovMapGisApi_RemoveGPSMarker()
{
    var requestType = "removeGPSMarker";
    var request = {
        "requestType": requestType
    };

    if (!doProcess())
    {
        addDelegateToQueue(request, requestType);
        return;
    }

    postMapRequest(request);
};