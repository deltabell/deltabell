/* to run this script:

open terminal, cd to main folder of project
run: node path_to_this_script
*/

var Mongoose = require('mongoose');
var Apartment = require('../../models/apartment');

prod_db_con_string = 'mongodb://productionuser:aA!#fgdgHH$19v@ds111798.mlab.com:11798/dbprod';
dev_db_con_string = 'mongodb://devteam:devteam!@ds127399.mlab.com:27399/dbdev';
Mongoose.connect(prod_db_con_string);

var db = Mongoose.connection;

/* Template
var newApartment = new Apartment({
        matterportKey: "",
        apartmentTitle:  "",
        summary: {
            price: ?,
            rooms: ?,
            size: ?,
            contact: ""
            floor: ?
        },
        details:{
            description: "",
            features: []//['מיזוג','מעלית','מרפסת','סורגים','מרפסת שמש','חניה','ממ״ד','מחסן','משופצת'}
        },
        lat: ?.? //smaller
        lng: ?.? //bigger
        sketchFile: "",
        gush: "",
        section: "",
        images:[
            {src: ''},
            {src: ''},
            {src: ''},
            {src: ''},
            {src: ''},
            {src: ''}
        ]
});
*/

var newApartment = new Apartment({
    matterportKey: "",
        apartmentTitle: "נחל יעלה, רמת בית שמש א, בית שמש",
        summary: {
            price: "-",
            rooms: 10,
            size: "450 מ׳׳ר",
            floor: 0,
            contact: "רימון נדל׳׳ן 02-9991000"
        },
        details:{
            description: " שטח בנוי 450 מ’’ר. בקומת הכניסה: סלון ענק, פינת אוכל מרווחת, 2 פינות ישיבה גדולות, שירותים וגינה היקפית ענקית. בקומה הראשונה: 3 חדרי שינה, חדר אמבטיה + יחידת הורים נפרדת ומרפסת. בקומה השניה: 2 חדרים, חדר אמבטיה, שירותים. למגורים או למשרד. בקומה 1- : בריכה פרטית מפוארת עם מקלחות. יחידת מגורים נפרדת עם כניסה פרטית, בעלת 2.5 חדרי שינה כולל יחידת הורים, סלון ומטבח.+\nBeautiful free standing Villa in Ramat Beit Shemesh Alef.A total of 450 meters. Entrance level: Spacious salon, large dining room, 2 sitting areas, bathroom, and wrap around manicured garden. First floor: 3 bedrooms, en suite bath, family bath. Second floor:  2 large rooms, bathroom, perfect for office or additional living quarters. Minus 1: large and luxurious inground pool, showers, and a separate 2.5 bedroom apartment with ensuite bath and a large salon.",
            features: ['מיזוג','מרפסת שמש','חניה',]
        },
        lat: 31.717214, //smaller
        lng: 34.998588, //bigger
        sketchFile: "",
        gush: "",
        section: "",
        images:[
            {src: '01.31.2017_14.05.18.jpg'},
            {src: '01.31.2017_14.05.42.jpg'},
            {src: '01.31.2017_14.06.04.jpg'},
            {src: '01.31.2017_21.13.26.jpg'},
            {src: 'rimon_logo.png'},
            {src: '01.31.2017_21.30.47.jpg'},
            {src: '01.31.2017_21.31.19.jpg'},
            {src: '01.31.2017_21.33.23.jpg'},
            {src: '01.31.2017_21.36.03.jpg'},
            {src: '01.31.2017_21.37.31.jpg'},
            {src: '01.31.2017_21.38.27.jpg'},
            {src: '01.31.2017_21.40.06.jpg'},
            {src: '01.31.2017_21.40.30.jpg'},
            {src: '01.31.2017_21.42.21.jpg'},
            {src: '01.31.2017_21.43.09.jpg'},
            {src: '01.31.2017_21.44.00.jpg'},
            {src: '01.31.2017_21.50.07.jpg'},
            {src: '01.31.2017_21.50.30.jpg'}

        ]
})

Apartment.createApartment(newApartment, function(err, apartment){
    if(err) throw err;
    console.log(apartment);
});

Apartment.getApartmentById('58873f388001b805f0284055', function(err, apartment){
    if(err) throw err;
    console.log(apartment);
});
console.log('Press any key to exit');

process.stdin.setRawMode(true);
process.stdin.resume();
process.stdin.on('data', process.exit.bind(process, 0));

