var fs = require('fs');
var Nodemailer = require('nodemailer');
var Apartment //Because it's circular require - apartment & utils. it is acts with lazy loading

var Utils = {
    // Returns the configuration
    // Case there are no configuration file it returns default configurations
    getConfig: function(){
        if (fs.existsSync('./config.js')) {
            return(require('../config.js'));
        } else {
            return(require('../default_config.js'));
        };       
    },
    getReversedString: function(original_string){
        var o = '';
        for (var i = original_string.length - 1; i >= 0; i--)
            o += original_string[i];
        return o;
    },
    sendEmail: function(options) {
        subject = options.subject 
        emailTarget = options.emailTarget 
        text = options.text
        attachedFiles = options.attachedFiles
        callback = options.callback

        var Config = this.getConfig();
        var transporter = Nodemailer.createTransport({
            service: 'Gmail',
            auth: {
                user: Config.info_email_user, // DeltaBell info mail
                pass: Config.info_email_password // DeltaBell info password
            }
        });

        
        // Mail options
        var mailOptions = {
            from: 'info@deltabell.net', // sender address
            to: emailTarget, // list of receivers
            bcc: 'amram@deltabell.net',
            subject: subject,
            text: text
        };
        
        if (attachedFiles && attachedFiles.length > 0) {
            attachments_objects = attachedFiles.map(function(filePath){
                return {
                    filename: filePath.substring(filePath.lastIndexOf('/')+1),
                    path: Config.storage_root_path + filePath.substring(1),
                    cid: 'info@deltabell.net' // should be as unique as possible
                }
            })
            mailOptions["attachments"] = attachments_objects
        }
        
        // Sending the mail
        transporter.sendMail(mailOptions, function(error, info){
            if(error){
                console.log(error);
                callback(error, false);
            } else {
                // redirecting the client to the success form
                callback(null, true);
            };
        });
    },
    printError: function(context, err){
        console.log('error in ' + context + ': ' + err.message);
    },
    //Changing relevant tags for sharing into index.html where receives request for an apartment page
    loadOrReplaceMetaTags: function(requestUrl, callback){
        if (requestUrl.toLowerCase().includes('singleapartment')) {
            EditingHtmlTags.handleSingleApartmentRequest(requestUrl, callback);
        } else if (requestUrl.toLowerCase().includes('contactus')) {
            EditingHtmlTags.handleContactUsRequest(requestUrl, callback);
        } else {
            callback(null);
        }
    }        
}


// Private class for this file, targeted to editing html tags. It needed for search engines & sharing
var EditingHtmlTags = {
    // Inner impot to prevent circular import apartment file & utils file
    getApartmentFile: function(){
        if (!Apartment) {Apartment = new require('../models/apartment.js')}
        return Apartment;
    },
    // Replace tag title for google search results
    handleContactUsRequest: function(requestUrl, callback){
        fs.readFile('./index.html', {encoding:'utf8'}, function(err, file){
            if (err) {
                callback(null);
            } else {
                try {
                    searchFor = new RegExp("<title>.*</title>");
                    file = file.replace(searchFor, "<title>דלתא בל - צור קשר</title>")
                    callback(file);
                } catch (err) {
                    Utils.printError('defining tags for sharing in index.html for contactUs page', err);
                    callback(null);
                } finally {}
            }                                
        }.bind(this));
    },
    handleSingleApartmentRequest: function(requestUrl, callback){
        // Replace tags for sharing facebook & google search results
        urlParts = requestUrl.split('/');
        apartmentId = urlParts[urlParts.length - 1];
        
        this.getApartmentFile().getApartmentById(apartmentId, function (err, apartment){
            if (err) { callback(null); }
            else {
                fs.readFile('./index.html', {encoding:'utf8'}, function(err, file){
                    if (err) { callback(null); } 
                    else 
                    {
                        try
                        {
                            urlPrefix = "https://" + Utils.getConfig().hostname;
                            
                            propertyValues = {'title' : apartment.apartmentTitle + " | " + "דלתא-בל",
                                                'url': urlPrefix + requestUrl,
                                                'description': apartment.details.description,
                                                'image': urlPrefix + apartment.images[0].src};
                            
                            for (var propertyName in propertyValues){
                                searchFor = new RegExp(":"+propertyName+".*>", "g");
                                replaceTo = ":"+propertyName+'" content="' + propertyValues[propertyName] + '" />';
                                
                                file = file.replace(searchFor, replaceTo);
                            } 
                            
                            searchFor = new RegExp("<meta name=\"description\".*>");
                            file = file.replace(searchFor,
                                                "<meta name=\"description\" content=\"" + apartment.details.description + "\"/>");
                            callback(file);
                        } 
                        catch (err) {
                            Utils.printError('defining tags for sharing in index.html', err);
                            callback(null);
                        }
                        finally {} // Do nothing
                    }                                
                }.bind(this));
            }
        }.bind(this))
    }
}

module.exports = Utils;
