var path = require('path');
var webpack = require('webpack');


module.exports = function(env) {

    var config_plugins = [new webpack.HotModuleReplacementPlugin()];
    if (env && env.prod) {
        config_plugins = config_plugins.concat(
            [
                new webpack.optimize.AggressiveMergingPlugin(),
                new webpack.optimize.OccurrenceOrderPlugin(),
                new webpack.DefinePlugin({
                    'process.env': {
                        // This has effect on the react lib size
                        'NODE_ENV': JSON.stringify('production')
                    },
                }),
                new webpack.optimize.UglifyJsPlugin({
                    compressor: {
                        warnings: false
                    }
                })
            ])
    }

    return {
        mode: 'development',
        entry: './client/client_app_entry.js',

        // watch: true,
        output: {
            path: path.join(__dirname, 'public'),
            filename: 'deltabell_bundle.js',
            sourcePrefix: ''
        },
        // plugins: config_plugins,
        module: {
            unknownContextCritical: false,
            rules: [
                {
                    test: /\.m?js$/,
                    exclude: /(node_modules|bower_components)/,
                    use: {
                      loader: 'babel-loader',
                      options: {
                        presets: ['@babel/preset-react','@babel/preset-env']
                      }
                    }
                  },
                  {
                    test: /\.css$/i,
                    use: ['style-loader', 'css-loader'],
                  },
            ]
        }
    }
};